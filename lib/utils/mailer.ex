defmodule PandaPhoenix.Mailer do
  @moduledoc false

  use Bamboo.Mailer, otp_app: :panda_phoenix
end
