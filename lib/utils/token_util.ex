defmodule PandaPhoenix.Token do
  @moduledoc false

  alias PandaPhoenix.Logger
  alias PandaPhoenix.Watcher.Reported.ReportedWatcher
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.Repo
  alias PandaPhoenix.DateUtil


  defp time_validation, do: 1_209_600 #2 weeks
  defp time_validation_refresh, do: 4_838_400 #8 weeks
  defp time_validation_24h, do: 86_400 #24H

  defp token_user, do: "user"
  defp token_room, do: "public-room"
  defp token_user_refresh, do: "user-refrech"
  defp token_data, do: "data"
  defp token_active_user, do: "active-user"
  defp token_reset_password, do: "reset-password"


  #
  # IMAGE/VIDEO/DATA
  #

  def sign_data(data_name) do
    PandaPhoenix.Endpoint.config(:secret_key_base_data)
      |> Phoenix.Token.sign(token_data, data_name)
      |> String.replace(".", "")
      |> String.replace("/", "")
  end

  #
  # RESET PASSWORD
  #

  def sign_reset_password(user_id) do
    PandaPhoenix.Endpoint.config(:secret_key_base_reset_password)
      |> Phoenix.Token.sign(token_reset_password, user_id)
  end

  def verify_reset_password(token) do
    case PandaPhoenix.Endpoint.config(:secret_key_base_reset_password)
          |> Phoenix.Token.verify(token_reset_password, token, max_age: time_validation_24h) do
      {:ok, user_id} ->
        {:authenticated, user_id}
      _ ->
        :invalid
    end
  end

  #
  # ACTIVE USER
  #

  def sign_active_user(user_id) do
    PandaPhoenix.Endpoint.config(:secret_key_base_activation_user)
      |> Phoenix.Token.sign(token_active_user, user_id)
  end

  def verify_active_user(token) do
    case PandaPhoenix.Endpoint.config(:secret_key_base_activation_user)
          |> Phoenix.Token.verify(token_active_user, token, max_age: time_validation_24h) do
      {:ok, user_id} ->
        {:authenticated, user_id}
      _ ->
        :invalid
    end
  end

  #
  # USER
  #

  def sign_user(user_id) do
    Phoenix.Token.sign(PandaPhoenix.Endpoint, token_user, user_id)
  end

  def verify_user(%{token: token, with_disconnection: with_disconnection, can_active: can_active}) do
    Logger.debug(__ENV__, "verify_user", [%{token: token}, %{with_disconnection: with_disconnection}, %{can_active: can_active}])
    case Phoenix.Token.verify(PandaPhoenix.Endpoint, token_user, token, max_age: time_validation) do
      {:ok, user_id} ->
        case ReportedWatcher.get(user_id, with_disconnection, can_active) do
          {:error, %{time: time}} ->
            {:reported, %{time: time}, user_id}
          :ok ->
            case Repo.get_by(UserAuthentication, user_id: user_id) do
              nil ->
                :invalid
              user_auth ->
                case user_auth.token == String.replace_prefix(token, "Bearer ", "") do
                  true ->
                    {:authenticated, user_id}
                  false ->
                    :duplicate_user
                end
            end
        end
      {:error, value} ->
        Logger.warn(__ENV__, "verify_user case {:error, value}", [%{value: value}])
        :invalid
    end
  end

  def verify_user(%{token: token, user_id: user_id, with_disconnection: with_disconnection}) do
    case verify_user(%{token: token, with_disconnection: with_disconnection, can_active: false}) do
      {:authenticated, user_id_auth} ->
        if user_id == user_id_auth do
          {:authenticated, user_id_auth}
        else
          :invalid
        end
      {:reported, %{time: time}, _user_id} ->
          {:reported, %{time: time}}
      :invalid ->
        :invalid
      :duplicate_user ->
        :duplicate_user
    end
  end

  #
  # public_room
  #

  def sign_public_room(room_token) do
    PandaPhoenix.Endpoint.config(:secret_key_base_room)
      |> Phoenix.Token.sign(token_room, %{list: room_token, time: DateUtil.timestamp})
  end

  def verify_public_room(room_token) do
    case PandaPhoenix.Endpoint.config(:secret_key_base_room)
          |> Phoenix.Token.verify(token_room, room_token, max_age: time_validation) do
      {:ok, %{list: public_room, time: _timestamp}} ->
        {:authenticated, public_room}
      _ ->
        :invalid
    end
  end

  #
  # public_room
  #

  def sign_user_refrech(user_id) do
    PandaPhoenix.Endpoint.config(:secret_key_refresh_token)
          |> Phoenix.Token.sign(token_user_refresh, user_id)
  end

  def verify_for_renew(token, refresh_token) do
    case Phoenix.Token.verify(PandaPhoenix.Endpoint, token_user, token, max_age: time_validation_refresh) do
      {:ok, user_id} ->
        case PandaPhoenix.Endpoint.config(:secret_key_refresh_token)
              |> Phoenix.Token.verify(token_user_refresh, refresh_token, max_age: time_validation_refresh) do
          {:ok, user_id_refresh} ->
            if user_id == user_id_refresh do
              case Repo.one(UserAuthentication.query_auth_by_user_id(user_id)) do
                nil ->
                  :invalid
                user_auth ->
                  if user_auth.token == token && user_auth.refresh_token == refresh_token do
                    %{token: sign_user(user_id), refresh_token: sign_user_refrech(user_id), user_id: user_id}
                  else
                    :invalid
                  end
              end
            else
              :invalid
            end
          {:error, _} ->
            :invalid
        end
      {:error, _} ->
        :invalid
    end
  end

end
