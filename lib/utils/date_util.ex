defmodule PandaPhoenix.DateUtil do
  @moduledoc false

  epoch = {{1970, 1, 1}, {0, 0, 0}}
  @epoch :calendar.datetime_to_gregorian_seconds(epoch)

  def format_date_time({{year, month, day}, {hour, minute, second}}) do
    :io_lib.format("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B",
     [year, month, day, hour, minute, second])
     |> List.flatten
     |> to_string
  end

  def format_date({{year, month, day}}) do
    :io_lib.format("~4..0B-~2..0B-~2..0B",
      [year, month, day])
      |> List.flatten
      |> to_string
  end

  def from_timestamp(timestamp) do
    timestamp
    |> Kernel.+(@epoch)
    |> :calendar.gregorian_seconds_to_datetime
  end

  def to_timestamp(datetime) do
    datetime
    |> :calendar.datetime_to_gregorian_seconds
    |> Kernel.-(@epoch)
  end

  def ecto_to_timestamp({{year, month, day}, {hour, minute, second, millisecond}}) do
    {:ok, result} = {{year, month, day}, {hour, minute, second, millisecond}}
    |> Ecto.DateTime.cast

    ecto_to_timestamp(result)
  end

  def ecto_to_timestamp(datetime) do
    datetime
    |> Ecto.DateTime.to_erl
    |> :calendar.datetime_to_gregorian_seconds
    |> Kernel.-(@epoch)
  end

  def timestamp do
    :os.system_time(:seconds)
  end

end
