defmodule PandaPhoenix.RetoUtil do
  @moduledoc false

  alias PandaPhoenix.Logger

  def query_to_tuple(query_result, table_name) do
    case query_to_tuple(query_result) do
       {:ok, result} ->
         {:ok, %{table: table_name, result: result}}
        other ->
          other
    end
  end

  def query_to_tuple(query_result) do
    case query_result do
      {:ok, result} ->
        list_tuple =
          if result.num_rows > 0 do
            Enum.map result.rows, fn(row) ->
              Enum.reduce(Enum.zip(result.columns, row), %{}, fn({key, value}, map) ->
                Map.put(map, String.to_atom(key), value)
              end)
            end
          else
            []
          end

        Logger.debug(__ENV__, "QUERY RESULT", [%{result: list_tuple}])
        {:ok, list_tuple}

      [h|t] ->
        list = [h|t]

        list_tuple =
          if length(list) > 0 do
            Enum.map list, fn(row) ->
              Enum.reduce(Enum.zip(Map.keys(row), Map.values(row)), %{}, fn({key, value}, map) ->
                Map.put(map, key, value)
              end)
            end
          else
            []
          end

        Logger.debug(__ENV__, "QUERY RESULT", [%{result: list_tuple}])
        {:ok, list_tuple}
      [] ->
        Logger.debug(__ENV__, "QUERY RESULT", [%{result: []}])
        {:ok, []}
      other ->
        Logger.error(__ENV__, "QUERY NOT BE CONSUMED", [%{result: other}])
        query_result
    end
  end

  def query_to_tuple_and_get_fisrt(query_result) do
    case query_to_tuple(query_result) do
      {:ok, list} ->
        {:ok, List.first(list)}
      _ ->
        query_result
    end

  end

end
