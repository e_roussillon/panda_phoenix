defmodule PandaPhoenix.LocalisationUtil do
  @moduledoc false

  @empty_info "NONE"
  @key_api Application.get_env(:panda_phoenix, :google_api_key)
  @url_api Application.get_env(:panda_phoenix, :google_api_url)

  use Phoenix.Controller

  # UTIL
  alias PandaPhoenix.LocalisationBrasil
  alias PandaPhoenix.LocalisationCanada
  alias PandaPhoenix.LocalisationDefault
  alias PandaPhoenix.LocalisationDefaultWithoutCity
  alias PandaPhoenix.ErrorLocalization
  # alias PandaPhoenix.Logger

  # CONTROLLER
  alias PandaPhoenix.PublicRoomLocalizationController

  def init_path(conn, latitude, longitude) do
    String.replace(@url_api, "@LAT", latitude)
      |> String.replace("@LNG", longitude)
      |> String.replace("@KEY", @key_api)
      |> get_result_google_api(conn, latitude, longitude)
  end

  def get_value(map, key) do
    case Map.get(map, key)  do
      nil ->
        @empty_info
      list ->
        keys = Map.keys(list)
        case length(keys) do
          nil ->
            @empty_info
          0 ->
            @empty_info
          1 ->
            case List.first(keys) |> get_result() do
              {:ok, "unnamed road"} ->
                @empty_info
              {:ok, name} ->
                name
            end
          _ ->
          {name, _} = Map.to_list(list)
                              |> Enum.sort(fn(x, y) -> {_t_name, t_num} = x
                                                       {_t_name2, t_num2} = y
                                                       t_num > t_num2
                                           end)
                              |> List.first()
            name
        end
    end
  end

  def get_value_but_different_of(map, key, different_of) do
    case Map.get(map, key)  do
      nil ->
        @empty_info
      list ->
        keys = Map.keys(list)
        case length(keys) do
          nil ->
            @empty_info
          0 ->
            @empty_info
          1 ->
            case List.first(keys) |> get_result() do
              {:ok, "unnamed road"} ->
                @empty_info
              {:ok, name} ->
                 {:ok, different_of_downcase} = get_result(different_of)

                if String.contains?(name, different_of_downcase)  do
                  @empty_info
                else
                  name
                end

            end
          _ ->
            {:ok, different_of_downcase} = get_result(different_of)
            Map.to_list(list)
                  |> Enum.sort(fn(x, y) -> {_t_name, t_num} = x
                                            {_t_name2, t_num2} = y
                                            t_num > t_num2 end)
                  |> check_list_value(different_of_downcase)
        end
    end
  end

  defp check_list_value([], _different_of) do
    @empty_info
  end

  defp check_list_value([{name, _count}|t], different_of) do
    {:ok, new_name} = get_result(name)
    if String.contains?(new_name, different_of)  do
      check_list_value(t, different_of)
    else
      new_name
    end
  end

  #
  # PRIVATE
  #

  defp get_result_google_api(url, conn, latitude, longitude) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        case Poison.Parser.parse!(body) do
          %{"results" => results, "status" => "OK"} ->
            case results |>  get_path()  do
              {:ok, %{:result => result, :api => result_api}} ->
                s_result_api = Poison.encode! result_api
                s_result = Poison.encode! result
                PublicRoomLocalizationController.create(conn, %{"public_room_localization" => %{"latitude" => latitude, "longitude" => longitude, "api_result" => s_result_api, "result" => s_result, "google_result" => Poison.encode! results}, "api_result" => result_api, "result" => result})
              :error ->
                conn
                  |> put_status(:not_found)
                  |> render(PandaPhoenix.ChangesetView, "error.json", ErrorLocalization.init_error_country_not_found())
            end
          _ ->
            conn
              |> put_status(:not_found)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorLocalization.init_error_empty())
        end
      _ ->
        conn
          |> put_status(:not_found)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorLocalization.init_error_empty())
    end
  end

  defp get_result(result) do
    if is_binary(result) do
      {:ok, String.downcase(result) }
    else
      {:ok, result}
    end
  end

  def empty_info do
    @empty_info
  end

  defp address_list([], result) do
    result
  end

  defp address_list([%{"address_components" => list, "formatted_address" => _, "geometry" => _, "place_id" => _, "types" => _} | t], result) do
    address_item(t, list, result)
  end

  defp address_item(list, [], result) do
    address_list(list, result)
  end

  defp address_item(list, [%{"long_name" => long_name, "short_name" => _, "types" => types} | t], result) do
    result = type_list(long_name, types, result)
    address_item(list, t, result)
  end

  defp type_list(_long_name, [], result) do
    result
  end

  defp type_list(long_name, [type|t], result) do
    result = case Map.get(result, type) do
                nil ->
                  Map.put(result, type, Map.put(Map.new(), long_name, 1))
                list ->
                  case Map.get(list, long_name) do
                    nil ->
                       Map.put(result, type, Map.put(list, long_name, 1))
                    _ ->
                      Map.put(result, type, Map.update!(list, long_name, &(&1 + 1)))
                  end
              end
    type_list(long_name, t, result)
  end

  defp get_path(result) do
    result_api = address_list(result, Map.new())
    case get_value(result_api, "country") do
      "albania" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "albania"), :api => result_api}}
      "algeria" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "algeria"), :api => result_api}}
      "angola" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "angola"), :api => result_api}}
      "anguilla" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "angola"), :api => result_api}}
      "antigua and barbuda" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "antigua and barbuda"), :api => result_api}}
      "argentina" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "argentina"), :api => result_api}}
      "armenia" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "armenia"), :api => result_api}}
      "australia" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "australia"), :api => result_api}}
      "austria" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "austria"), :api => result_api}}
      "azerbaijan" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "azerbaijan"), :api => result_api}}
      "the bahamas" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "the bahamas"), :api => result_api}}
      "bahrain" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "bahrain"), :api => result_api}}
      "barbados" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "barbados"), :api => result_api}}
      "belarus" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "belarus"), :api => result_api}}
      "belgium" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "belgium"), :api => result_api}}
      "belize" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "belize"), :api => result_api}}
      "benin" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "benin"), :api => result_api}}
      "bermuda" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "bermuda"), :api => result_api}}
      "bhutan" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "bhutan"), :api => result_api}}
      "bolivia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "bolivia"), :api => result_api}}
      "botswana" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "botswana"), :api => result_api}}
      "brazil" ->
        {:ok, %{:result => LocalisationBrasil.init_path(result_api, "brazil"), :api => result_api}}
      "brunei" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "brunei"), :api => result_api}}
      "bulgaria" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "bulgaria"), :api => result_api}}
      "burkina faso" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "burkina faso"), :api => result_api}}
      "cambodia" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "cambodia"), :api => result_api}}
      "canada" ->
        {:ok, %{:result => LocalisationCanada.init_path(result_api, "canada"), :api => result_api}}
      "cape verde" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "cape verde"), :api => result_api}}
      "cayman islands" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "cayman islands"), :api => result_api}}
      "chad" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "chad"), :api => result_api}}
      "chile" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "chile"), :api => result_api}}
      "china" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "china"), :api => result_api}}
      "colombia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "colombia"), :api => result_api}}
      "republic of the congo" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "republic of the congo"), :api => result_api}}
      "costa rica" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "costa rica"), :api => result_api}}
      "croatia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "croatia"), :api => result_api}}
      "cyprus" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "cyprus"), :api => result_api}}
      "czech republic" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "czech republic"), :api => result_api}}
      "denmark" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "denmark"), :api => result_api}}
      "dominica" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "dominica"), :api => result_api}}
      "dominican republic" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "dominican republic"), :api => result_api}}
      "ecuador" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "ecuador"), :api => result_api}}
      "egypt" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "egypt"), :api => result_api}}
      "el salvador" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "el salvador"), :api => result_api}}
      "estonia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "estonia"), :api => result_api}}
      "fiji" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "fiji"), :api => result_api}}
      "finland" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "finland"), :api => result_api}}
      "france" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "france"), :api => result_api}}
      "the gambia" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "the gambia"), :api => result_api}}
      "germany" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "germany"), :api => result_api}}
      "ghana" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "ghana"), :api => result_api}}
      "greece" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "greece"), :api => result_api}}
      "grenada" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "grenada"), :api => result_api}}
      "guatemala" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "guatemala"), :api => result_api}}
      "guinea-bissau" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "guinea-bissau"), :api => result_api}}
      "guyana" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "guyana"), :api => result_api}}
      "honduras" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "honduras"), :api => result_api}}
      "hong kong" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "hong kong"), :api => result_api}}
      "hungary" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "hungary"), :api => result_api}}
      "iceland" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "iceland"), :api => result_api}}
      "india" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "india"), :api => result_api}}
      "indonesia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "indonesia"), :api => result_api}}
      "ireland" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "ireland"), :api => result_api}}
      "israel" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "israel"), :api => result_api}}
      "italy" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "italy"), :api => result_api}}
      "jamaica" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "jamaica"), :api => result_api}}
      "japan" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "japan"), :api => result_api}}
      "jordan" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "jordan"), :api => result_api}}
      "kazakhstan" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "kazakhstan"), :api => result_api}}
      "kenya" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "kenya"), :api => result_api}}
      "south korea" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "south korea"), :api => result_api}}
      "kuwait" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "kuwait"), :api => result_api}}
      "kyrgyzstan" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "kyrgyzstan"), :api => result_api}}
      "laos" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "laos"), :api => result_api}}
      "latvia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "latvia"), :api => result_api}}
      "lebanon" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "lebanon"), :api => result_api}}
      "liberia" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "liberia"), :api => result_api}}
      "lithuania" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "lithuania"), :api => result_api}}
      "luxembourg" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "luxembourg"), :api => result_api}}
      "macau" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "macau"), :api => result_api}}
      "macedonia (fyrom)" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "macedonia (fyrom)"), :api => result_api}}
      "madagascar" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "madagascar"), :api => result_api}}
      "malawi" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "malawi"), :api => result_api}}
      "malaysia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "malaysia"), :api => result_api}}
      "mali" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "mali"), :api => result_api}}
      "malta" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "malta"), :api => result_api}}
      "mauritania" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "mauritania"), :api => result_api}}
      "mauritius" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "mauritius"), :api => result_api}}
      "mexico" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "mexico"), :api => result_api}}
      "federated states of micronesia" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "federated states of micronesia"), :api => result_api}}
      "moldova" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "moldova"), :api => result_api}}
      "mongolia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "mongolia"), :api => result_api}}
      "montserrat" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "montserrat"), :api => result_api}}
      "mozambique" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "mozambique"), :api => result_api}}
      "namibia" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "namibia"), :api => result_api}}
      "nepal" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "nepal"), :api => result_api}}
      "netherlands" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "netherlands"), :api => result_api}}
      "new zealand" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "new zealand"), :api => result_api}}
      "nicaragua" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "nicaragua"), :api => result_api}}
      "niger" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "niger"), :api => result_api}}
      "nigeria" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "nigeria"), :api => result_api}}
      "norway" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "norway"), :api => result_api}}
      "oman" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "oman"), :api => result_api}}
      "pakistan" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "pakistan"), :api => result_api}}
      "palau" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "palau"), :api => result_api}}
      "panama" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "panama"), :api => result_api}}
      "papua new guinea" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "papua new guinea"), :api => result_api}}
      "paraguay" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "paraguay"), :api => result_api}}
      "peru" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "peru"), :api => result_api}}
      "philippines" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "philippines"), :api => result_api}}
      "poland" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "poland"), :api => result_api}}
      "portugal" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "portugal"), :api => result_api}}
      "qatar" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "qatar"), :api => result_api}}
      "romania" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "romania"), :api => result_api}}
      "russia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "russia"), :api => result_api}}
      "saint lucia" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "saint lucia"), :api => result_api}}
      "saudi arabia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "saudi arabia"), :api => result_api}}
      "senegal" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "senegal"), :api => result_api}}
      "sierra leone" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "sierra leone"), :api => result_api}}
      "singapore" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "singapore"), :api => result_api}}
      "slovakia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "slovakia"), :api => result_api}}
      "slovenia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "slovenia"), :api => result_api}}
      "solomon islands" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "solomon islands"), :api => result_api}}
      "south africa" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "south africa"), :api => result_api}}
      "spain" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "spain"), :api => result_api}}
      "sri lanka" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "sri lanka"), :api => result_api}}
      "saint kitts and nevis" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "saint kitts and nevis"), :api => result_api}}
      "saint vincent and the grenadines" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "saint vincent and the grenadines"), :api => result_api}}
      "suriname" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "suriname"), :api => result_api}}
      "swaziland" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "swaziland"), :api => result_api}}
      "sweden" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "sweden"), :api => result_api}}
      "switzerland" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "switzerland"), :api => result_api}}
      "são tomé and príncipe" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "são tomé and príncipe"), :api => result_api}}
      "taiwan" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "taiwan"), :api => result_api}}
      "tajikistan" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "tajikistan"), :api => result_api}}
      "tanzania" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "tanzania"), :api => result_api}}
      "thailand" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "thailand"), :api => result_api}}
      "trinidad and tobago" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "trinidad and tobago"), :api => result_api}}
      "tunisia" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "tunisia"), :api => result_api}}
      "turkey" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "turkey"), :api => result_api}}
      "turkmenistan" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "turkmenistan"), :api => result_api}}
      "turks and caicos islands" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "turks and caicos islands"), :api => result_api}}
      "uganda" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "uganda"), :api => result_api}}
      "ukraine" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "ukraine"), :api => result_api}}
      "united arab emirates" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "united arab emirates"), :api => result_api}}
      "united kingdom" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "united kingdom"), :api => result_api}}
      "united states" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "united states"), :api => result_api}}
      "uruguay" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "uruguay"), :api => result_api}}
      "uzbekistan" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "uzbekistan"), :api => result_api}}
      "venezuela" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "venezuela"), :api => result_api}}
      "vietnam" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "vietnam"), :api => result_api}}
      "british virgin islands" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "british virgin islands"), :api => result_api}}
      "yemen" ->
        {:ok, %{:result => LocalisationDefaultWithoutCity.init_path(result_api, "yemen"), :api => result_api}}
      "zimbabwe" ->
        {:ok, %{:result => LocalisationDefault.init_path(result_api, "zimbabwe"), :api => result_api}}
      country when country == @empty_info ->
        :error
      _ ->
        {:ok, %{:result => "", :api => result_api}}
    end
  end


end
