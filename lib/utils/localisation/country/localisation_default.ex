defmodule PandaPhoenix.LocalisationDefault do
  @moduledoc false

  alias PandaPhoenix.LocalisationUtil

  def init_path(result_api, country) do
    # :country -> "country"
    # :state  -> "administrative_area_level_1"
    # :county -> "administrative_area_level_2"
    # :city -> "locality"
    # :neighborhood -> "sublocality"
    # :street -> "route"
    # :street_number -> "street_number"

    # state
    state = LocalisationUtil.get_value(result_api, "administrative_area_level_1")
    # county
    county =
      case LocalisationUtil.get_value(result_api, "administrative_area_level_2") do
        "NONE" ->
          LocalisationUtil.get_value(result_api, "administrative_area_level_3")
        other ->
          other
      end
    # city
    city =
      case LocalisationUtil.get_value(result_api, "locality") do
        "NONE" ->
          case LocalisationUtil.get_value(result_api, "administrative_area_level_3") do
            "NONE" ->
              case LocalisationUtil.get_value(result_api, "administrative_area_level_2") do
                "NONE" ->
                  LocalisationUtil.get_value(result_api, "administrative_area_level_1")
                other ->
                  other
              end
            other ->
              other
          end
        other ->
          other
      end
    # neighborhood
    neighborhood =
      case LocalisationUtil.get_value(result_api, "sublocality") do
        "NONE" ->
          LocalisationUtil.get_value(result_api, "neighborhood")
        other ->
          other
      end
    # street
    street =
      case LocalisationUtil.get_value(result_api, "route") do
        "NONE" ->
          case LocalisationUtil.get_value(result_api, "sublocality_level_2") do
              "NONE" ->
                LocalisationUtil.get_value(result_api, "sublocality_level_1")
              other ->
                other
          end
        other ->
          other
      end
    # street_number
    street_number =
      case LocalisationUtil.get_value(result_api, "street_number") do
        "NONE" ->
          LocalisationUtil.get_value(result_api, "premise")
        other ->
          other
      end

    %{:country => country}
      |> Map.put(:state, state |> String.downcase() |> String.split("state of ") |> List.last())
      |> Map.put(:county, county)
      |> Map.put(:city, city)
      |> Map.put(:neighborhood, neighborhood)
      |> Map.put(:street, street)
      |> Map.put(:street_number, street_number)
  end

end
