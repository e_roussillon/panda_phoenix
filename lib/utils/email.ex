defmodule PandaPhoenix.Email do
  @moduledoc false

  use Bamboo.Phoenix, view: PandaPhoenix.EmailView
  alias PandaPhoenix.Token
  alias PandaPhoenix.UserPasswordController
  alias PandaPhoenix.Logger

  @from "noreply@dotpanda.co"

  def welcome_email(user) do
    link = "#{Application.get_env(:panda_phoenix, :url_email)}active?token=#{Token.sign_active_user(user.id)}"
    Logger.info(__ENV__, "send welcome_email", [%{link: link}])

    new_email()
    |> to(user.email)
    |> from(@from)
    |> subject("Welcome to .panda!")
    |> put_html_layout({PandaPhoenix.LayoutView, "email.html"})
    |> render("welcome.html", pseudo: String.capitalize(user.pseudo), link: link)
    |> PandaPhoenix.Mailer.deliver_later
  end

  def reset_password_email(user) do
    token = Token.sign_reset_password(user.id)
    UserPasswordController.update(%{"user_id" => user.id, "user_password" => %{"reset_password_token" => token}})

    link = "#{Application.get_env(:panda_phoenix, :url_email)}reset?token=#{token}"

    Logger.info(__ENV__, "send reset_password_email", [%{link: link}])
    new_email()
    |> to(user.email)
    |> from(@from)
    |> subject("#{String.capitalize(user.pseudo)}, here's the link to reset your password")
    |> put_html_layout({PandaPhoenix.LayoutView, "email.html"})
    |> render("password.html", pseudo: String.capitalize(user.pseudo), link: link)
    |> PandaPhoenix.Mailer.deliver_later
  end

end
