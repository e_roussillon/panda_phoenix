defmodule PandaPhoenix.EmailValidator do

  # ensure that the email looks valid
  def validate_email(email) when is_binary(email) do
    case Regex.run(~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/, email) do
      nil ->
        {:error, "Invalid email"}
      [email] ->
        :ok
    end
  end

end
