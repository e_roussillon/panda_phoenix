defmodule PandaPhoenix.Logger do
  @moduledoc false

  import Logger

  #
  # Error
  #

  def error(msg) do
    Logger.error("Message: #{msg}")
  end

  def error(file_env, msg) do
    {method, param} = file_env.function
    Logger.error("\n  Processing by #{file_env.module}.#{method}/#{param}:\n    File: #{file_env.module}:#{file_env.line}\n   Message: #{msg}")
  end

  def error(file_env, msg, param) do
    param_string = change_param_to_string(param, "")
    {method, param} = file_env.function
    Logger.error("\n  Processing by #{file_env.module}.#{method}/#{param}:\n    File: #{file_env.module}:#{file_env.line}\n    Message: #{msg}\n    Param:#{param_string}\n")
  end

  #
  # Warn
  #

  def warn(msg) do
    Logger.warn("Message: #{msg}")
  end

  def warn(file_env, msg) do
    {method, param} = file_env.function
    Logger.warn("\n  Processing by #{file_env.module}.#{method}/#{param}:\n    File: #{file_env.module}:#{file_env.line}\n    Message: #{msg}")
  end

  def warn(file_env, msg, param) do
    param_string = change_param_to_string(param, "")
    {method, param} = file_env.function
    Logger.warn("\n  Processing by #{file_env.module}.#{method}/#{param}:\n    File: #{file_env.module}:#{file_env.line}\n    Message: #{msg}\n    Param:#{param_string}\n")
  end

  #
  # Debug
  #

  def debug(msg) do
    Logger.debug("#{msg}")
  end

  def debug(file_env, msg) do
    {method, param} = file_env.function
    Logger.debug("\n  Processing by #{file_env.module}.#{method}/#{param}:\n    File: #{file_env.module}:#{file_env.line}\n    Message: #{msg}")
  end

  def debug(file_env, msg, param) do
    param_string = change_param_to_string(param, "")
    {method, param} = file_env.function
    Logger.debug("\n  Processing by #{file_env.module}.#{method}/#{param}:\n    File: #{file_env.module}:#{file_env.line}\n    Message: #{msg}\n    Param:#{param_string}\n")
  end

  #
  # Info
  #

  def info(msg) do
    Logger.info("#{msg}")
  end

  def info(file_env, msg) do
    {method, param} = file_env.function
    Logger.info("\n  Processing by #{file_env.module}.#{method}/#{param}:\n    File: #{file_env.module}:#{file_env.line}\n    Message: #{msg}")
  end

  def info(file_env, msg, param) do
    param_string = change_param_to_string(param, "")
    {method, param} = file_env.function
    Logger.info("\n  Processing by #{file_env.module}.#{method}/#{param}:\n    File: #{file_env.module}:#{file_env.line}\n    Message: #{msg}\n    Param:#{param_string}\n")
  end

  #
  # Private
  #

  defp change_param_to_string([], result) do
    result
  end

  defp change_param_to_string([h|t], result) do
    key = Map.keys(h) |> List.first()
    value = Map.get(h, key, "")

    tmp = "\n     -> #{key}: #{inspect value}"
    change_param_to_string(t, result <> tmp)
  end

end
