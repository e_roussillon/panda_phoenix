defmodule PandaPhoenix.ErrorAuth do
  @moduledoc false

  alias PandaPhoenix.ErrorUtil

  def init_error_user_not_found, do: ErrorUtil.init(ErrorUtil.error_auth, 0, "email or password incorrect")
  def init_error_token_not_found, do: ErrorUtil.init(ErrorUtil.error_auth, 1, "token is not found")
  def init_error_token_invalid, do: ErrorUtil.init(ErrorUtil.error_auth, 2, "token is invalid")
  def init_error_token_not_renew, do: ErrorUtil.init(ErrorUtil.error_auth, 3, "token not renew")
  def init_error_user_blocked(time), do: ErrorUtil.init(:details, ErrorUtil.error_auth, 4, "user reported", time)
  def init_error_duplicate_user, do: ErrorUtil.init(ErrorUtil.error_auth, 5, "user already login")
  def init_error_push_param_missing, do: ErrorUtil.init(ErrorUtil.error_auth, 6, "param missing")
  def init_error_rating_limit(time), do: ErrorUtil.init(:details, ErrorUtil.error_auth, 7, "rating limit", time)
  def init_error_not_active, do: ErrorUtil.init(ErrorUtil.error_auth, 8, "user not active")

end
