defmodule PandaPhoenix.ErrorData do
  @moduledoc false
  
  alias PandaPhoenix.ErrorUtil

  def init_error_upload_data, do: ErrorUtil.init(ErrorUtil.error_user, 0, "can not upload image")

end
