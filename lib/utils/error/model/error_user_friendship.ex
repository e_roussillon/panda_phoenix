defmodule PandaPhoenix.ErrorUserFriendship do
  @moduledoc false
  
  alias PandaPhoenix.ErrorUtil

  def init_error_action_imposible, do: ErrorUtil.init(ErrorUtil.error_user_friendship, 0, "action impossible")
  def init_error_find_page_param, do: ErrorUtil.init(ErrorUtil.error_user, 1, "page have to start at minimum 1")

end
