defmodule PandaPhoenix.ErrorMessageSender do
  @moduledoc false

  alias PandaPhoenix.ErrorUtil

  def init_error_send_message, do: ErrorUtil.init(ErrorUtil.error_message_sender, 0, "impossible to send message")
  def init_error_user_not_grant, do: ErrorUtil.init(ErrorUtil.error_message_sender, 1, "not grant privilege")
  def init_error_room_not_found, do: ErrorUtil.init(ErrorUtil.error_message_sender, 2, "room not found")
  def init_error_user_not_found, do: ErrorUtil.init(ErrorUtil.error_message_sender, 3, "user not found")
  def init_error_param_invalid, do: ErrorUtil.init(ErrorUtil.error_message_sender, 4, "params invalid")

end
