defmodule PandaPhoenix.ErrorChangeset do
  @moduledoc false
  
  alias PandaPhoenix.ErrorUtil

  def init_error(map), do: ErrorUtil.init(:changeset, ErrorUtil.error_changeset, 0, "Parameter Error", map)

end
