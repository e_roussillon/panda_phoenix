defmodule PandaPhoenix.ErrorLocalization do
  @moduledoc false
  
  alias PandaPhoenix.ErrorUtil

  def init_error_empty, do: ErrorUtil.init(ErrorUtil.error_localization, 0, "result empty")
  def init_error_country_not_found, do: ErrorUtil.init(ErrorUtil.error_localization, 1, "country is not found")
  def init_error_country_not_mapped, do: ErrorUtil.init(ErrorUtil.error_localization, 2, "country is not mapped")

end
