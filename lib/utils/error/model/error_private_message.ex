defmodule PandaPhoenix.ErrorPrivateMessage do
  @moduledoc false
  
  alias PandaPhoenix.ErrorUtil

  def init_error_message_empty, do: ErrorUtil.init(ErrorUtil.error_private_message, 0, "message empty")
  def init_error_room_invalid, do: ErrorUtil.init(ErrorUtil.error_private_message, 1, "room is invalid")
  def init_error_message_blocked, do: ErrorUtil.init(ErrorUtil.error_private_message, 3, "message blocked")
  def init_error_message_not_found, do: ErrorUtil.init(ErrorUtil.error_private_message, 4, "message not found")

end
