defmodule PandaPhoenix.ErrorDB do
  @moduledoc false
  
  alias PandaPhoenix.ErrorUtil

  def init_error, do: ErrorUtil.init(ErrorUtil.error_db, 0, "Request SQL")

end
