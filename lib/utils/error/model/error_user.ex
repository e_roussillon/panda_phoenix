defmodule PandaPhoenix.ErrorUser do
  @moduledoc false

  alias PandaPhoenix.ErrorUtil

  def init_error_user_not_found, do: ErrorUtil.init(ErrorUtil.error_user, 0, "user not found")
  def init_error_user_email_not_valid, do: ErrorUtil.init(ErrorUtil.error_user, 1, "email not valid")

end
