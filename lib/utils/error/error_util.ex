defmodule PandaPhoenix.ErrorUtil do
  @moduledoc false

  def init(type, code, message) do
    %{type: type, code: code, msg: message}
  end

  def init(:changeset, type, code, message, fields) do
    %{type: type, code: code, msg: message, fields: fields}
  end

  def init(:details, type, code, message, details) do
    %{type: type, code: code, msg: message, details: details}
  end

  #
  # NUMBER
  #

  def error_db, do: 0
  def error_changeset, do: 1
  def error_localization, do: 2
  def error_public_room, do: 3
  def error_auth, do: 4
  def error_user_friendship, do: 5
  def error_private_message, do: 6
  def error_message_sender, do: 7
  def error_user, do: 8

end
