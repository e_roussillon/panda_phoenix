defmodule PandaPhoenix do
  @moduledoc false
  
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Start the endpoint when the application starts
      supervisor(PandaPhoenix.Endpoint, []),
      # Start the Ecto repository
      supervisor(PandaPhoenix.Repo, []),
      # Here you could define other workers and supervisors as children
      # worker(PandaPhoenix.Worker, [arg1, arg2, arg3]),
      worker(PandaPhoenix.Watcher.Online.OnlineWatcher, []),
      worker(PandaPhoenix.Watcher.Channel.ChannelWatcher, []),
      worker(PandaPhoenix.Watcher.Reported.ReportedWatcher, []),

    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: PandaPhoenix.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PandaPhoenix.Endpoint.config_change(changed, removed)
    :ok
  end
end
