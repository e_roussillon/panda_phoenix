defmodule PandaPhoenix.Endpoint do
  @moduledoc false

  use Phoenix.Endpoint, otp_app: :panda_phoenix

  socket "/socket", PandaPhoenix.UserSocket

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phoenix.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/", from: :panda_phoenix, gzip: false,
    only: ~w(css fonts images js)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Poison,
    read_timeout: 120_000

  plug Plug.MethodOverride
  plug Plug.Head

  plug Plug.Session,
    store: :cookie,
    key: "_panda_phoenix_key",
    signing_salt: "D6XZmhTd"

  plug PandaPhoenix.Router


  # Bug with Poison -> unable to encode value: {nil, "XXXX"}
  # defimpl Poison.Encoder, for: Any do
  #   def encode(%{__struct__: _} = struct, options) do
  #     map = struct
  #           |> Map.from_struct
  #           |> sanitize_map
  #     Poison.Encoder.Map.encode(map, options)
  #   end
  #
  #   defp sanitize_map(map) do
  #     Map.drop(map, [:__meta__, :__struct__])
  #   end
  # end
end
