defmodule PandaPhoenix.Watcher.Channel.UserSocketWatcher do
  @moduledoc false

  alias PandaPhoenix.Logger
  # alias PandaPhoenix.UserAuthenticationController

  def demonitor(%{user_id: user_id}) do
    Logger.debug(__ENV__, "Demonitor user", [%{"user_id" => user_id}])
  end

  def monitor(%{user_id: user_id}) do
    Logger.debug(__ENV__, "Monitor user", [%{"user_id" => user_id}])
  end

end
