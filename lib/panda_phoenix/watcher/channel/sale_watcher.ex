defmodule PandaPhoenix.Watcher.Channel.SaleWatcher do
  @moduledoc false

  alias PandaPhoenix.Logger

  def demonitor(%{user_id: user_id, room_id: _, room: _}) do
    Logger.debug(__ENV__, "Demonitor user", [%{"user_id" => user_id}])
  end

  def monitor(%{user_id: user_id, room_id: _, room: _}) do
    Logger.debug(__ENV__, "Monitor user", [%{"user_id" => user_id}])
  end

end
