defmodule PandaPhoenix.Watcher.Channel.GeneralWatcher do
  @moduledoc false

  alias PandaPhoenix.Logger
  alias PandaPhoenix.UserAuthenticationController

  def demonitor(%{user_id: user_id, room_id: _, room: _}) do
    # UserAuthenticationController.update_auth_user(user_id, %{room: nil})
    Logger.debug(__ENV__, "Demonitor user", [%{"user_id" => user_id}])
  end

  def monitor(%{user_id: user_id, room_id: room_id, room: _}) do
    UserAuthenticationController.update_auth_user(user_id, %{room: "#{room_id}"})
    Logger.debug(__ENV__, "Monitor user", [%{"user_id" => user_id}])
  end

end
