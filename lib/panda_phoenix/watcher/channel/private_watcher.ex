defmodule PandaPhoenix.Watcher.Channel.PrivateWatcher do
  @moduledoc false

  alias PandaPhoenix.Logger
  alias PandaPhoenix.UserAuthenticationController
  alias PandaPhoenix.Watcher.Online.OnlineWatcher

  def demonitor(%{user_id: user_id}) do
    UserAuthenticationController.update_auth_user(user_id, %{last_seen: Ecto.DateTime.utc, is_online: false})
    OnlineWatcher.disconnect(user_id)
    Logger.debug(__ENV__, "Demonitor user", [%{"user_id" => user_id}])
  end

  def monitor(%{user_id: user_id}) do
    UserAuthenticationController.update_auth_user(user_id, %{is_online: true})
    OnlineWatcher.connect(user_id)
    Logger.debug(__ENV__, "Monitor user", [%{"user_id" => user_id}])
  end

end
