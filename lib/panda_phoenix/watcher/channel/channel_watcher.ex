defmodule PandaPhoenix.Watcher.Channel.ChannelWatcher do
  @moduledoc false

  use GenServer
  alias PandaPhoenix.Logger

  ## Client API

  def monitor(pid, ma) do
    GenServer.call(__MODULE__, {:monitor, pid, ma})
  end

  # def demonitor(pid) do
  #   GenServer.call(__MODULE__, {:demonitor, pid})
  # end

  def demonitor_uid(module, uid) do
    GenServer.call(__MODULE__, {:demonitor_uid, uid, module})
  end

  def get_uid(module, uid) do
    GenServer.call(__MODULE__, {:get, uid, module})
  end

  ## Server API

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    Logger.debug("#{__MODULE__} -> init")
    Process.flag(:trap_exit, true)
    {:ok, %{channels: Map.new()}}
  end

  def handle_call({:monitor, pid, ma}, _from, state) do
    {:reply, :ok, put_channel(state, pid, ma)}
  end

  # def handle_call({:demonitor, pid}, _from, state) do
  #   drop_channel(state, pid)
  # end

  def handle_call({:demonitor_uid, uid, module}, _from, state) do
    case find_user_id(state, Map.keys(state.channels), uid, module) do
      :not_found ->
        {:reply, :not_found, state}
      {:ok, pid} ->
        {:reply, :ok, drop_channel(state, pid)}
    end
  end

  def handle_call({:get, uid, module}, _from, state) do
    case find_user_id(state, Map.keys(state.channels), uid, module) do
      :not_found ->
        {:reply, :not_found, state}
      {:ok, _pid} ->
        {:reply, :ok, state}
    end
  end

  # def handle_call({:checkout, _, _, time}, _from, state) do
  #   {:noreply, %{channels: HashDict.new()}}
  # end

  # def handle_call({:find_pid, uid, module}, _from, state) do
  #   case find_user_id(state, HashDict.keys(state.channels), uid, module) do
  #     :not_found ->
  #       {:reply, {:not_found}, state}
  #     {:ok, pid} ->
  #       {:reply, {:ok, pid}, state}
  #   end
  # end

  def handle_info({:EXIT, pid, _}, state) do
    if Application.get_env(:panda_phoenix, :environment) != :test do
      {:noreply, drop_channel(state, pid)}
    else
      {:noreply, %{state | channels: Map.delete(state.channels, pid)}}
    end
  end

  #
  # PRIVATE
  #

  defp find_user_id(_state, [], _uid, _module) do
    :not_found
  end

  defp find_user_id(state, [h|t], uid, module) do
    case Map.fetch(state.channels, h) do
      :error ->
        :not_found
      {:ok, {mod, args}} ->
        if mod == module do
          case args do
            %{user_id: user_id, room_id: _room_id, room: _public_room} when user_id == uid ->
              {:ok, h}
            %{user_id: user_id} when user_id == uid ->
              {:ok, h}
            _ ->
              find_user_id(state, t, uid, module)
          end
        else
          find_user_id(state, t, uid, module)
        end
    end
  end

  defp drop_channel(state, pid) do
    case Map.fetch(state.channels, pid) do
      :error ->
        state
      {:ok, {mod, args}} ->
        Process.unlink(pid)
        Task.start_link(fn -> apply(mod, :demonitor, [args]) end)
        %{state | channels: Map.delete(state.channels, pid)}
    end
  end

  defp put_channel(state, pid, {mod, args}) do
    Process.link(pid)
    Task.start_link(fn -> apply(mod, :monitor, [args]) end)
    %{state | channels: Map.put(state.channels, pid, {mod, args})}
  end
end
