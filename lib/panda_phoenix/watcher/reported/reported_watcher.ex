defmodule PandaPhoenix.Watcher.Reported.ReportedWatcher do
  @moduledoc false

  use GenServer

  alias PandaPhoenix.Logger
  alias PandaPhoenix.DateUtil
  alias PandaPhoenix.UserReportedController
  alias PandaPhoenix.PrivateChannel
  alias PandaPhoenix.Math

  @time_sec_force_logout Application.get_env(:panda_phoenix, :time_sec_force_logout)
  @time_sec_force_logout_duplicat Application.get_env(:panda_phoenix, :time_sec_force_logout_duplicat)
  @time_sec_by_report Application.get_env(:panda_phoenix, :time_sec_by_report)
  @time_click_max_report Application.get_env(:panda_phoenix, :time_click_max_report)

  ## Client API

  def add(user_reporte_id) do
    GenServer.cast(__MODULE__, {:add, user_reporte_id})
  end

  # TODO:
  # (hard-deprecated in elixir_dispatch)
  def get(user_id, force_disconnect, _can_active \\false) do
    GenServer.call(__MODULE__, {:get, user_id, force_disconnect})
  end

  def is_active?(user_id) do
    GenServer.call(__MODULE__, {:is_active, user_id})
  end

  def force_disconnect_duplicat(user_id) do
    GenServer.cast(__MODULE__, {:force_disconnect_duplicat, user_id})
  end

  ## Server API

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    Logger.debug("#{__MODULE__} -> init")
    {:ok, %{}}
  end

  ## call

  def handle_call({:get, user_id, force_disconnect}, _from, state) do
    result =
      case UserReportedController.show(user_id) do
      :error ->
        :ok
      {:ok, user_reported} ->
        Logger.info(__ENV__, "user", [%{user_reported: user_reported}])
        case check_is_report_is_valid(%{user_reported: user_reported, force_disconnect: force_disconnect}) do
          :ok ->
            :ok
          {:error, time} ->
            {:error, time}
        end
    end
    {:reply, result, state}
  end

  def handle_call({:is_active, user_id}, _from, state) do
    result =
      case UserReportedController.show(user_id) do
        {:ok, user_reported} ->
          user_reported.is_used == false
        :error ->
          :false
      end
    {:reply, result, state}
  end

  ## cast

  def handle_cast({:add, user_reporte_id}, state) do
    case UserReportedController.user_reported(user_reporte_id) do
      {:ok, user_reported} ->
        original_count = div(user_reported.count, @time_click_max_report)
        new_count = div((user_reported.count + 1), @time_click_max_report)
        case new_count > original_count do
          true ->
              case UserReportedController.update_report(user_reported, %{count: (user_reported.count + 1), is_active: true}) do
                {:ok, user_reported} ->
                  force_disconnect_time(%{user_id: user_reported.user_id, count: (user_reported.count), force_disconnect: true})
                _other ->
                  :error
              end

          false ->
              case user_reported.is_used do
                true ->
                  # we need to check if the use have been report after be reconnected
                  # he can be disconnected if he is reported before the time he have been disconnected
                  # ex: he is disconected for 10sec, can not be reported for 20sec after last report
                  # ex: he is disconected for 20sec, can not be reported for 40sec after last report
                  timestamp = DateUtil.timestamp - DateUtil.ecto_to_timestamp(user_reported.updated_at)
                  time_be_out = (@time_sec_by_report * Math.pow(2, (new_count-1))) * 2
                  if timestamp >= time_be_out do
                    # {:ok, user_reported} =
                    UserReportedController.update_report(user_reported, %{count: (user_reported.count + 1), is_active: false})
                  else
                    new_count_big = (new_count + 1) * @time_click_max_report
                    case UserReportedController.update_report(user_reported, %{count: new_count_big, is_used: false, is_active: true}) do
                      {:ok, user_reported} ->
                        force_disconnect_time(%{user_id: user_reported.user_id, count: user_reported.count, force_disconnect: true})
                      _other ->
                        :error
                    end
                  end

                false ->
                  case user_reported.is_active do
                    false ->
                      # {:ok, user_reported}
                      UserReportedController.update_report(user_reported, %{count: (user_reported.count + 1), is_active: false})
                    true ->
                      # The user is already disconected. we need to wait he reconnect to add more report
                      :ok
                  end
              end
        end
      _other ->
        :error
    end
    {:noreply, state}
  end

  def handle_cast({:force_disconnect_duplicat, user_id}, state) do
    PandaPhoenix.Endpoint.broadcast_from(self(), "private_channel:#{user_id}", PrivateChannel.channel_name(:duplicat), %{})
    Process.send_after(self(), {:force_disconnect, user_id}, @time_sec_force_logout_duplicat * 1000)
    {:noreply, state}
  end

  ## info

  def handle_info({:force_disconnect, user_id}, state) do
    PandaPhoenix.Endpoint.broadcast_from(self(), "users_socket:" <> "#{user_id}", "disconnect",  %{})
    {:noreply, state}
  end

  def handle_info({:force_disconnect_user, user_id}, state) do
    force_disconnect(user_id, %{time: @time_sec_by_report})
    {:noreply, state}
  end

  def handle_info(_other, state) do
    {:noreply, state}
  end

  #
  # PRIVATE
  #

  defp force_disconnect(user_id, %{time: time}) do
      PandaPhoenix.Endpoint.broadcast_from(self(), "private_channel:#{user_id}", PrivateChannel.channel_name(:reported), %{time: time})
      Process.send_after(self, {:force_disconnect, user_id}, @time_sec_force_logout * 1000)
  end

  defp check_is_report_is_valid(%{user_reported: user_reported, force_disconnect: force_disconnect}) do
    case user_reported.is_used do
      true ->
        :ok
      false ->
        case user_reported.is_active do
          true ->
            timestamp = DateUtil.timestamp - DateUtil.ecto_to_timestamp(user_reported.updated_at)
            new_count = div(user_reported.count, @time_click_max_report)
            time_be_out = @time_sec_by_report * Math.pow(2, (new_count-1))
            if timestamp >= time_be_out do
              case UserReportedController.update_report(user_reported, %{count: user_reported.count, is_used: true, is_active: false}) do
                {:ok, _} ->
                  :ok
                {:error, _} ->
                  {:error, force_disconnect_time(%{user_id: user_reported.user_id, count: @time_click_max_report, force_disconnect: force_disconnect})}
              end
            else
              {:error, force_disconnect_time(%{user_id: user_reported.user_id, time: time_be_out - timestamp, force_disconnect: force_disconnect})}
            end
          false ->
            :ok
        end
    end
  end

  defp force_disconnect_time(%{user_id: user_id, count: count, force_disconnect: force_disconnect}) do
    new_count = div(count, @time_click_max_report)
    time_sec = @time_sec_by_report * Math.pow(2, (new_count-1))
    force_disconnect_time(%{user_id: user_id, time: time_sec, force_disconnect: force_disconnect})
  end

  defp force_disconnect_time(%{user_id: user_id, time: time, force_disconnect: force_disconnect}) do
    response = %{time: time}
    if force_disconnect do
      force_disconnect(user_id, response)
    end
    response
  end

end
