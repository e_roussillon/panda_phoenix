defmodule PandaPhoenix.Watcher.Online.OnlineWatcher do
  @moduledoc false
  
  use GenServer

  alias PandaPhoenix.Logger
  alias PandaPhoenix.Repo
  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.DateUtil
  alias PandaPhoenix.PrivateChannel

  ## Client API

  def connect(user_id) do
    GenServer.cast(__MODULE__, {:connect, user_id})
  end

  def disconnect(user_id) do
    GenServer.cast(__MODULE__, {:disconnect, user_id})
  end

  ## Server API

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    Logger.debug("#{__MODULE__} -> init")
    {:ok, nil}
  end

  def handle_cast({:connect, user_id}, state) do
    send_broadcast(user_id, :connect)
    {:noreply, state}
  end

  def handle_cast({:disconnect, user_id}, state) do
    send_broadcast(user_id, :disconnect)
    {:noreply, state}
  end

  #
  # PRIVATE
  #

  defp send_broadcast(user_id, chat) do
    case Repo.all(UserFriendship.query_user_friendships_online_accepted(user_id)) do
      [h|t] ->
        list = PandaPhoenix.UserView.render("index.json", %{users: [h|t]})

        if chat == :connect do
          PandaPhoenix.Endpoint.broadcast_from(self(), "private_channel:#{user_id}", PrivateChannel.channel_name(:connect_list), %{list: list})
        end

        Enum.each list, fn user ->
          PandaPhoenix.Endpoint.broadcast_from(self(), "private_channel:#{user.id}", PrivateChannel.channel_name(chat), %{user_id: user_id, last_seen: DateUtil.timestamp})
        end
        :ok
      _ ->
        Logger.debug(__ENV__, "Send broadcast not send - do not have friend :(", [])
        :not_foud
    end
  end

end
