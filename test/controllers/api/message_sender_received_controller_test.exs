defmodule PandaPhoenix.MessageSenderReceivedControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.User
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.PrivateMessage
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateRoom


  @user1 %User{birthday: Ecto.Date.cast!("1990-04-17"), email: "dovi1@gmail.com", pseudo: "dovi1"}
  @user2 %User{birthday: Ecto.Date.cast!("1990-04-17"), email: "dovi2@gmail.com", pseudo: "dovi2"}
  @room %PrivateRoom{name: ""}

  @valid_attrs %{latitude: "120.5", longitude: "120.5", public_room_id: 42}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "send broadcase received with render success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id1 = user1.id
    token1 = Token.sign_user(user_id1)
    Repo.insert! %UserAuthentication{user_id: user_id1, token: token1}

    user2 = Repo.insert! @user2
    user_id2 = user2.id

    room1 = Repo.insert! @room
    Repo.insert! %PrivateRoomUser{room_id: room1.id, user_id: user_id1}
    Repo.insert! %PrivateRoomUser{room_id: room1.id, user_id: user_id2}
    message = Repo.insert! %PrivateMessage{room_id: room1.id, msg: "hello", from_user_id: user_id2, index_row: "index", status: PrivateMessage.message_received}

    #TODO - can catch Phoenix.Socket.Broadcast
    # room_name = "private_channel:" <> "#{user_id2}"
    # PandaPhoenix.Endpoint.subscribe(room_name)

    conn = conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token1)
      |> post("/api/v1/confirmation/messages", %{"messages" => [%{"id" => message.id, "status" => message.status}]})

    # :timer.sleep(2000)

    # assert_receive %Phoenix.Socket.Broadcast{event: "news:msg:received", payload: %{}}
    # PandaPhoenix.Endpoint.unsubscribe(room_name)

    assert response(conn, 200) == "{}"
  end

  test "send broadcase received with render error not found user id", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id1 = user1.id
    token1 = Token.sign_user(user_id1)
    Repo.insert! %UserAuthentication{user_id: user_id1, token: token1}

    user2 = Repo.insert! @user2
    user_id2 = user2.id

    room1 = Repo.insert! @room
    Repo.insert! %PrivateRoomUser{room_id: room1.id, user_id: user_id1}
    message = Repo.insert! %PrivateMessage{room_id: room1.id, msg: "hello", from_user_id: user_id2, index_row: "index"}

    #TODO - can catch Phoenix.Socket.Broadcast
    # room_name = "private_channel:" <> "#{user_id2}"
    # PandaPhoenix.Endpoint.subscribe(room_name)

    conn = conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token1)
      |> post("/api/v1/confirmation/messages", %{"messages" => [%{"id" => message.id, "status" => message.status}]})

    # :timer.sleep(2000)
    #
    # assert_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{}}
    # PandaPhoenix.Endpoint.unsubscribe(room_name)

    assert response(conn, 200) == "{}"
  end

  test "send broadcase received with render error", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id1 = user1.id
    token1 = Token.sign_user(user_id1)
    Repo.insert! %UserAuthentication{user_id: user_id1, token: token1}

    #TODO - can catch Phoenix.Socket.Broadcast
    # room_name = "private_channel:" <> "#{user_id2}"
    # PandaPhoenix.Endpoint.subscribe("private_channel:" <> "#{user_id2}")

    conn = conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token1)
      |> post("/api/v1/confirmation/messages", %{"messages" => [%{"id" => -1, "status" => -1}]})

    # :timer.sleep(2000)

    # assert_receive %Phoenix.Socket.Broadcast{event: "news:msg:received", payload: %{}}
    # PandaPhoenix.Endpoint.unsubscribe("private_channel:" <> "#{user_id2}")

    assert response(conn, 200) == "{}"
  end

end
