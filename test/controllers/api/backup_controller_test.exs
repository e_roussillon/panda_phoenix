defmodule PandaPhoenix.BackupControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.User
  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.DateUtil
  alias PandaPhoenix.UserFilter
  alias PandaPhoenix.PrivateRoom
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateMessage

  @user1 %User{birthday: Ecto.Date.cast!("1990-04-17"), email: "dovi1@gmail.com", gender: 1, pseudo: "dovi1"}
  @user2 %User{birthday: Ecto.Date.cast!("1990-04-17"), email: "dovi2@gmail.com", gender: 1, pseudo: "dovi2"}

  setup %{conn: conn} do
    conn = conn
        |> put_req_header("accept", "application/json")

    {:ok, conn: conn}
  end

  #
  # User
  #

  test "get backup user data render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  #
  # Friendship
  #

  test "get backup user data with friendship friendship_not_friend render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_not_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_not_friend (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_not_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_pending render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_pending, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 2
    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id}, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, 'dovi2', 1, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_pending (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_pending, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 2
    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id2}, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, 'dovi2', 1, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_cancel_request render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_cancel_request (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_cancel_request, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 2
    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id2}, NULL, 'dovi2', 2, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_refused_before_be_friend render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_refused_before_be_friend (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 2
    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id2}, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, 'dovi2', 3, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_blocked_before_be_friend render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 2
    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id}, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, 'dovi2', 4, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_blocked_before_be_friend (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_friend render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 2
    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id}, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, 'dovi2', 5, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_friend (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, last_seen: Ecto.DateTime.utc}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 2
    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id2}, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, 'dovi2', 5, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_refused_after_be_friend render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_refused_after_be_friend (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_blocked_after_be_friend render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 2
    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id}, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, 'dovi2', 7, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_blocked_after_be_friend (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_unblocked render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_unblocked, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with friendship friendship_unblocked (other user) render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_unblocked, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  #
  # Filter
  #

  test "get backup user data with filters render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    filter1 = Repo.insert! %UserFilter{name: "highlight", is_highlight: true, user_id: user_id}
    filter2 = Repo.insert! %UserFilter{name: "not highlight", is_highlight: false, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 3
    assert List.first(list) == "INSERT OR REPLACE INTO user_filters (user_filters_id, is_highlight, name, updated_at, user_id) VALUES (#{filter1.id}, 1, 'highlight', '#{Ecto.DateTime.to_string(filter1.updated_at)}', #{user_id});"
    assert Enum.at(list, 1) == "INSERT OR REPLACE INTO user_filters (user_filters_id, is_highlight, name, updated_at, user_id) VALUES (#{filter2.id}, 0, 'not highlight', '#{Ecto.DateTime.to_string(filter2.updated_at)}', #{user_id});"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with filters other user render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    Repo.insert! %UserFilter{name: "highlight", is_highlight: true, user_id: user_id2}
    Repo.insert! %UserFilter{name: "not highlight", is_highlight: false, user_id: user_id2}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 1
    assert List.first(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  #
  # Chat
  #

  test "get backup user data with chat but not friendship render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    private_room_user1 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    private_room_user2 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    private_message = Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: 0, from_user_id: user_id, type: 0, data_id: 0, index_row: "index"}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 6
    assert List.first(list) == "INSERT OR REPLACE INTO private_messages (data_id, from_user_id, private_messages_id, index_row, inserted_at, msg, room_id, status, type) VALUES (0, #{user_id}, #{private_message.id}, 'index', #{DateUtil.ecto_to_timestamp(private_message.inserted_at)}.0, 'hello', #{private_room.id}, 0, 0);"
    assert Enum.at(list, 1) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room.id}, '#{Ecto.DateTime.to_string(private_room_user1.updated_at)}', #{user_id});"
    assert Enum.at(list, 2) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room.id}, '#{Ecto.DateTime.to_string(private_room_user2.updated_at)}', #{user_id2});"
    assert Enum.at(list, 3) == "INSERT OR REPLACE INTO private_rooms (data_id, private_rooms_id, is_group, name, updated_at) VALUES (NULL, #{private_room.id}, 0, 'test', '#{Ecto.DateTime.to_string(private_room.updated_at)}');"
    assert Enum.at(list, 4) == "INSERT OR REPLACE INTO users (users_id, last_action, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, NULL, 'dovi2', NULL, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end

  test "get backup user data with chat and friendship render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    private_room_user1 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    private_room_user2 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    private_message = Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: 0, from_user_id: user_id, type: 0, data_id: 0, index_row: "index"}
    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/auth/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 6
    assert List.first(list) == "INSERT OR REPLACE INTO private_messages (data_id, from_user_id, private_messages_id, index_row, inserted_at, msg, room_id, status, type) VALUES (0, #{user_id}, #{private_message.id}, 'index', #{DateUtil.ecto_to_timestamp(private_message.inserted_at)}.0, 'hello', #{private_room.id}, 0, 0);"
    assert Enum.at(list, 1) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room.id}, '#{Ecto.DateTime.to_string(private_room_user1.updated_at)}', #{user_id});"
    assert Enum.at(list, 2) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room.id}, '#{Ecto.DateTime.to_string(private_room_user2.updated_at)}', #{user_id2});"
    assert Enum.at(list, 3) == "INSERT OR REPLACE INTO private_rooms (data_id, private_rooms_id, is_group, name, updated_at) VALUES (NULL, #{private_room.id}, 0, 'test', '#{Ecto.DateTime.to_string(private_room.updated_at)}');"
    assert Enum.at(list, 4) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id}, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, 'dovi2', 5, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert List.last(list) == "INSERT OR REPLACE INTO users (birthday, description, email, gender, users_id, pseudo, updated_at, url_blur, url_original, url_thumb) VALUES ('1990-04-17', NULL, 'dovi1@gmail.com', 1, #{user_id}, 'dovi1', #{DateUtil.ecto_to_timestamp(user1.updated_at)}.0, NULL, NULL, NULL);"
  end


end
