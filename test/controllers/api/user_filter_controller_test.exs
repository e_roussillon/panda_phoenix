defmodule PandaPhoenix.UserFilterControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.UserFilter
  alias PandaPhoenix.UserAuthentication

  @user_id 1
  @valid_attrs_inactive %{active: false, is_highlight: true, name: "some content", user_id: @user_id}
  @valid_attrs_active %{active: false, is_highlight: true, name: "some content", user_id: @user_id}
  @valid_attrs %{is_highlight: true, name: "some content", user_id: @user_id}
  @invalid_attrs %{}

  setup %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("authorization", "Bearer " <> token)

    {:ok, conn: conn}
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = conn
      |> post(user_filter_path(conn, :create), user_filter: @valid_attrs)

    assert json_response(conn, 201)["id"]
    assert Repo.get_by(UserFilter, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = conn
      |> post(user_filter_path(conn, :create), user_filter: @invalid_attrs)

    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    user_filter = Repo.insert! %UserFilter{}
    conn = put conn, user_filter_path(conn, :update, user_filter), user_filter: @valid_attrs
    assert json_response(conn, 200)["id"]
    assert Repo.get_by(UserFilter, @valid_attrs)
  end

  test "reactive and renders chosen resource when data is valid", %{conn: conn} do
    user_filter = Repo.insert! UserFilter.changeset(%UserFilter{}, @valid_attrs_inactive)
    conn = conn
        |> post(user_filter_path(conn, :create), user_filter: @valid_attrs)
    assert json_response(conn, 200)["id"] == user_filter.id
    assert Repo.get_by(UserFilter, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    user_filter = Repo.insert! %UserFilter{}
    conn = put conn, user_filter_path(conn, :update, user_filter), user_filter: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "does update chosen resource and renders errors when data is invalid", %{conn: conn} do
    user_filter = Repo.insert! %UserFilter{}
    conn = put conn, user_filter_path(conn, :update, user_filter), user_filter: @valid_attrs_active
    assert json_response(conn, 200)["id"]
    assert Repo.get_by(UserFilter, @valid_attrs_active)
  end

  test "deletes chosen resource", %{conn: conn} do
    user_filter = Repo.insert! UserFilter.changeset(%UserFilter{}, @valid_attrs_inactive)

    conn = delete conn, user_filter_path(conn, :delete, user_filter)
    assert response(conn, 200) == "{}"
  end

  test "can not deletes chosen resource", %{conn: conn} do
    user_filter = Repo.insert! %UserFilter{}
    conn = delete conn, user_filter_path(conn, :delete, user_filter)
    assert json_response(conn, 422) == %{"code" => 0,
    "fields" => [%{"field" => "user_id", "message" => ["can't be blank"]},
     %{"field" => "name", "message" => ["can't be blank"]}],
    "msg" => "Parameter Error", "type" => 1}
  end

end
