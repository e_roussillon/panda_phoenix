defmodule PandaPhoenix.UserFriendshipControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.Router
  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.User
  alias PandaPhoenix.UserAuthentication

  @opt Router.init([])

  @user_id 0
  @invalid_attrs_user %{to_user_id: @user_id}
  @invalid_attrs_status %{to_user_id: 2}
  @invalid_attrs %{}

  @user1 %User{birthday: Ecto.Date.cast!("2010-04-17"), email: "dovi@gmail.com", gender: 0, pseudo: "dovi"}
  @user2 %User{birthday: Ecto.Date.cast!("2010-04-17"), email: "dovi1@gmail.com", gender: 0, pseudo: "dovi1"}
  @user3 %User{birthday: Ecto.Date.cast!("2010-04-17"), email: "dovi2@gmail.com", gender: 0, pseudo: "dovi2"}

  @valid_attrs_create %{to_user_id: 2, status: 0}

  setup %{conn: conn} do
    {:ok, conn: conn}
  end

  test "does not create resource and renders errors when data is invalid", %{conn: _} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", @invalid_attrs)
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "to_user_id", "message" => ["can't be blank"]}, %{"field" => "status", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "does not create resource and renders errors when user be friend with it's self", %{conn: _} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", @invalid_attrs_user)
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "user_id", "message" => ["User cannot be friend with his self"]}, %{"field" => "status", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "does not create resource and renders errors when user ask friendship without status", %{conn: _} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", @invalid_attrs_status)
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "status", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "create resource and renders when user ask friendship with message", %{conn: _} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", @valid_attrs_create)
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "user not found", "type" => 8}
  end

  test "create resource and render error if send status 'friendship_not_friend' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "create resource and render error if send status 'friendship_cancel_request' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "create resource and render error if send status 'friendship_refused_before_be_friend' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "create resource and render error if send status 'friendship_friend' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "create resource and render error if send status 'friendship_refused_after_be_friend' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "create resource and render error if send status 'friendship_unblocked' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "create resource and render success if send status 'friendship_pending' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_pending
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "create resource and render success if send status 'friendship_blocked_before_be_friend' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "create resource and render error if send status 'friendship_blocked_after_be_friend' when user are not friend" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  #
  # Status friendship_not_friend
  #

  test "update resource and render error if status was 'friendship_not_friend' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_pending' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_cancel_request' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_before_be_friend' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_before_be_friend' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_not_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_not_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  #
  # Status friendship_pending
  #

  test "update resource and render success if status was 'friendship_not_friend' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_pending
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_pending' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render success if status was 'friendship_cancel_request' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_pending
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render success if status was 'friendship_refused_before_be_friend' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_pending
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_blocked_before_be_friend' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_pending'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_pending})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  #
  # Status friendship_cancel_request
  #

  test "update resource and render error if status was 'friendship_not_friend' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render success if status was 'friendship_pending' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_cancel_request
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render success if status was 'friendship_cancel_request' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

      assert json_response(conn, 201)["friendship_last_action"] == @user_id
      assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
      assert json_response(conn, 201)["id"] == user.id
      assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render success if status was 'friendship_refused_before_be_friend' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_blocked_before_be_friend' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_cancel_request'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_cancel_request})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  #
  # Status friendship_refused_before_be_friend
  #

  test "update resource and render error if status was 'friendship_not_friend' and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render success if status was 'friendship_pending' and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]

  end

  test "update resource and render error if status was 'friendship_cancel_request' and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_before_be_friend' and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render success if status was 'friendship_refused_before_be_friend' set by other user and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: user.id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

      assert json_response(conn, 201)["friendship_last_action"] == @user_id
      assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
      assert json_response(conn, 201)["id"] == user.id
      assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_refused_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  #
  # Status friendship_blocked_before_be_friend
  #

  test "update resource and render error if status was 'friendship_not_friend' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_pending' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_cancel_request' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_refused_before_be_friend' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_blocked_before_be_friend' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)


    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_blocked_before_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_before_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  #
  # Status friendship_friend
  #

  test "update resource and render error if status was 'friendship_not_friend' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_pending' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_cancel_request' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_before_be_friend' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_before_be_friend' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  #
  # Status friendship_refused_after_be_friend
  #

  test "update resource and render error if status was 'friendship_not_friend' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_pending' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_cancel_request' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_refused_before_be_friend' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_blocked_before_be_friend' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_refused_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_refused_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  #
  # Status friendship_blocked_after_be_friend
  #

  test "update resource and render error if status was 'friendship_not_friend' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_pending' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_cancel_request' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_refused_before_be_friend' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_blocked_before_be_friend' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_blocked_after_be_friend'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_blocked_after_be_friend})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_after_be_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  #
  # Status friendship_unblocked
  #

  test "update resource and render error if status was 'friendship_not_friend' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_not_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_pending' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_pending, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_cancel_request' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_before_be_friend' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_before_be_friend' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_not_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_friend' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_refused_after_be_friend' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 201)["friendship_last_action"] == @user_id
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_friend
    assert json_response(conn, 201)["id"] == user.id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "update resource and render error if status was 'friendship_unblocked' and user send status 'friendship_unblocked'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_unblocked, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: UserFriendship.friendship_unblocked})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  #
  # status other
  #

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status '-1'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: -1})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  test "update resource and render error if status was 'friendship_blocked_after_be_friend' and user send status '9'" do
    user = Repo.insert! %User{}
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    Repo.insert! %UserFriendship{last_action: @user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user.id, user_id: @user_id}

    conn = build_conn(:post, "/api/v1/update_friendship", %{to_user_id: user.id, status: 9})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    assert json_response(conn, 422) == %{"code" => 0, "msg" => "action impossible", "type" => 5}
  end

  #
  # Broacast message
  #

  test "update resource and render success with a boardcast", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    conn = conn
            |> put_req_header("authorization", "Bearer " <> token2)
            |> put_req_header("accept", "application/json")
            |> post("/api/v1/update_friendship", %{to_user_id: user_id, status: UserFriendship.friendship_blocked_before_be_friend})

    assert json_response(conn, 201)["friendship_last_action"] == user_id2
    assert json_response(conn, 201)["friendship_status"] == UserFriendship.friendship_blocked_before_be_friend
    assert json_response(conn, 201)["id"] == user_id
    assert Map.keys(json_response(conn, 201)) == ["birthday", "description", "friendship_last_action", "friendship_status", "gender", "id", "is_online", "last_seen", "pseudo", "reported", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  #
  # Find user
  #

  test "find user render error with invalid param", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
            |> put_req_header("authorization", "Bearer " <> token)
            |> put_req_header("accept", "application/json")
            |> post("/api/v1/find_friendships", %{"pseudo" => "", "page" => 0})

    assert json_response(conn, 422) == %{"code" => 1, "msg" => "page have to start at minimum 1", "type" => 8}
  end

  test "find user render success with valid param", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    Repo.insert! @user2
    Repo.insert! @user3

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when one of the user if friend of the other", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_pending, to_user_id: user2.id, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 1, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi2"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user are friend but not the current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    user2 = Repo.insert! @user2
    user3 = Repo.insert! @user3
    user_id3 = user3.id
    token3 = Token.sign_user(user_id3)
    Repo.insert! %UserAuthentication{user_id: user_id3, token: token3}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_pending, to_user_id: user2.id, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token3)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  #
  # ERROR
  #
  test "find user render success when other user are 'friendship_blocked_before_be_friend' but not the current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    user3 = Repo.insert! @user3
    user_id3 = user3.id
    token3 = Token.sign_user(user_id3)
    Repo.insert! %UserAuthentication{user_id: user_id3, token: token3}

    {:ok, _} = Repo.insert %UserFriendship{last_action: user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token3)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user are 'friendship_blocked_after_be_friend' but not the current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    user3 = Repo.insert! @user3
    user_id3 = user3.id
    token3 = Token.sign_user(user_id3)
    Repo.insert! %UserAuthentication{user_id: user_id3, token: token3}

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token3)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == 0
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when current user blocked 'friendship_blocked_before_be_friend' other user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 1, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi2"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user blocked 'friendship_blocked_before_be_friend' current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_blocked_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 1, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi2"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when current user blocked 'friendship_blocked_after_be_friend' other user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 1, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi2"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user blocked 'friendship_blocked_after_be_friend' current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_blocked_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 1, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi2"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when current user blocked 'friendship_friend' other user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 1, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi2"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user blocked 'friendship_friend' current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 1, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == nil
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi2"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when current user blocked 'friendship_not_friend' other user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_not_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user blocked 'friendship_not_friend' current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_not_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id2
    assert first["friendship_status"] == UserFriendship.friendship_not_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when current user blocked 'friendship_cancel_request' other user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_cancel_request, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id
    assert first["friendship_status"] == UserFriendship.friendship_cancel_request
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user blocked 'friendship_cancel_request' current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_cancel_request, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id2
    assert first["friendship_status"] == UserFriendship.friendship_cancel_request
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when current user blocked 'friendship_refused_before_be_friend' other user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id
    assert first["friendship_status"] == UserFriendship.friendship_refused_before_be_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user blocked 'friendship_refused_before_be_friend' current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_refused_before_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id2
    assert first["friendship_status"] == UserFriendship.friendship_refused_before_be_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when current user blocked 'friendship_refused_after_be_friend' other user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id
    assert first["friendship_status"] == UserFriendship.friendship_refused_after_be_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user blocked 'friendship_refused_after_be_friend' current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_refused_after_be_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id2
    assert first["friendship_status"] == UserFriendship.friendship_refused_after_be_friend
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when current user blocked 'friendship_unblocked' other user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_unblocked, to_user_id: user_id2, user_id: user_id}

    # Create user 3
    build_conn(:post, user_path(conn, :create), user: %{birthday: "2010-04-17", email: "dovi2@gmail.com", gender: true, pseudo: "dovi2"})
              |> put_req_header("accept", "application/json")
              |> put_req_header("password", "123456")
              |> Router.call(@opt)

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id
    assert first["friendship_status"] == UserFriendship.friendship_unblocked
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

  test "find user render success when other user blocked 'friendship_unblocked' current user", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    user2 = Repo.insert! @user2
    user_id2 = user2.id
    Repo.insert! @user3

    Repo.insert! %UserFriendship{last_action: user_id2, status: UserFriendship.friendship_unblocked, to_user_id: user_id2, user_id: user_id}

    conn = conn
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer " <> token)
            |> post("/api/v1/find_friendships", %{"pseudo" => "dov", "page" => 1})

    %{"entries" => list, "page_number" => 1, "page_size" => 30, "total_entries" => 2, "total_pages" => 1} = json_response(conn, 200)
    first = List.first(list)
    assert first["friendship_last_action"] == user_id2
    assert first["friendship_status"] == UserFriendship.friendship_unblocked
    assert first["is_online"] == false
    assert first["pseudo"] == "dovi1"
    assert first["url_blur"] == nil
    assert first["url_original"] == nil
    assert first["url_thumb"] == nil
    assert Map.keys(first) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]

    last = List.last(list)
    assert last["friendship_last_action"] == nil
    assert last["friendship_status"] == UserFriendship.friendship_not_friend
    assert last["is_online"] == false
    assert last["pseudo"] == "dovi2"
    assert last["url_blur"] == nil
    assert last["url_original"] == nil
    assert last["url_thumb"] == nil
    assert Map.keys(last) ==  ["friendship_last_action", "friendship_status", "id", "is_online", "last_seen", "pseudo", "updated_at", "url_blur", "url_original", "url_thumb"]
  end

end
