defmodule PandaPhoenix.PublicRoomLocalizationControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.Router
  alias PandaPhoenix.PublicRoomLocalization
  alias PandaPhoenix.PublicRoom
  alias PandaPhoenix.PublicRoomLocalization
  alias PandaPhoenix.UserAuthentication

  @opt Router.init([])
  @user_id 1
  @valid_attrs %{"latitude" => "-23.526286", "longitude" => "-46.196609"}
  @invalid_attrs %{}
  @invalid_attrs1 %{"latitude" => "-32.368848", "longitude" => "-11.575980"}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "render error when data is invalid", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/find_room", @invalid_attrs)

    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "longitude", "message" => ["can't be blank"]}, %{"field" => "latitude", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "render success when data is valid", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/find_room", @valid_attrs)

    %{"rooms" => rooms, "token" => token} = assert json_response(conn, 200)

    rooms = Enum.map(rooms, fn(room) -> for {key, val} <- room, into: %{}, do: {String.to_atom(key), val} end)
    assert length(rooms) == 8
    {:authenticated, public_room} = PandaPhoenix.Token.verify_public_room(token)
    assert rooms == public_room
  end

  test "render error when data is invalid formate", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/find_room", @valid_attrs)

    %{"rooms" => _, "token" => _} = assert json_response(conn, 200)

    conn = build_conn(:post, "/api/v1/find_room", @valid_attrs)
              |> put_req_header("accept", "application/json")
              |> put_req_header("authorization", "Bearer " <> token)
              |> Router.call(@opt)

    %{"rooms" => rooms, "token" => token} = assert json_response(conn, 200)

    assert (from u in PublicRoomLocalization) |> Repo.all |> length == 1
    assert (from u in PublicRoom) |> Repo.all |> length == 8

    rooms = Enum.map(rooms, fn(room) -> for {key, val} <- room, into: %{}, do: {String.to_atom(key), val} end)
    assert length(rooms) == 8
    {:authenticated, public_room} = PandaPhoenix.Token.verify_public_room(token)
    assert rooms == public_room
  end

  test "render error when data is invalid geo", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/find_room", @invalid_attrs1)

    assert json_response(conn, 404) == %{"type" => 2,"msg" => "country is not mapped", "code" => 2}

    conn = build_conn(:post, "/api/v1/find_room", @invalid_attrs1)
              |> put_req_header("accept", "application/json")
              |> put_req_header("authorization", "Bearer " <> token)
              |> Router.call(@opt)
    assert json_response(conn, 404) == %{"type" => 2,"msg" => "country is not mapped", "code" => 2}
  end

end
