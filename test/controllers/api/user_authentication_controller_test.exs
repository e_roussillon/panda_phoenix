defmodule PandaPhoenix.UserAuthenticationControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.Router
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.UserAuthenticationController
  alias PandaPhoenix.User
  alias PandaPhoenix.UserPassword
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.UserReportedController
  alias PandaPhoenix.Watcher.Reported.ReportedWatcher

  @psw "123456"
  @invalid_psw "1234"
  @email "dovi@gmail.com"
  @token_fb "CAACxWMIMcZCIBANPUqTJIafPhVImMqGmVaiWL5v6lMbGkrL3GnS76Dzkyd4scVyrkhfvPbpo1xS3DCW3AZCE9hmwhjfhiXGZAzTZAChNNPjmmIDDV4ZBjLCK2wl6rAD3OW6HBGMqUrPV8kFT9XEkb7WHIj3uXqrdgKYzvn5AoLcvPxoX6FGBXC9RZB9DbmLKWBAKZA4FfBMU2HCoMjKbjX81fRNOMVOvAkXsZBd3S4Dwl79ZCi7a353eflKQ6qS86FdtwHOcw9ahFuQZDZD"
  @token_fb_invalid "CAACxWMIMcZCIBANPUq"

  @opt Router.init([])
  @valid_user_attrs %{birthday: Ecto.Date.cast!("1990-04-17"), email: @email, pseudo: "qwerty"}
  @invalid_attrs %{}
  @valid_attrs_auth %{is_online: true, room: "some content", refresh_token: "some content", token: "some content", user_id: 42}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "does not login and renders errors when user not register", %{conn: conn} do
    assert conn
            |> put_req_header("password", @psw)
            |> post("/api/v1/auth", @valid_user_attrs)
            |> json_response(422) == %{"type" => 4,"msg" => "email or password incorrect", "code" => 0}
  end

  test "does not login and renders errors when the password is not good", %{conn: conn} do
    user = Repo.insert! %User{birthday: Ecto.Date.cast!("1990-04-17"), email: @email, pseudo: "qwerty"}
    Repo.insert! UserPassword.changeset(%UserPassword{}, %{"user_id" => user.id, "password" => @psw})
    Repo.insert! %UserAuthentication{user_id: user.id}

    assert conn
            |> put_req_header("password", @invalid_psw)
            |> post("/api/v1/auth", @valid_user_attrs)
            |> json_response(422) == %{"type" => 4,"msg" => "email or password incorrect", "code" => 0}
  end

  test "does not login and renders errors when user is reported but wrong password", %{conn: conn} do
    user = Repo.insert! %User{birthday: Ecto.Date.cast!("1990-04-17"), email: @email, pseudo: "qwerty"}
    Repo.insert! UserPassword.changeset(%UserPassword{}, %{"user_id" => user.id, "password" => @psw})
    Repo.insert! %UserAuthentication{user_id: user.id}
    {:ok, _ } = UserReportedController.user_reported(user.id)

    assert ReportedWatcher.is_active?(user.id) == true

    assert conn
            |> put_req_header("password", @invalid_psw)
            |> post("/api/v1/auth", @valid_user_attrs)
            |> json_response(422) == %{"type" => 4,"msg" => "email or password incorrect", "code" => 0}
  end

  test "does not login and renders errors when user is reported", %{conn: conn} do
    user = Repo.insert! %User{birthday: Ecto.Date.cast!("1990-04-17"), email: @email, pseudo: "qwerty"}
    Repo.insert! UserPassword.changeset(%UserPassword{}, %{"user_id" => user.id, "password" => @psw})
    Repo.insert! %UserAuthentication{user_id: user.id}
    ReportedWatcher.add(user.id)

    assert ReportedWatcher.is_active?(user.id) == true

    {:ok, _ } = UserReportedController.user_reported(user.id)
    {:error, %{time: time}} = ReportedWatcher.get(user.id, true, true)

    %{"type" => 4, "msg" => "user reported", "details" => %{"time" => val_time}, "code" => 4} = conn
            |> put_req_header("password", @psw)
            |> post("/api/v1/auth", @valid_user_attrs)
            |> json_response(401)
    assert ((time - val_time) <= 2) == true
  end

  test "does login and renders success when user is reported but time is over", %{conn: conn} do
    user = Repo.insert! %User{birthday: Ecto.Date.cast!("1990-04-17"), email: @email, pseudo: "qwerty", active: true}
    Repo.insert! UserPassword.changeset(%UserPassword{}, %{"user_id" => user.id, "password" => @psw})
    Repo.insert! %UserAuthentication{user_id: user.id}
    ReportedWatcher.add(user.id)

    {:error, _} = ReportedWatcher.get(user.id, true, true)
    assert ReportedWatcher.is_active?(user.id) == true

    Repo
      |> Ecto.Adapters.SQL.query("UPDATE public.user_reported SET updated_at = '2016-08-07 15:04:19' WHERE user_id = #{user.id};")

    assert conn
            |> put_req_header("password", @psw)
            |> post("/api/v1/auth", @valid_user_attrs)
            |> json_response(201)
            |> Map.keys == ["refresh_token", "token"]

    Repo
      |> Ecto.Adapters.SQL.query("DELETE FROM public.user_reported WHERE user_id = #{user.id};")
  end

  test "does not login and renders errors when the password is missing", %{conn: conn} do
    user = Repo.insert! %User{birthday: Ecto.Date.cast!("1990-04-17"), email: @email, pseudo: "qwerty"}
    Repo.insert! UserPassword.changeset(%UserPassword{}, %{"user_id" => user.id, "password" => @psw})
    Repo.insert! %UserAuthentication{user_id: user.id}

    assert conn
            |> post("/api/v1/auth", @valid_user_attrs)
            |> json_response(422) == %{"type" => 4,"msg" => "email or password incorrect", "code" => 0}
  end

  test "login and renders success", %{conn: conn} do
    user = Repo.insert! %User{birthday: Ecto.Date.cast!("1990-04-17"), email: @email, pseudo: "qwerty", active: true}
    Repo.insert! UserPassword.changeset(%UserPassword{}, %{"user_id" => user.id, "password" => @psw})
    Repo.insert! %UserAuthentication{user_id: user.id}

    assert conn
            |> put_req_header("password", @psw)
            |> post("/api/v1/auth", @valid_user_attrs)
            |> json_response(201)
            |> Map.keys == ["refresh_token", "token"]
  end

  test "login and renders errors when active is false", %{conn: conn} do
    user = Repo.insert! %User{birthday: Ecto.Date.cast!("1990-04-17"), email: @email, pseudo: "qwerty", active: false}
    Repo.insert! UserPassword.changeset(%UserPassword{}, %{"user_id" => user.id, "password" => @psw})
    Repo.insert! %UserAuthentication{user_id: user.id}

    assert conn
            |> put_req_header("password", @psw)
            |> post("/api/v1/auth", @valid_user_attrs)
            |> json_response(422) == %{"type" => 4, "msg" => "user not active", "code" => 8}
  end

  test "renew login and renders success when data is valid", %{conn: conn} do
    conn = conn
                |> put_req_header("password", @psw)
                |> post(user_path(conn, :create), user: @valid_user_attrs)

      token = json_response(conn, 201)["token"]
      refresh_token = json_response(conn, 201)["refresh_token"]
      assert !is_nil(token)
      assert !is_nil(refresh_token)

    new_conn = build_conn(:post, "/api/v1/auth/renew", %{})
      |> put_req_header("authorization", token)
      |> put_req_header("authorization_refesh", refresh_token)
      |> Router.call(@opt)

    token2 = json_response(new_conn, 201)["token"]
    refresh_token2 = json_response(new_conn, 201)["refresh_token"]
    assert !is_nil(token2)
    assert !is_nil(refresh_token2)
    assert token2 != token
    assert refresh_token2 != refresh_token
  end

  test "renew login and renders error when refresh_token is empty", %{conn: conn} do
    token = "g3QAAAACZAAEZGF0YWErZAAGc2lnbmVkbgYANv96e1YB##QUWSbjJVQ3Flb3xAApjeRnfcc7Q="

    assert conn
            |> put_req_header("authorization", token)
            |> post("/api/v1/auth/renew", %{})
            |> json_response(422) == %{"code" => 3, "msg" => "token not renew", "type" => 4}
  end

  test "renew login and renders error when token is empty", %{conn: conn} do
    refresh_token = "g3QAAAACZAAEZGF0YWErZAAGc2lnbmVkbgYANv96e1YB##QUWSbjJVQ3Flb3xAApjeRnfcc7Q="

    assert conn
              |> put_req_header("authorization", refresh_token)
              |> post("/api/v1/auth/renew", %{})
              |> json_response(422) == %{"code" => 3, "msg" => "token not renew", "type" => 4}
  end

  test "renew login and renders error when token is ok but refresh_token is not", %{conn: conn} do
    conn = conn
            |> put_req_header("password", @psw)
            |> post(user_path(conn, :create), user: @valid_user_attrs)

    token = json_response(conn, 201)["token"]
    refresh_token = json_response(conn, 201)["refresh_token"]
    assert !is_nil(token)
    assert !is_nil(refresh_token)

    refresh_token = "g3QAAAACZAAEZGF0YWErZAAGc2lnbmVkbgYANv96e1YB##QUWSbjJVQ3Flb3xAApjeRnfcc7Q="
    assert build_conn(:post, "/api/v1/auth/renew", %{})
            |> put_req_header("authorization", token)
            |> put_req_header("authorization_refesh", refresh_token)
            |> Router.call(@opt)
            |> json_response(422) == %{"code" => 3, "msg" => "token not renew", "type" => 4}
  end

  test "check if user reset token", %{conn: _} do
    user_auth = UserAuthentication.changeset(%UserAuthentication{}, @valid_attrs_auth) |> Repo.insert!

    user_params = Map.new()
                    |> Map.put("password", "test")
                    |> Map.put("user_id", user_auth.user_id)

    result = UserAuthenticationController.insert_user_token(user_auth.user_id, user_params)

    {:ok, %{user_authentication: user_authentication}} = result
    assert user_authentication.token != user_auth.token
    assert user_authentication.refresh_token != user_auth.refresh_token
    assert user_authentication.room == user_auth.room
    assert user_authentication.user_id == user_auth.user_id
    assert user_authentication.id == user_auth.id
  end

  test "check if user can reset some value token, room and is_online", %{conn: _} do
    user_auth = UserAuthentication.changeset(%UserAuthentication{}, @valid_attrs_auth) |> Repo.insert!

    result = UserAuthenticationController.update_auth_user(user_auth.user_id, %{push_token: "push_token", os: "os"})
    {:ok, %{user_authentication: user_authentication}} = result
    assert user_authentication.push_token == "push_token"
    assert user_authentication.os == "os"
  end

  test "check if user can reset some value room and is_online", %{conn: _} do
    user_auth = UserAuthentication.changeset(%UserAuthentication{}, @valid_attrs_auth) |> Repo.insert!

    last_seen = Ecto.DateTime.utc

    result = UserAuthenticationController.update_auth_user(user_auth.user_id, %{last_seen: last_seen, is_online: false})
    {:ok, %{user_authentication: user_authentication}} = result
    assert user_authentication.last_seen == last_seen
    assert user_authentication.is_online == false
  end

  test "check if user can reset some value is_online", %{conn: _} do
    user_auth = UserAuthentication.changeset(%UserAuthentication{}, @valid_attrs_auth) |> Repo.insert!

    result = UserAuthenticationController.update_auth_user(user_auth.user_id, %{is_online: false})
    {:ok, %{user_authentication: user_authentication}} = result
    assert user_authentication.is_online == false
  end

  test "check if user can reset some value room", %{conn: _} do
    user_auth = UserAuthentication.changeset(%UserAuthentication{}, @valid_attrs_auth) |> Repo.insert!

    result = UserAuthenticationController.update_auth_user(user_auth.user_id, %{room: "room"})
    {:ok, %{user_authentication: user_authentication}} = result
    assert user_authentication.room == "room"
  end

  test "check if user can reset some value token", %{conn: _} do
    user_auth = UserAuthentication.changeset(%UserAuthentication{}, @valid_attrs_auth) |> Repo.insert!

    result = UserAuthenticationController.update_auth_user(user_auth.user_id, %{token: "token"})
    {:ok, %{user_authentication: user_authentication}} = result
    assert user_authentication.token == "token"
  end

end
