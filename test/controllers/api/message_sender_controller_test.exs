defmodule PandaPhoenix.MessageSenderControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.User
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.UserPassword
  alias PandaPhoenix.PublicRoom

  @user1 %User{birthday: Ecto.Date.cast!("1990-04-17"), email: "dovi1@gmail.com", pseudo: "dovi1"}

  @valid_attrs %{latitude: "120.5", longitude: "120.5", public_room_id: 42}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "send broadcase but render error missing message", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    Repo.insert! %UserPassword{user_id: user_id, can_broadcast: true}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/broadcast/message", %{"room_id" => 1, "message" => "", "url" => "http://www.google.com", "is_general_room" => true})

      assert json_response(conn, 422) == %{"code" => 4, "msg" => "params invalid", "type" => 7}
  end

  test "send broadcase but render error missing user id invalid", %{conn: conn} do
    token = Token.sign_user(-1)
    Repo.insert! %UserAuthentication{user_id: -1, token: token}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/broadcast/message", %{"room_id" => 1, "message" => "hello", "url" => "http://www.google.com", "is_general_room" => true})

      assert json_response(conn, 422) == %{"type" => 7, "code" => 3, "msg" => "user not found"}
  end

  test "send broadcase but render error missing user no grant", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    Repo.insert! %UserPassword{user_id: user_id, can_broadcast: false}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/broadcast/message", %{"room_id" => 1, "message" => "hello", "url" => "http://www.google.com", "is_general_room" => true})

      assert json_response(conn, 422) == %{"type" => 7, "code" => 1, "msg" => "not grant privilege"}
  end

  test "send broadcase but render error room not found", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    Repo.insert! %UserPassword{user_id: user_id, can_broadcast: true}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/broadcast/message", %{"room_id" => -1, "message" => "hello", "url" => "http://www.google.com", "is_general_room" => true})

      assert json_response(conn, 422) == %{"type" => 7, "code" => 2, "msg" => "room not found"}
  end

  test "send broadcase general and render success", %{conn: conn} do
    init_rooms

    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    Repo.insert! %UserPassword{user_id: user_id, can_broadcast: true}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/broadcast/message", %{"room_id" => 1, "message" => "hello", "url" => "http://www.google.com", "is_general_room" => true})

    assert response(conn, 200) == "{}"
  end

  test "send broadcase sale and render success", %{conn: conn} do
    init_rooms

    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    Repo.insert! %UserPassword{user_id: user_id, can_broadcast: true}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/broadcast/message", %{"room_id" => 1, "message" => "hello", "url" => "http://www.google.com", "is_general_room" => false})

      assert response(conn, 200) == "{}"
  end

  defp init_rooms do
    Repo.get!(PublicRoom, 1)
                  |> PublicRoom.changeset(%{lft: 0, rgt: 15})
                  |> Repo.update!
    Repo.insert! %PublicRoom{name: "brazil_1_country", original_name: "brazil", type: "country", lft: 1, rgt: 14}
    Repo.insert! %PublicRoom{name: "são-paulo_3_state", original_name: "são paulo", type: "state", lft: 2, rgt: 13}
    Repo.insert! %PublicRoom{name: "mogi-das-cruzes_4_county", original_name: "mogi das cruzes", type: "county", lft: 3, rgt: 12}
    Repo.insert! %PublicRoom{name: "mogi-das-cruzes_5_city", original_name: "mogi das cruzes", type: "city", lft: 4, rgt: 11}
    Repo.insert! %PublicRoom{name: "NONE_6_neighborhood", original_name: "NONE", type: "neighborhood", lft: 5, rgt: 10}
    Repo.insert! %PublicRoom{name: "rua-tenente-manoel-alves-dos-anjos_7_street", original_name: "rua tenente manoel alves dos anjos", type: "street", lft: 6, rgt: 9}
    Repo.insert! %PublicRoom{name: "338_8_street_number", original_name: "338", type: "street_number", lft: 7, rgt: 8}
  end

end
