defmodule PandaPhoenix.UserAuthenticationPlugTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.Watcher.Reported.ReportedWatcher

  @user_id 1
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "does call api with duplicat token and renders errors", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    :timer.sleep(1000);

    conn = conn
      |> put_req_header("authorization", "Bearer " <> Token.sign_user(@user_id))
      |> post(user_report_path(conn, :create), user_report: @invalid_attrs)

    assert json_response(conn, 409) == %{"type" => 4, "msg" => "user already login", "code" => 5}
  end

  test "does call api without token and renders errors", %{conn: conn} do
    conn = conn
      |> post(user_report_path(conn, :create), user_report: @invalid_attrs)

    assert json_response(conn, 404) == %{"type" => 4, "msg" => "token is not found", "code" => 1}
  end

  test "does call api with token invalid and renders errors", %{conn: conn} do
    conn = conn
      |> put_req_header("authorization", "Bearer " <> Token.sign_user(@user_id))
      |> post(user_report_path(conn, :create), user_report: @invalid_attrs)

    :timer.sleep(2000)

    assert json_response(conn, 401) == %{"type" => 4, "msg" => "token is invalid", "code" => 2}
  end

  test "does call api with user reported and renders errors", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    ReportedWatcher.add(@user_id)

    :timer.sleep(2000)

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post(user_report_path(conn, :create), user_report: @invalid_attrs)

    assert json_response(conn, 401) == %{"type" => 4, "msg" => "user reported", "details" => %{"time" => 148}, "code" => 4}
  end

  test "does call api push token and renders errors", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    :timer.sleep(2000)

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/push_token", %{"token" => "", "os" => ""})

    assert json_response(conn, 422) == %{"code" => 6, "msg" => "param missing", "type" => 4}
  end

  test "does call api push token is nil and renders errors", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    :timer.sleep(2000)

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/push_token", %{"token" => nil, "os" => "ios"})

    assert json_response(conn, 422) == %{"code" => 6, "msg" => "param missing", "type" => 4}
  end

  test "does call api push os is nil and renders errors", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    :timer.sleep(2000)

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/push_token", %{"token" => "1234", "os" => nil})

    assert json_response(conn, 422) == %{"code" => 6, "msg" => "param missing", "type" => 4}
  end

  test "does call api push os is not android or ios and renders errors", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    :timer.sleep(2000)

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/push_token", %{"token" => "1234", "os" => "lalal"})

    assert json_response(conn, 422) == %{"code" => 6, "msg" => "param missing", "type" => 4}
  end

  test "does call api push os is ios and renders success", %{conn: conn} do
    token = Token.sign_user(@user_id)
    auth = Repo.insert! %UserAuthentication{user_id: @user_id, token: token, refresh_token: "123456789"}

    :timer.sleep(2000)

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/push_token", %{"token" => "1234", "os" => "ios"})

    assert response(conn, 200) == "{}"

    result = Repo.get(UserAuthentication, auth.id)
    assert result.os == "ios"
    assert result.push_token == "1234"
  end

  test "does call api push os is android and renders success", %{conn: conn} do
    token = Token.sign_user(@user_id)
    auth = Repo.insert! %UserAuthentication{user_id: @user_id, token: token, refresh_token: "123456789"}

    :timer.sleep(2000)

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post("/api/v1/push_token", %{"token" => "1234", "os" => "android"})

    assert response(conn, 200) == "{}"

    result = Repo.get(UserAuthentication, auth.id)
    assert result.os == "android"
    assert result.push_token == "1234"
  end


end
