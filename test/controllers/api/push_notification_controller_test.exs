defmodule PandaPhoenix.PushNotificationControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.PushNotificationController

  test "does call push with no token and renders errors" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: "ios", token: "", msg: "hello", playload: %{"test" => "test"}}) == :error
  end

  test "does call push with nil token and renders errors" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: "ios", token: nil, msg: "hello", playload: %{"test" => "test"}}) == :error
  end

  test "does call push with no platorm and renders errors" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: "", token: "1234", msg: "hello", playload: %{"test" => "test"}}) == :error
  end

  test "does call push with nil platorm and renders errors" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: nil, token: "1234", msg: "hello", playload: %{"test" => "test"}}) == :error
  end

  test "does call push with platorm not use and renders errors" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: "hello", token: "1234", msg: "hello", playload: %{"test" => "test"}}) == :error
  end

  test "does call push with no message and renders success" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: "ios", token: "1234", msg: "", playload: %{"test" => "test"}}) == :ok
  end

  test "does call push with nil message and renders success" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: "ios", token: "1234", msg: nil, playload: %{"test" => "test"}}) == :ok
  end

  test "does call push with nil user_id and renders errors" do
    assert PushNotificationController.push(:msg, %{user_id: nil, os: "ios", token: "1234", msg: nil, playload: %{"test" => "test"}}) == :error
  end

  test "does call push with platorm ios and renders success" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: "ios", token: "1234", msg: "hello", playload: %{"test" => "test"}}) == :ok
  end

  test "does call push with platorm android and renders success" do
    assert PushNotificationController.push(:msg, %{user_id: 1, os: "android", token: "1234", msg: "hello", playload: %{"test" => "test"}}) == :not_implemented
  end


  test "does call push friendship with no token and renders errors" do
    assert PushNotificationController.push(:friendship, %{user_id: 1, os: "ios", token: "", playload: %{"test" => "test"}}) == :error
  end

  test "does call push friendship with nil token and renders errors" do
    assert PushNotificationController.push(:friendship, %{user_id: 1, os: "ios", token: nil, playload: %{"test" => "test"}}) == :error
  end

  test "does call push friendship with no platorm and renders errors" do
    assert PushNotificationController.push(:friendship, %{user_id: 1, os: "", token: "1234", playload: %{"test" => "test"}}) == :error
  end

  test "does call push friendship with nil platorm and renders errors" do
    assert PushNotificationController.push(:friendship, %{user_id: 1, os: nil, token: "1234", playload: %{"test" => "test"}}) == :error
  end

  test "does call push friendship with nil user_id and renders errors" do
    assert PushNotificationController.push(:friendship, %{user_id: nil, os: nil, token: "1234", playload: %{"test" => "test"}}) == :error
  end

  test "does call push friendship with platorm ios and renders success" do
    assert PushNotificationController.push(:friendship, %{user_id: 1, os: "ios", token: "1234", playload: %{"test" => "test"}}) == :ok
  end

  test "does call push friendship with platorm android and renders success" do
    assert PushNotificationController.push(:friendship, %{user_id: 1, os: "android", token: "1234", playload: %{"test" => "test"}}) == :not_implemented
  end

end
