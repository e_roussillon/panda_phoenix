defmodule PandaPhoenix.BackupPendingControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.User
  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.DateUtil
  alias PandaPhoenix.PrivateRoom
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateMessage

  @user1 %User{birthday: Ecto.Date.cast!("1990-04-17"), email: "dovi1@gmail.com", pseudo: "dovi1"}
  @user2 %User{birthday: Ecto.Date.cast!("1990-04-17"), email: "dovi2@gmail.com", pseudo: "dovi2"}

  setup %{conn: conn} do
    conn = conn
        |> put_req_header("accept", "application/json")

    {:ok, conn: conn}
  end

  #
  # Not pending
  #

  test "get not backup pending render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 0
  end

  test "get not backup pending last_seen over message sent render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_received, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index"}
    Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_sending, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index1"}
    Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_read, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index2"}

    :timer.sleep(1000)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 0
  end

  test "get not backup pending message with status 'send' but sender is current user render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_sent, from_user_id: user_id, type: 0, data_id: 0, index_row: "index"}

    :timer.sleep(1000)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, last_seen: Ecto.DateTime.utc}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 0
  end

  test "get not backup pending message with status 'send' but sender is current user and with friendship render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_sent, from_user_id: user_id, type: 0, data_id: 0, index_row: "index"}
    Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_friend, to_user_id: user_id2, user_id: user_id}

    :timer.sleep(1000)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, last_seen: Ecto.DateTime.utc}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 0
  end

  test "get backup pending message with status 'send' render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    private_room_user1 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    private_room_user2 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    private_message = Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_sent, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index"}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 5

    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, last_action, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, NULL, '#{user2.pseudo}', NULL, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert Enum.at(list, 1) == "INSERT OR REPLACE INTO private_rooms (data_id, private_rooms_id, is_group, name, updated_at) VALUES (NULL, #{private_room.id}, 0, '#{private_room.name}', '#{Ecto.DateTime.to_string(private_room.updated_at)}');"
    assert Enum.at(list, 2) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user1.room_id}, '#{Ecto.DateTime.to_string(private_room_user1.updated_at)}', #{private_room_user1.user_id});"
    assert Enum.at(list, 3) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user2.room_id}, '#{Ecto.DateTime.to_string(private_room_user2.updated_at)}', #{private_room_user2.user_id});"
    assert List.last(list) == "INSERT OR REPLACE INTO private_messages (data_id, from_user_id, private_messages_id, index_row, inserted_at, msg, room_id, status, type) VALUES (0, #{private_message.from_user_id}, #{private_message.id}, '#{private_message.index_row}', #{DateUtil.ecto_to_timestamp(private_message.inserted_at)}.0, '#{private_message.msg}', #{private_message.room_id}, #{private_message.status}, #{private_message.type});"
  end

  test "get backup pending 2 messages with status 'send' render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    private_room_user1 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    private_room_user2 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    private_message = Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_sent, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index"}
    private_message2 = Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello2", status: PrivateMessage.message_sent, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index2"}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 6

    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, last_action, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, NULL, '#{user2.pseudo}', NULL, #{DateUtil.ecto_to_timestamp(user2.updated_at)}.0, NULL, NULL, NULL);"
    assert Enum.at(list, 1) == "INSERT OR REPLACE INTO private_rooms (data_id, private_rooms_id, is_group, name, updated_at) VALUES (NULL, #{private_room.id}, 0, '#{private_room.name}', '#{Ecto.DateTime.to_string(private_room.updated_at)}');"
    assert Enum.at(list, 2) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user1.room_id}, '#{Ecto.DateTime.to_string(private_room_user1.updated_at)}', #{private_room_user1.user_id});"
    assert Enum.at(list, 3) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user2.room_id}, '#{Ecto.DateTime.to_string(private_room_user2.updated_at)}', #{private_room_user2.user_id});"
    assert Enum.at(list, 4) == "INSERT OR REPLACE INTO private_messages (data_id, from_user_id, private_messages_id, index_row, inserted_at, msg, room_id, status, type) VALUES (0, #{private_message.from_user_id}, #{private_message.id}, '#{private_message.index_row}', #{DateUtil.ecto_to_timestamp(private_message.inserted_at)}.0, '#{private_message.msg}', #{private_message.room_id}, #{private_message.status}, #{private_message.type});"
    assert List.last(list) == "INSERT OR REPLACE INTO private_messages (data_id, from_user_id, private_messages_id, index_row, inserted_at, msg, room_id, status, type) VALUES (0, #{private_message2.from_user_id}, #{private_message2.id}, '#{private_message2.index_row}', #{DateUtil.ecto_to_timestamp(private_message2.inserted_at)}.0, '#{private_message2.msg}', #{private_message2.room_id}, #{private_message2.status}, #{private_message2.type});"
  end

  test "get backup pending 2 messages one status 'send' other different status render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    auth2 = Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, last_seen: Ecto.DateTime.utc}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    private_room_user1 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    private_room_user2 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    private_message = Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_sent, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index"}
    Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello2", status: PrivateMessage.message_read, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index2"}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]

    assert length(list) == 5

    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, last_action, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, NULL, '#{user2.pseudo}', NULL, #{DateUtil.ecto_to_timestamp(auth2.last_seen)}.0, NULL, NULL, NULL);"
    assert Enum.at(list, 1) == "INSERT OR REPLACE INTO private_rooms (data_id, private_rooms_id, is_group, name, updated_at) VALUES (NULL, #{private_room.id}, 0, '#{private_room.name}', '#{Ecto.DateTime.to_string(private_room.updated_at)}');"
    assert Enum.at(list, 2) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user1.room_id}, '#{Ecto.DateTime.to_string(private_room_user1.updated_at)}', #{private_room_user1.user_id});"
    assert Enum.at(list, 3) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user2.room_id}, '#{Ecto.DateTime.to_string(private_room_user2.updated_at)}', #{private_room_user2.user_id});"
    assert List.last(list) == "INSERT OR REPLACE INTO private_messages (data_id, from_user_id, private_messages_id, index_row, inserted_at, msg, room_id, status, type) VALUES (0, #{private_message.from_user_id}, #{private_message.id}, '#{private_message.index_row}', #{DateUtil.ecto_to_timestamp(private_message.inserted_at)}.0, '#{private_message.msg}', #{private_message.room_id}, #{private_message.status}, #{private_message.type});"
  end

  test "get backup pending 2 messages one status 'send' other diferent status and with friendship render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    private_room_user1 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    private_room_user2 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    private_message = Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello", status: PrivateMessage.message_sent, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index"}
    Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello2", status: PrivateMessage.message_read, from_user_id: user_id2, type: 0, data_id: 0, index_row: "index2"}
    friendship = Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 5

    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id}, NULL, 'dovi2', #{UserFriendship.friendship_friend}, #{DateUtil.ecto_to_timestamp(friendship.updated_at)}.0, NULL, NULL, NULL);"
    assert Enum.at(list, 1) == "INSERT OR REPLACE INTO private_rooms (data_id, private_rooms_id, is_group, name, updated_at) VALUES (NULL, #{private_room.id}, 0, '#{private_room.name}', '#{Ecto.DateTime.to_string(private_room.updated_at)}');"
    assert Enum.at(list, 2) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user1.room_id}, '#{Ecto.DateTime.to_string(private_room_user1.updated_at)}', #{private_room_user1.user_id});"
    assert Enum.at(list, 3) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user2.room_id}, '#{Ecto.DateTime.to_string(private_room_user2.updated_at)}', #{private_room_user2.user_id});"
    assert List.last(list) == "INSERT OR REPLACE INTO private_messages (data_id, from_user_id, private_messages_id, index_row, inserted_at, msg, room_id, status, type) VALUES (0, #{private_message.from_user_id}, #{private_message.id}, '#{private_message.index_row}', #{DateUtil.ecto_to_timestamp(private_message.inserted_at)}.0, '#{private_message.msg}', #{private_message.room_id}, #{private_message.status}, #{private_message.type});"
  end

  test "get backup pending 1 message status 'read' render with success", %{conn: conn} do
    user1 = Repo.insert! @user1
    user_id = user1.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    user2 = Repo.insert! @user2
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2}

    private_room = Repo.insert! %PrivateRoom{name: "test"}
    private_room_user1 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id}
    private_room_user2 = Repo.insert! %PrivateRoomUser{room_id: private_room.id, user_id: user_id2}
    private_message = Repo.insert! %PrivateMessage{room_id: private_room.id, msg: "hello2", status: PrivateMessage.message_read, from_user_id: user_id, type: 0, data_id: 0, index_row: "index2"}
    friendship = Repo.insert! %UserFriendship{last_action: user_id, status: UserFriendship.friendship_friend, to_user_id: user_id2, user_id: user_id}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> get("/api/v1/pending/backup", %{})

    list = json_response(conn, 201)["backup"]
    assert length(list) == 5

    assert List.first(list) == "INSERT OR REPLACE INTO users (users_id, is_online, last_action, last_seen, pseudo, status, updated_at, url_blur, url_original, url_thumb) VALUES (#{user_id2}, 0, #{user_id}, NULL, 'dovi2', #{UserFriendship.friendship_friend}, #{DateUtil.ecto_to_timestamp(friendship.updated_at)}.0, NULL, NULL, NULL);"
    assert Enum.at(list, 1) == "INSERT OR REPLACE INTO private_rooms (data_id, private_rooms_id, is_group, name, updated_at) VALUES (NULL, #{private_room.id}, 0, '#{private_room.name}', '#{Ecto.DateTime.to_string(private_room.updated_at)}');"
    assert Enum.at(list, 2) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user1.room_id}, '#{Ecto.DateTime.to_string(private_room_user1.updated_at)}', #{private_room_user1.user_id});"
    assert Enum.at(list, 3) == "INSERT OR REPLACE INTO private_room_users (room_id, updated_at, user_id) VALUES (#{private_room_user2.room_id}, '#{Ecto.DateTime.to_string(private_room_user2.updated_at)}', #{private_room_user2.user_id});"
    assert List.last(list) == "INSERT OR REPLACE INTO private_messages (data_id, from_user_id, private_messages_id, index_row, inserted_at, msg, room_id, status, type) VALUES (0, #{private_message.from_user_id}, #{private_message.id}, '#{private_message.index_row}', #{DateUtil.ecto_to_timestamp(private_message.inserted_at)}.0, '#{private_message.msg}', #{private_message.room_id}, #{private_message.status}, #{private_message.type});"
  end

end
