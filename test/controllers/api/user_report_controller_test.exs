defmodule PandaPhoenix.UserReportControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.UserReport
  alias PandaPhoenix.UserReported
  alias PandaPhoenix.Router
  alias PandaPhoenix.UserAuthentication

  @opt Router.init([])

  @user_id 1
  @user_reported_id 2
  @user_id_2 3
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "does not create a report and renders errors when data is invalid", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post(user_report_path(conn, :create), user_report: @invalid_attrs)

    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "user_id_reported", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  # test "1 does not create resource and renders error when data is invalid can not have same user", %{conn: conn} do
  #   conn = conn
  #     |> put_req_header("authorization", "Bearer " <> Token.sign_user(@user_reported_id))
  #     |> post(user_report_path(conn, :create), user_report: %{"user_id_reported" => @user_reported_id})
  #
  #   result = (from u in UserReport)
  #             |> Repo.all
  #   assert length(result) == 0
  #
  #   result = (from u in UserReported)
  #             |> Repo.all
  #   assert length(result) == 0
  #
  #   assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "user_id_reportor", "message" => ["can not be equal to other"]}, %{"field" => "user_id_reported", "message" => ["can not be equal to other"]}],
  #            "msg" => "Parameter Error", "type" => 1}
  # end

  test "does create a report and renders success", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post(user_report_path(conn, :create), user_report: %{"user_id_reported" => @user_reported_id})

    :timer.sleep(2000)

    result = (from u in UserReport)
              |> Repo.all
    assert length(result) == 1
    assert List.first(result).user_id_reported == @user_reported_id
    assert List.first(result).user_id_reportor == @user_id

    result = (from u in UserReported)
              |> Repo.all
    assert length(result) == 1
    assert List.first(result).count == 1
    assert List.first(result).is_active == true
    assert List.first(result).is_used == false
    assert List.first(result).notified == 0
    assert List.first(result).user_id == @user_reported_id

    assert conn.status == 200
    assert conn.resp_body == "{}"
  end

  test "does update a report and renders success and check counter", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}
    token2 = Token.sign_user(@user_id_2)
    Repo.insert! %UserAuthentication{user_id: @user_id_2, token: token2}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post(user_report_path(conn, :create), user_report: %{"user_id_reported" => @user_reported_id})

    conn = build_conn(:post, user_report_path(conn, :create), user_report: %{"user_id_reported" => @user_reported_id})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token2)
      |> Router.call(@opt)

    :timer.sleep(2000)

    result = (from u in UserReport)
              |> Repo.all
    assert length(result) == 2
    assert List.first(result).user_id_reported == @user_reported_id
    assert List.first(result).user_id_reportor == @user_id

    assert List.last(result).user_id_reported == @user_reported_id
    assert List.last(result).user_id_reportor == @user_id_2

    result = (from u in UserReported)
              |> Repo.all
    assert length(result) == 1
    assert List.first(result).count == 2
    assert List.first(result).is_active == true
    assert List.first(result).is_used == false
    assert List.first(result).notified == 0
    assert List.first(result).user_id == @user_reported_id

    assert conn.status == 200
    assert conn.resp_body == "{}"
  end

  test "does create a report and renders error if same user try to report 2 time", %{conn: conn} do
    token = Token.sign_user(@user_id)
    Repo.insert! %UserAuthentication{user_id: @user_id, token: token}

    conn = conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> post(user_report_path(conn, :create), user_report: %{"user_id_reported" => @user_reported_id})

    conn = build_conn(:post, user_report_path(conn, :create), user_report: %{"user_id_reported" => @user_reported_id})
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> token)
      |> Router.call(@opt)

    result = (from u in UserReported)
                |> Repo.all
    assert length(result) == 1

    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "user_id_reportor_user_id_reported", "message" => ["has already been taken"]}], "msg" => "Parameter Error", "type" => 1}
  end

end
