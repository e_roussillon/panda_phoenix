defmodule PandaPhoenix.UserControllerTest do
  use PandaPhoenix.ConnCase

  alias PandaPhoenix.Router
  alias PandaPhoenix.User

  @psw "123456"
  @email "dovi@gmail.com"
  @valid_attrs %{birthday: "1990-04-17", email: @email, gender: 0, pseudo: "dovi12"}
  @valid_attrs_full %{birthday: "1990-04-18", email: @email, gender: 0, pseudo: "dovi13", description: "some content he"}

  @opt Router.init([])
  @invalid_attrs %{}
  @invalid_attrs1 %{birthday: "", email: "", pseudo: ""}
  @invalid_attrs2 %{birthday: "1990-04-17", email: "", gender: 0, pseudo: ""}
  @invalid_attrs3 %{birthday: "1990-04-17", email: @email, gender: 0, pseudo: ""}
  @invalid_attrs4 %{birthday: "2010-04-17", email: @email, gender: 0, pseudo: ""}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "does not create resource and renders errors when missing password", %{conn: conn} do
    conn = delete_req_header(conn, "password")

    conn = post conn, user_path(conn, :create), user: @valid_attrs
    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "password", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = put_req_header(conn, "password", @psw)
    conn = post conn, user_path(conn, :create), user: @invalid_attrs
    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "pseudo", "message" => ["can't be blank"]}, %{"field" => "email", "message" => ["can't be blank"]}, %{"field" => "birthday", "message" => ["can't be blank"]}],
             "msg" => "Parameter Error", "type" => 1}
  end

  test "does not create resource and renders errors when data is empty", %{conn: conn} do
    conn = put_req_header(conn, "password", @psw)

    conn = post conn, user_path(conn, :create), user: @invalid_attrs1
    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "pseudo", "message" => ["can't be blank"]}, %{"field" => "email", "message" => ["can't be blank"]}, %{"field" => "birthday", "message" => ["can't be blank"]}],
             "msg" => "Parameter Error", "type" => 1}
  end

  test "does not create resource and renders errors when birthday is not empty", %{conn: conn} do
    conn = put_req_header(conn, "password", @psw)
    conn = post conn, user_path(conn, :create), user: @invalid_attrs2
    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "pseudo", "message" => ["can't be blank"]}, %{"field" => "email", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "does not create resource and renders errors when birthday, email and gender are not empty", %{conn: conn} do
    conn = put_req_header(conn, "password", @psw)
    conn = post conn, user_path(conn, :create), user: @invalid_attrs3
    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "pseudo", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "does not create resource and renders errors when birthday under 16", %{conn: conn} do
    conn = put_req_header(conn, "password", @psw)
    conn = post conn, user_path(conn, :create), user: @invalid_attrs4
    assert json_response(conn, 422) == %{"code" => 0, "fields" => [%{"field" => "pseudo", "message" => ["can't be blank"]}, %{"field" => "birthday", "message" => ["birthday under 16"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = put_req_header(conn, "password", @psw)
    conn = post conn, user_path(conn, :create), user: @valid_attrs

    keys = json_response(conn, 201) |> Map.keys
    assert keys == ["refresh_token", "token"]
  end

  test "updates and renders success when data is valid", %{conn: conn} do
    conn = conn
            |> put_req_header("password", @psw)
            |> post(user_path(conn, :create), user: @valid_attrs)
    token = json_response(conn, 201)["token"]

    {:authenticated, user_id} = PandaPhoenix.Token.verify_user(%{token: token, with_disconnection: true, can_active: false})

    user = Repo.get(User, user_id)
    new_conn = build_conn(:put, user_path(conn, :update, user), %{user: @valid_attrs_full})
        |> put_req_header("authorization", "Bearer " <> token)
        |> Router.call(@opt)

    assert json_response(new_conn, 200)["birthday"] == "1990-04-18"
    assert json_response(new_conn, 200)["description"] == "some content he"
    assert json_response(new_conn, 200)["email"] == "dovi@gmail.com"
    assert json_response(new_conn, 200)["gender"] == 0
    assert json_response(new_conn, 200)["id"] == user_id
    assert json_response(new_conn, 200)["pseudo"] == "dovi13"
    assert json_response(new_conn, 200)["url_blur"] == nil
    assert json_response(new_conn, 200)["url_original"] == nil
    assert json_response(new_conn, 200)["url_thumb"] == nil
  end

  test "updates and renders errors when data is empty", %{conn: conn} do
    conn = conn
            |> put_req_header("password", @psw)
            |> post(user_path(conn, :create), user: @valid_attrs)
    token = json_response(conn, 201)["token"]

    {:authenticated, user_id} = PandaPhoenix.Token.verify_user(%{token: token, with_disconnection: true, can_active: false})

    user = Repo.get(User, user_id)
    new_conn = build_conn(:put, user_path(conn, :update, user), %{user: @invalid_attrs1})
        |> put_req_header("authorization", "Bearer " <> token)
        |> Router.call(@opt)

    assert json_response(new_conn, 422) == %{"code" => 0, "fields" => [%{"field" => "pseudo", "message" => ["can't be blank"]}, %{"field" => "email", "message" => ["can't be blank"]}, %{"field" => "birthday", "message" => ["can't be blank"]}],
             "msg" => "Parameter Error", "type" => 1}
  end

  test "updates and renders errors when birthday is not empty", %{conn: conn} do
    conn = conn
            |> put_req_header("password", @psw)
            |> post(user_path(conn, :create), user: @valid_attrs)
    token = json_response(conn, 201)["token"]

    {:authenticated, user_id} = PandaPhoenix.Token.verify_user(%{token: token, with_disconnection: true, can_active: false})

    user = Repo.get(User, user_id)
    new_conn = build_conn(:put, user_path(conn, :update, user), %{user: @invalid_attrs2})
        |> put_req_header("authorization", "Bearer " <> token)
        |> Router.call(@opt)

    assert json_response(new_conn, 422) == %{"code" => 0, "fields" => [%{"field" => "pseudo", "message" => ["can't be blank"]}, %{"field" => "email", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "updates and renders errors when birthday, email and gender are not empty", %{conn: conn} do
    conn = conn
            |> put_req_header("password", @psw)
            |> post(user_path(conn, :create), user: @valid_attrs)
    token = json_response(conn, 201)["token"]

    {:authenticated, user_id} = PandaPhoenix.Token.verify_user(%{token: token, with_disconnection: true, can_active: false})

    user = Repo.get(User, user_id)
    new_conn = build_conn(:put, user_path(conn, :update, user), %{user: @invalid_attrs3})
        |> put_req_header("authorization", "Bearer " <> token)
        |> Router.call(@opt)

    assert json_response(new_conn, 422) == %{"code" => 0, "fields" => [%{"field" => "pseudo", "message" => ["can't be blank"]}], "msg" => "Parameter Error", "type" => 1}
  end

  test "show and renders success when data is valid", %{conn: conn} do
    conn = conn
            |> put_req_header("password", @psw)
            |> post(user_path(conn, :create), user: @valid_attrs)
    token = json_response(conn, 201)["token"]

    {:authenticated, user_id} = PandaPhoenix.Token.verify_user(%{token: token, with_disconnection: true, can_active: false})

    user = Repo.get(User, user_id)
    new_conn = build_conn(:get, user_path(conn, :show, user))
        |> put_req_header("authorization", "Bearer " <> token)
        |> Router.call(@opt)

    assert json_response(new_conn, 200)["birthday"] == "1990-04-17"
    assert json_response(new_conn, 200)["description"] == nil
    assert json_response(new_conn, 200)["email"] == nil
    assert json_response(new_conn, 200)["gender"] == 0
    assert json_response(new_conn, 200)["id"] == user_id
    assert json_response(new_conn, 200)["pseudo"] == "dovi12"
    assert json_response(new_conn, 200)["friendship_status"] == 0
    assert json_response(new_conn, 200)["friendship_last_action"] == nil
    assert json_response(new_conn, 200)["is_online"] == false
    assert json_response(new_conn, 200)["reported"] == false
    assert json_response(new_conn, 200)["url_blur"] == nil
    assert json_response(new_conn, 200)["url_original"] == nil
    assert json_response(new_conn, 200)["url_thumb"] == nil
  end

  test "show and renders error when data is invalid", %{conn: conn} do
    conn = conn
            |> put_req_header("password", @psw)
            |> post(user_path(conn, :create), user: @valid_attrs)
    token = json_response(conn, 201)["token"]

    {:authenticated, _} = PandaPhoenix.Token.verify_user(%{token: token, with_disconnection: true, can_active: false})

    user = %User{id: -1}
    new_conn = build_conn(:get, user_path(conn, :show, user))
        |> put_req_header("authorization", "Bearer " <> token)
        |> Router.call(@opt)

    assert json_response(new_conn, 422) == %{"code" => 0, "msg" => "user not found", "type" => 8}
  end
end
