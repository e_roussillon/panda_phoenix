defmodule PandaPhoenix.PublicRoomControllerTest do
  use PandaPhoenix.ConnCase

  # alias PandaPhoenix.PublicRoom
  @valid_attrs %{lft: 42, name: "some content", original_name: "some content", rgt: 42, type: "some content"}
  @invalid_attrs %{}

  # setup %{conn: conn} do
  #   {:ok, conn: put_req_header(conn, "accept", "application/json")}
  # end
  #
  # test "lists all entries on index", %{conn: conn} do
  #   # conn = get conn, public_room_path(conn, :index)
  #   # assert json_response(conn, 200)["data"] == []
  # end
  #
  # test "shows chosen resource", %{conn: conn} do
  #   # public_room = Repo.insert! %PublicRoom{}
  #   # conn = get conn, public_room_path(conn, :show, public_room)
  #   # assert json_response(conn, 200)["data"] == %{"id" => public_room.id,
  #   #   "name" => public_room.name,
  #   #   "original_name" => public_room.original_name,
  #   #   "type" => public_room.type,
  #   #   "lft" => public_room.lft,
  #   #   "rgt" => public_room.rgt}
  # end
  #
  # test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
  #   # assert_error_sent 404, fn ->
  #   #   get conn, public_room_path(conn, :show, -1)
  #   # end
  # end
  #
  # test "creates and renders resource when data is valid", %{conn: conn} do
  #   # conn = post conn, public_room_path(conn, :create), public_room: @valid_attrs
  #   # assert json_response(conn, 201)["data"]["id"]
  #   # assert Repo.get_by(PublicRoom, @valid_attrs)
  # end
  #
  # test "does not create resource and renders errors when data is invalid", %{conn: conn} do
  #   # conn = post conn, public_room_path(conn, :create), public_room: @invalid_attrs
  #   # assert json_response(conn, 422)["errors"] != %{}
  # end
  #
  # test "updates and renders chosen resource when data is valid", %{conn: conn} do
  #   # public_room = Repo.insert! %PublicRoom{}
  #   # conn = put conn, public_room_path(conn, :update, public_room), public_room: @valid_attrs
  #   # assert json_response(conn, 200)["data"]["id"]
  #   # assert Repo.get_by(PublicRoom, @valid_attrs)
  # end
  #
  # test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
  #   # public_room = Repo.insert! %PublicRoom{}
  #   # conn = put conn, public_room_path(conn, :update, public_room), public_room: @invalid_attrs
  #   # assert json_response(conn, 422)["errors"] != %{}
  # end
  #
  # test "deletes chosen resource", %{conn: conn} do
  #   # public_room = Repo.insert! %PublicRoom{}
  #   # conn = delete conn, public_room_path(conn, :delete, public_room)
  #   # assert response(conn, 204)
  #   # refute Repo.get(PublicRoom, public_room.id)
  # end
end
