defmodule PandaPhoenix.PublicMessageControllerTest do
  use PandaPhoenix.ConnCase

  # alias PandaPhoenix.PublicMessage
  @valid_attrs %{data_id: 42, from_user_id: 42, is_broadcast: true, msg: "some content", public_room_id: 42}
  @invalid_attrs %{}

  # setup %{conn: conn} do
  #   {:ok, conn: put_req_header(conn, "accept", "application/json")}
  # end
  #
  # test "lists all entries on index", %{conn: conn} do
  #   # conn = get conn, public_message_path(conn, :index)
  #   # assert json_response(conn, 200)["data"] == []
  # end
  #
  # test "shows chosen resource", %{conn: conn} do
  #   # public_message = Repo.insert! %PublicMessage{}
  #   # conn = get conn, public_message_path(conn, :show, public_message)
  #   # assert json_response(conn, 200)["data"] == %{"id" => public_message.id,
  #   #   "from_user_id" => public_message.from_user_id,
  #   #   "is_broadcast" => public_message.is_broadcast,
  #   #   "data_id" => public_message.data_id,
  #   #   "msg" => public_message.msg,
  #   #   "public_room_id" => public_message.public_room_id}
  # end
  #
  # test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
  #   # assert_error_sent 404, fn ->
  #   #   get conn, public_message_path(conn, :show, -1)
  #   # end
  # end
  #
  # test "creates and renders resource when data is valid", %{conn: conn} do
  #   # conn = post conn, public_message_path(conn, :create), public_message: @valid_attrs
  #   # assert json_response(conn, 201)["data"]["id"]
  #   # assert Repo.get_by(PublicMessage, @valid_attrs)
  # end
  #
  # test "does not create resource and renders errors when data is invalid", %{conn: conn} do
  #   # conn = post conn, public_message_path(conn, :create), public_message: @invalid_attrs
  #   # assert json_response(conn, 422)["errors"] != %{}
  # end
  #
  # test "updates and renders chosen resource when data is valid", %{conn: conn} do
  #   # public_message = Repo.insert! %PublicMessage{}
  #   # conn = put conn, public_message_path(conn, :update, public_message), public_message: @valid_attrs
  #   # assert json_response(conn, 200)["data"]["id"]
  #   # assert Repo.get_by(PublicMessage, @valid_attrs)
  # end
  #
  # test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
  #   # public_message = Repo.insert! %PublicMessage{}
  #   # conn = put conn, public_message_path(conn, :update, public_message), public_message: @invalid_attrs
  #   # assert json_response(conn, 422)["errors"] != %{}
  # end
  #
  # test "deletes chosen resource", %{conn: conn} do
  #   # public_message = Repo.insert! %PublicMessage{}
  #   # conn = delete conn, public_message_path(conn, :delete, public_message)
  #   # assert response(conn, 204)
  #   # refute Repo.get(PublicMessage, public_message.id)
  # end
end
