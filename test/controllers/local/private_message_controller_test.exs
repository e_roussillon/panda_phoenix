defmodule PandaPhoenix.PrivateMessageControllerTest do
  use PandaPhoenix.ConnCase

  # alias PandaPhoenix.PrivateMessage
  @valid_attrs %{data_id: 42, from_user_id: 42, is_read: true, msg: "some content", room_id: 42}
  @invalid_attrs %{}

  # setup %{conn: conn} do
  #   {:ok, conn: put_req_header(conn, "accept", "application/json")}
  # end
  #
  # test "lists all entries on index", %{conn: conn} do
  #   # conn = get conn, private_message_path(conn, :index)
  #   # assert json_response(conn, 200)["data"] == []
  # end
  #
  # test "shows chosen resource", %{conn: conn} do
  #   # private_message = Repo.insert! %PrivateMessage{}
  #   # conn = get conn, private_message_path(conn, :show, private_message)
  #   # assert json_response(conn, 200)["data"] == %{"id" => private_message.id,
  #   #   "room_id" => private_message.room_id,
  #   #   "msg" => private_message.msg,
  #   #   "is_read" => private_message.is_read,
  #   #   "from_user_id" => private_message.from_user_id,
  #   #   "data_id" => private_message.data_id}
  # end
  #
  # test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
  #   # assert_error_sent 404, fn ->
  #   #   get conn, private_message_path(conn, :show, -1)
  #   # end
  # end
  #
  # test "creates and renders resource when data is valid", %{conn: conn} do
  #   # conn = post conn, private_message_path(conn, :create), private_message: @valid_attrs
  #   # assert json_response(conn, 201)["data"]["id"]
  #   # assert Repo.get_by(PrivateMessage, @valid_attrs)
  # end
  #
  # test "does not create resource and renders errors when data is invalid", %{conn: conn} do
  #   # conn = post conn, private_message_path(conn, :create), private_message: @invalid_attrs
  #   # assert json_response(conn, 422)["errors"] != %{}
  # end
  #
  # test "updates and renders chosen resource when data is valid", %{conn: conn} do
  #   # private_message = Repo.insert! %PrivateMessage{}
  #   # conn = put conn, private_message_path(conn, :update, private_message), private_message: @valid_attrs
  #   # assert json_response(conn, 200)["data"]["id"]
  #   # assert Repo.get_by(PrivateMessage, @valid_attrs)
  # end
  #
  # test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
  #   # private_message = Repo.insert! %PrivateMessage{}
  #   # conn = put conn, private_message_path(conn, :update, private_message), private_message: @invalid_attrs
  #   # assert json_response(conn, 422)["errors"] != %{}
  # end
  #
  # test "deletes chosen resource", %{conn: conn} do
  #   # private_message = Repo.insert! %PrivateMessage{}
  #   # conn = delete conn, private_message_path(conn, :delete, private_message)
  #   # assert response(conn, 204)
  #   # refute Repo.get(PrivateMessage, private_message.id)
  # end
end
