defmodule PandaPhoenix.UserFriendshipTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.UserFriendship

  @valid_attrs %{last_action: 42, status: 42, to_user_id: 42, user_id: 42}
  @invalid_attrs %{}

  test "UserFriendship changeset with valid attributes" do
    changeset = UserFriendship.changeset(%UserFriendship{}, @valid_attrs)
    assert changeset.valid?
  end

  test "UserFriendship changeset with invalid attributes" do
    changeset = UserFriendship.changeset(%UserFriendship{}, @invalid_attrs)
    refute changeset.valid?
  end
end
