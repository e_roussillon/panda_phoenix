defmodule PandaPhoenix.PublicRoomTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.PublicRoom

  @valid_attrs %{lft: 42, name: "some content", original_name: "some content", rgt: 42, type: "some content"}
  @invalid_attrs %{}

  test "PublicRoom changeset with valid attributes" do
    changeset = PublicRoom.changeset(%PublicRoom{}, @valid_attrs)
    assert changeset.valid?
  end

  test "PublicRoom changeset with invalid attributes" do
    changeset = PublicRoom.changeset(%PublicRoom{}, @invalid_attrs)
    refute changeset.valid?
  end
end
