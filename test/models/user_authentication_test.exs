defmodule PandaPhoenix.UserAuthenticationTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.UserAuthentication

  @valid_attrs %{last_seen: Ecto.DateTime.utc, is_online: true, room: "some content", refresh_token: "some content", token: "some content", user_id: 42}
  @valid_attrs_minimum %{refresh_token: "some content", token: "some content", user_id: 42}

  @invalid_attrs %{}


  test "UserAuthentication changeset with valid attributes" do
    changeset = UserAuthentication.changeset(%UserAuthentication{}, @valid_attrs)
    assert changeset.valid?
  end

  test "UserAuthentication changeset with valid attributes with minimum" do
    changeset = UserAuthentication.changeset(%UserAuthentication{}, @valid_attrs_minimum)
    assert changeset.valid?
  end

  test "UserAuthentication changeset with invalid attributes" do
    changeset = UserAuthentication.changeset(%UserAuthentication{}, @invalid_attrs)
    refute changeset.valid?
  end

end
