defmodule PandaPhoenix.PublicRoomLocalizationTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.PublicRoomLocalization

  @valid_attrs %{latitude: "120.5", longitude: "120.5", api_result: "result from google API"}
  @valid_attrs_minimum %{public_room_id: 42, result: "some data", latitude: "120.5", longitude: "120.5", api_result: "result from google API"}

  @invalid_attrs %{}

  test "PublicRoomLocalization changeset with valid attributes" do
    changeset = PublicRoomLocalization.changeset(%PublicRoomLocalization{}, @valid_attrs)
    assert changeset.valid?
  end

  test "PublicRoomLocalization changeset with valid attributes with minimum" do
    changeset = PublicRoomLocalization.changeset(%PublicRoomLocalization{}, @valid_attrs_minimum)
    assert changeset.valid?
  end

  test "PublicRoomLocalization changeset with invalid attributes" do
    changeset = PublicRoomLocalization.changeset(%PublicRoomLocalization{}, @invalid_attrs)
    refute changeset.valid?
  end
end
