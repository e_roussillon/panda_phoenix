defmodule PandaPhoenix.UserTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.User

  @valid_attrs %{birthday: "1990-04-17", data_id: 42, description: "some content", email: "test@gmail.com", gender: 0, pseudo: "qwerty"}
  @valid_attrs1 %{birthday: "1990-04-17", email: "test@gmail.com", gender: 1, pseudo: "qwerty"}

  @invalid_attrs %{}
  @invalid_attrs1 %{birthday: "", email: "", gender: "", pseudo: ""}
  @invalid_attrs2 %{birthday: "1990-04-17", email: "", gender: "", pseudo: ""}
  @invalid_attrs3 %{birthday: "1990-04-17", email: "test@gmail.com", gender: "", pseudo: ""}
  @invalid_attrs4 %{birthday: "1990-04-17", email: "test@gmail.com", gender: 0, pseudo: ""}


  @invalid_attrs_pseudo %{birthday: "1990-04-17", data_id: 42, description: "some content", email: "test@gmail.com", gender: 0, pseudo: ""}
  @invalid_attrs_pseudo1 %{birthday: "1990-04-17", data_id: 42, description: "some content", email: "test@gmail.com", gender: 0, pseudo: "do do"}
  @invalid_attrs_pseudo2 %{birthday: "1990-04-17", data_id: 42, description: "some content", email: "test@gmail.com", gender: 0, pseudo: "dodo!"}

  @invalid_attrs_email %{birthday: "1990-04-17", data_id: 42, description: "some content", email: "testgmail.com", gender: 0, pseudo: "qwerty"}
  @invalid_attrs_email1 %{birthday: "1990-04-17", data_id: 42, description: "some content", email: "test@gmail", gender: 0, pseudo: "qwerty"}
  @invalid_attrs_email2 %{birthday: "1990-04-17", data_id: 42, description: "some content", email: "test@gmail.c", gender: 0, pseudo: "qwerty"}
  @invalid_attrs_email3 %{birthday: "1990-04-17", data_id: 42, description: "some content", email: "", gender: 0, pseudo: "qwerty"}

  @invalid_attrs_birthday %{birthday: "2010-04-17", data_id: 42, description: "some content", email: "test@gmail.c", gender: 0, pseudo: "qwerty"}

  test "User changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "User changeset with valid attributes with minimum" do
    changeset = User.changeset(%User{}, @valid_attrs1)
    assert changeset.valid?
  end



  test "User changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "User changeset with invalid attributes all empty" do
    changeset = User.changeset(%User{}, @invalid_attrs1)
    refute changeset.valid?
  end

  test "User changeset with invalid attributes only speudo, gender and email are empty" do
    changeset = User.changeset(%User{}, @invalid_attrs2)
    refute changeset.valid?
  end

  test "User changeset with invalid attributes only speudo and gender are empty" do
    changeset = User.changeset(%User{}, @invalid_attrs3)
    refute changeset.valid?
  end

  test "User changeset with invalid attributes only speudo is empty" do
    changeset = User.changeset(%User{}, @invalid_attrs4)
    refute changeset.valid?
  end



  test "User changeset with invalid missing speudo" do
    changeset = User.changeset(%User{}, @invalid_attrs_pseudo)
    refute changeset.valid?
  end

  test "User changeset with invalid pseudo with whitespace" do
    changeset = User.changeset(%User{}, @invalid_attrs_pseudo1)
    refute changeset.valid?
  end

  test "User changeset with invalid pseudo with characters special" do
    changeset = User.changeset(%User{}, @invalid_attrs_pseudo2)
    refute changeset.valid?
  end




  test "User changeset with invalid email missing @" do
    changeset = User.changeset(%User{}, @invalid_attrs_email)
    refute changeset.valid?
  end

  test "User changeset with invalid email missing .com" do
    changeset = User.changeset(%User{}, @invalid_attrs_email1)
    refute changeset.valid?
  end

  test "User changeset with invalid email missing character at end .c" do
    changeset = User.changeset(%User{}, @invalid_attrs_email2)
    refute changeset.valid?
  end

  test "User changeset with missing email" do
    changeset = User.changeset(%User{}, @invalid_attrs_email3)
    refute changeset.valid?
  end

  test "User changeset with birthday under 16" do
    changeset = User.changeset(%User{}, @invalid_attrs_birthday)
    refute changeset.valid?
  end
end
