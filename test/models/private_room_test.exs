defmodule PandaPhoenix.PrivateRoomTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.PrivateRoom

  @valid_attrs %{data_id: 42, is_group: true, name: "some content"}
  @valid_attrs_minimum %{name: "some content"}
  @invalid_attrs %{}

  test "PrivateRoom changeset with valid attributes" do
    changeset = PrivateRoom.changeset(%PrivateRoom{}, @valid_attrs)
    assert changeset.valid?
  end

  test "PrivateRoom changeset with valid attributes with minimum" do
    changeset = PrivateRoom.changeset(%PrivateRoom{}, @valid_attrs_minimum)
    assert changeset.valid?
  end

  test "PrivateRoomchangeset with invalid attributes" do
    changeset = PrivateRoom.changeset(%PrivateRoom{}, @invalid_attrs)
    refute changeset.valid?
  end
end
