defmodule PandaPhoenix.UserFilterTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.UserFilter

  @valid_attrs %{active: true, is_highlight: true, name: "some content", user_id: 42}
  @valid_attrs_minimum %{is_highlight: true, name: "some content", user_id: 42}

  @invalid_attrs %{}

  test "UserFilter changeset with valid attributes" do
    changeset = UserFilter.changeset(%UserFilter{}, @valid_attrs)
    assert changeset.valid?
  end

  test "UserFilter changeset with valid attributes with minimum" do
    changeset = UserFilter.changeset(%UserFilter{}, @valid_attrs_minimum)
    assert changeset.valid?
  end

  test "UserFilter changeset with invalid attributes" do
    changeset = UserFilter.changeset(%UserFilter{}, @invalid_attrs)
    refute changeset.valid?
  end
end
