defmodule PandaPhoenix.DataTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.Data

  @valid_attrs %{type: 42, url_blur: "some content", url_original: "some content", url_thumb: "some content"}
  @valid_attrs_minimum %{url_blur: "some content", url_original: "some content", url_thumb: "some content"}
  @invalid_attrs %{}

  test "data changeset with valid attributes" do
    changeset = Data.changeset(%Data{}, @valid_attrs)
    assert changeset.valid?
  end

  test "data changeset with valid attributes with minimum" do
    changeset = Data.changeset(%Data{}, @valid_attrs_minimum)
    assert changeset.valid?
  end

  test "data changeset with invalid attributes" do
    changeset = Data.changeset(%Data{}, @invalid_attrs)
    assert changeset.valid?
  end
end
