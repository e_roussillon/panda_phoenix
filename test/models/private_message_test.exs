defmodule PandaPhoenix.PrivateMessageTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.PrivateMessage

  @valid_attrs %{index_row: "some content", data_id: 42, type: 42, from_user_id: 42, status: 42, msg: "some content", room_id: 42}
  @valid_attrs_minimum %{index_row: "some content", from_user_id: 42, msg: "some content", room_id: 42}

  @invalid_attrs %{}


  test "PrivateMessage changeset with valid attributes" do
    changeset = PrivateMessage.changeset(%PrivateMessage{}, @valid_attrs)
    assert changeset.valid?
  end

  test "PrivateMessage changeset with valid attributes with minimum" do
    changeset = PrivateMessage.changeset(%PrivateMessage{}, @valid_attrs_minimum)
    assert changeset.valid?
  end

  test "PrivateMessage changeset with invalid attributes" do
    changeset = PrivateMessage.changeset(%PrivateMessage{}, @invalid_attrs)
    refute changeset.valid?
  end
end
