defmodule PandaPhoenix.GenericRoomTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.GenericRoom

  @valid_attrs %{latitude: "some content", longitude: "some content"}
  @invalid_attrs %{}

  test "genericRoom changeset with valid attributes" do
    changeset = GenericRoom.changeset(%GenericRoom{}, @valid_attrs)
    assert changeset.valid?
  end

  test "genericRoom changeset with invalid attributes" do
    changeset = GenericRoom.changeset(%GenericRoom{}, @invalid_attrs)
    refute changeset.valid?
  end
end
