defmodule PandaPhoenix.PublicMessageTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.PublicMessage

  @valid_attrs %{from_user_id: 42, is_broadcast: true, is_general: true, data_id: 42, msg: "some content", public_room_id: 42}
  @valid_attrs_minimum %{from_user_id: 42, msg: "some content", public_room_id: 42}

  @invalid_attrs %{}

  test "PublicMessage changeset with valid attributes" do
    changeset = PublicMessage.changeset(%PublicMessage{}, @valid_attrs)
    assert changeset.valid?
  end

  test "PublicMessage changeset with valid attributes with minimum" do
    changeset = PublicMessage.changeset(%PublicMessage{}, @valid_attrs_minimum)
    assert changeset.valid?
  end

  test "PublicMessage changeset with invalid attributes" do
    changeset = PublicMessage.changeset(%PublicMessage{}, @invalid_attrs)
    refute changeset.valid?
  end
end
