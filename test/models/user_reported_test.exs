defmodule PandaPhoenix.UserReportedTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.UserReported

  @valid_attrs %{notified: 42, is_active: true, is_used: true, count: 42, user_id: 42}
  @valid_attrs_minimum %{count: 42, user_id: 42}

  @invalid_attrs %{}

  test "UserReported changeset with valid attributes" do
    changeset = UserReported.changeset(%UserReported{}, @valid_attrs)
    assert changeset.valid?
  end

  test "UserReported changeset with valid attributes minimum" do
    changeset = UserReported.changeset(%UserReported{}, @valid_attrs_minimum)
    assert changeset.valid?
  end

  test "UserReported changeset with invalid attributes" do
    changeset = UserReported.changeset(%UserReported{}, @invalid_attrs)
    refute changeset.valid?
  end
end
