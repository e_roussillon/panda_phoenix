defmodule PandaPhoenix.PrivateRoomUserTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.PrivateRoomUser

  @valid_attrs %{room_id: 42, user_id: 42}
  @invalid_attrs %{}

  test "PrivateRoomUser changeset with valid attributes" do
    changeset = PrivateRoomUser.changeset(%PrivateRoomUser{}, @valid_attrs)
    assert changeset.valid?
  end

  test "PrivateRoomUser changeset with invalid attributes" do
    changeset = PrivateRoomUser.changeset(%PrivateRoomUser{}, @invalid_attrs)
    refute changeset.valid?
  end
end
