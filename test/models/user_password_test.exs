defmodule PandaPhoenix.UserPasswordTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.UserPassword

  @valid_attrs %{password: "some content", user_id: 42}
  @invalid_attrs %{}

  test "UserPassword changeset with valid attributes" do
    changeset = UserPassword.changeset(%UserPassword{}, @valid_attrs)
    assert changeset.valid?
  end

  test "UserPassword changeset with invalid attributes" do
    changeset = UserPassword.changeset(%UserPassword{}, @invalid_attrs)
    refute changeset.valid?
  end
end
