defmodule PandaPhoenix.UserReportTest do
  use PandaPhoenix.ModelCase

  alias PandaPhoenix.UserReport

  @valid_attrs %{user_id_reportor: 40, user_id_reported: 42}
  @invalid_attrs %{user_id_reportor: 42, user_id_reported: 42}
  @invalid_attrs1 %{}

  test "UserReport changeset with valid attributes" do
    changeset = UserReport.changeset(%UserReport{}, @valid_attrs)
    assert changeset.valid?
  end

  test "UserReport changeset with invalid attributes can not be same value" do
    changeset = UserReport.changeset(%UserReport{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "UserReport changeset with invalid attributes" do
    changeset = UserReport.changeset(%UserReport{}, @invalid_attrs1)
    refute changeset.valid?
  end
end
