defmodule PandaPhoenix.BroadcastChannelTest do
  use PandaPhoenix.ChannelCase

  alias PandaPhoenix.BroadcastChannel
  alias PandaPhoenix.User
  alias PandaPhoenix.PublicRoom
  alias PandaPhoenix.PublicRoomLocalization
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.Watcher.Channel.ChannelWatcher

  # setup do
  #   {:ok, _, socket} =
  #     socket("user_id", %{some: :assign})
  #     |> subscribe_and_join(BroadcastChannel, "broadcast:lobby")
  #
  #   {:ok, socket: socket}
  # end

  test "connect with success but join room with error" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    %{token: _, room: _, room_connect: room_connect} = init_rooms

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    assert {:error, %{reason: "unauthorized"}} = subscribe_and_join(socket, "broadcast_channel:" <> "#{room_connect.id}", %{"subject" => "", "body" => ""})
  end

  test "broadcast connect with success and force disconnection" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    %{token: token_room, room: _, room_connect: room_connect} = init_rooms

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, _} = subscribe_and_join(socket, "broadcast_channel:" <> "#{room_connect.id}", %{"subject" => token, "body" => token_room})

    ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.UserSocketWatcher, user_id)
    ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.BroadcastWatcher, user_id)

    :timer.sleep(2000)

    assert ChannelWatcher.get_uid(PandaPhoenix.Watcher.Channel.UserSocketWatcher, user_id) == :not_found
    assert ChannelWatcher.get_uid(PandaPhoenix.Watcher.Channel.BroadcastWatcher, user_id) == :not_found
  end

  defp init_rooms do
    Repo.get!(PublicRoom, 1)
                  |> PublicRoom.changeset(%{lft: 0, rgt: 15})
                  |> Repo.update!
    Repo.insert! %PublicRoom{name: "brazil_1_country", original_name: "brazil", type: "country", lft: 1, rgt: 14}
    Repo.insert! %PublicRoom{name: "são-paulo_3_state", original_name: "são paulo", type: "state", lft: 2, rgt: 13}
    Repo.insert! %PublicRoom{name: "mogi-das-cruzes_4_county", original_name: "mogi das cruzes", type: "county", lft: 3, rgt: 12}
    Repo.insert! %PublicRoom{name: "mogi-das-cruzes_5_city", original_name: "mogi das cruzes", type: "city", lft: 4, rgt: 11}
    Repo.insert! %PublicRoom{name: "NONE_6_neighborhood", original_name: "NONE", type: "neighborhood", lft: 5, rgt: 10}
    Repo.insert! %PublicRoom{name: "rua-tenente-manoel-alves-dos-anjos_7_street", original_name: "rua tenente manoel alves dos anjos", type: "street", lft: 6, rgt: 9}
    last_room = Repo.insert! %PublicRoom{name: "338_8_street_number", original_name: "338", type: "street_number", lft: 7, rgt: 8}
    Repo.insert! %PublicRoomLocalization{public_room_id: last_room.id, latitude: "10.333", longitude: "10.333", api_result: "", result: ""}

    case PublicRoom.query_path_from_public_room_id_to_root(last_room.id) do
      {:ok, public_room} ->
        %{token: Token.sign_public_room(public_room), room: public_room, room_connect: last_room}
      _ ->
        %{token: "", room: ""}
    end
  end

end
