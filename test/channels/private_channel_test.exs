defmodule PandaPhoenix.PrivateChannelTest do
  use PandaPhoenix.ChannelCase

  alias PandaPhoenix.User
  alias PandaPhoenix.PrivateRoom
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateMessage
  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.UserReportedController
  alias PandaPhoenix.Watcher.Reported.ReportedWatcher
  alias PandaPhoenix.Watcher.Channel.ChannelWatcher


  test "connect with error" do
    assert :error = connect(UserSocket, %{"token" => ""})
  end

  test "connect with error user reported active" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    ReportedWatcher.add(user_id)
    :timer.sleep(2000)

    assert :error = connect(UserSocket, %{"token" => token})
  end

  test "connect with success user reported used" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    ReportedWatcher.add(user_id)
    :timer.sleep(2000)
    assert {:ok, _} = UserReportedController.user_reported(:reset, user_id)

    assert {:ok, _} = connect(UserSocket, %{"token" => token})
    :timer.sleep(2000)
  end

  test "connect with success but join room with error" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    assert {:error, %{reason: "unauthorized"}} = subscribe_and_join(socket, "private_channel:1", %{"subject" => "", "body" => ""})
  end

  test "connect with success but be reported and logout" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name = "private_channel:" <> "#{user_id2}"
    room_name_user = "users_socket:" <> "#{user_id2}"

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, _} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})
    PandaPhoenix.Endpoint.subscribe(room_name)
    PandaPhoenix.Endpoint.subscribe(room_name_user)
    :timer.sleep(1000)

    {:ok, user_report} = UserReportedController.user_reported(user_id2)
    ReportedWatcher.add(user_report.user_id)
    :timer.sleep(2000)

    assert_receive %Phoenix.Socket.Broadcast{event: "news:user:reported", payload: %{time: 150}, topic: ^room_name}
    assert_receive %Phoenix.Socket.Broadcast{event: "disconnect", payload: %{}, topic: ^room_name_user}
    PandaPhoenix.Endpoint.unsubscribe(room_name)
    PandaPhoenix.Endpoint.unsubscribe(room_name_user)
    :timer.sleep(2000)
  end

  test "connect with success but same user connect other device" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    room_name = "private_channel:" <> "#{user_id}"
    room_name_user = "users_socket:" <> "#{user_id}"

    PandaPhoenix.Endpoint.subscribe(room_name)
    PandaPhoenix.Endpoint.subscribe(room_name_user)
    :timer.sleep(1000)

    ReportedWatcher.force_disconnect_duplicat(user_id)
    :timer.sleep(2000)

    assert_receive %Phoenix.Socket.Broadcast{event: "news:user:duplicate", payload: %{}, topic: ^room_name}
    assert_receive %Phoenix.Socket.Broadcast{event: "disconnect", payload: %{}, topic: ^room_name_user}
    PandaPhoenix.Endpoint.unsubscribe(room_name)
    PandaPhoenix.Endpoint.unsubscribe(room_name_user)
  end

  test "connect with success and got friendship list connected" do
    :timer.sleep(2000)
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    room1 = "private_channel:" <> "#{user_id}"

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    token2 = Token.sign_user(user_id2)
    Repo.insert! %UserAuthentication{user_id: user_id2, token: token2, is_online: true}
    room2 = "private_channel:" <> "#{user_id2}"

    status = UserFriendship.friendship_friend
    Repo.insert! %UserFriendship{last_action: user_id, status: status, to_user_id: user2.id, user_id: user_id}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, _} = subscribe_and_join(socket, room1, %{"subject" => token, "body" => pseudo})

    {:ok, socket2} = connect(UserSocket, %{"token" => token2})
    {:ok, _, _} = subscribe_and_join(socket2, room2, %{"subject" => token2, "body" => pseudo2})

    :timer.sleep(1000)

    # TODO - Check why why it's not working when run all test together
    # assert_receive %Phoenix.Socket.Broadcast{event: "news:users:connect", payload: %{list: [%{friendship_last_action: ^user_id, friendship_status: ^status, id: _, is_online: true, last_seen: _, pseudo: "dovi2", updated_at: _, url_blur: nil, url_original: nil, url_thumb: nil}]}, topic: room}
    # assert_receive %Phoenix.Socket.Message{event: "news:users:connect", payload: %{list: [%{friendship_last_action: ^user_id, friendship_status: ^status, id: _, is_online: true, last_seen: _, pseudo: "dovi2", updated_at: _, url_blur: nil, url_original: nil, url_thumb: nil}]}, ref: nil, topic: room}
    # assert_receive %Phoenix.Socket.Broadcast{event: "news:user:connect", payload: %{last_seen: _, user_id: ^user_id}, topic: room2}
    # assert_receive %Phoenix.Socket.Message{event: "news:user:connect", payload: %{last_seen: _, user_id: ^user_id}, ref: nil, topic: room2}
  end

  test "private connect with success and force disconnection" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, _} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})

    ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.UserSocketWatcher, user_id)
    ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.PrivateWatcher, user_id)

    :timer.sleep(2000)

    assert ChannelWatcher.get_uid(PandaPhoenix.Watcher.Channel.UserSocketWatcher, user_id) == :not_found
    assert ChannelWatcher.get_uid(PandaPhoenix.Watcher.Channel.PrivateWatcher, user_id) == :not_found
    :timer.sleep(2000)
  end

  test "send message with error" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, is_online: true}

    pseudo2 = "dovi2"
    user_id2 = 2
    Repo.insert! %UserAuthentication{user_id: user_id2, token: "123456", is_online: true}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})
    :timer.sleep(2000)

    push socket, "news:msg", %{"index" => 1,
                  "user_name" => pseudo,
                  "private_room_id" => -1,
                  "message" => "hello",
                  "to_user_id" => user_id2,
                  "to_user_name" => pseudo2,
                  "data_id" => -1}

    assert_push "news:msg:error", %{"error" => %{code: 0, fields: [%{field: :index_row, message: ["is invalid"]}], msg: "Parameter Error", type: 1}, "index" => 1, "user" => %{"friendship_last_action" => -1, "friendship_status" => 0, "from_user_id" => ^user_id, "from_user_name" => ^pseudo, "to_user_id" => ^user_id2, "to_user_name" => ^pseudo2}}
  end

  test "send message with error message nil" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    pseudo2 = "dovi2"
    user_id2 = 2

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})

    push socket, "news:msg", %{"index" => 1,
                  "user_name" => pseudo,
                  "private_room_id" => -1,
                  "message" => nil,
                  "to_user_id" => user_id2,
                  "to_user_name" => pseudo2,
                  "data_id" => -1}

    assert_push "news:msg:error", %{"error" => %{code: 0, fields: [%{field: :index_row, message: ["is invalid"]}], msg: "Parameter Error", type: 1}, "index" => 1, "user" => %{"friendship_last_action" => -1, "friendship_status" => 0, "from_user_id" => ^user_id, "from_user_name" => ^pseudo, "to_user_id" => ^user_id2, "to_user_name" => ^pseudo2}}
  end

  test "send message with error room invalid" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    pseudo2 = "dovi2"
    user_id2 = 2

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})

    :timer.sleep(2000)

    push socket, "news:msg", %{"index" => 1,
                  "user_name" => pseudo,
                  "private_room_id" => "-1",
                  "message" => "hello",
                  "to_user_id" => user_id2,
                  "to_user_name" => pseudo2,
                  "data_id" => -1}

    assert_push "news:msg:error", %{"error" => %{code: 1, msg: "room is invalid", type: 6}, "index" => 1, "user" => %{"friendship_last_action" => -1, "friendship_status" => 0, "from_user_id" => ^user_id, "from_user_name" => ^pseudo, "to_user_id" => ^user_id2, "to_user_name" => ^pseudo2}}
    :timer.sleep(2000)
  end

  test "send message without room with success" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, is_online: true}

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name = "private_channel:" <> "#{user_id2}"
    Repo.insert! %UserAuthentication{user_id: user_id2, token: "123456", is_online: true}

    {:ok, socket} = connect(UserSocket, %{"token": token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})

    PandaPhoenix.Endpoint.subscribe(room_name)
    :timer.sleep(2000)

    push socket, "news:msg", %{"index": "dovi_1",
                  "user_name": pseudo,
                  "private_room_id": -1,
                  "message": "hello",
                  "to_user_id": user_id2,
                  "to_user_name": pseudo2,
                  "data_id": -1}

    assert_push "news:msg", %{"friendship_last_action": -1,
                              "friendship_status": 0,
                              "from_user_id": ^user_id,
                              "from_user_name": ^pseudo,
                              "id": _,
                              "index": "dovi_1",
                              "inserted_at": _,
                              "msg": "hello",
                              "room_id": _,
                              "size": 0.0,
                              "status": 1,
                              "to_user_id": ^user_id2,
                              "to_user_name": ^pseudo2,
                              "type": -1,
                              "url_blur": nil,
                              "url_original": nil,
                              "url_thumb": nil
                              }

     assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{"friendship_last_action": -1,
                               "friendship_status": 0,
                               "from_user_id": ^user_id,
                               "from_user_name": ^pseudo,
                               "id": _,
                               "index": "dovi_1",
                               "inserted_at": _,
                               "msg": "hello",
                               "room_id": _,
                               "size": 0.0,
                               "status": 1,
                               "to_user_id": ^user_id2,
                               "to_user_name": ^pseudo2,
                               "type": -1,
                               "url_blur": nil,
                               "url_original": nil,
                               "url_thumb": nil}, topic: room_name}

    PandaPhoenix.Endpoint.unsubscribe(room_name)
  end

  test "send message with room with success" do
    private_room = Repo.insert! %PrivateRoom{}
    private_room_id = private_room.id

    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, is_online: true}

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name = "private_channel:" <> "#{user_id2}"
    Repo.insert! %UserAuthentication{user_id: user_id2, token: "12345", is_online: true}

    Repo.insert! %PrivateRoomUser{room_id: private_room_id, user_id: user_id}
    Repo.insert! %PrivateRoomUser{room_id: private_room_id, user_id: user_id2}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})

    PandaPhoenix.Endpoint.subscribe(room_name)
    :timer.sleep(2000)

    push socket, "news:msg", %{"index": "dovi_1",
                        "user_name": pseudo,
                        "private_room_id": private_room.id,
                        "message": "hello",
                        "to_user_id": user_id2,
                        "to_user_name": pseudo2,
                        "data_id": -1}

    assert_push "news:msg", %{"friendship_last_action": -1,
                              "friendship_status": 0,
                              "from_user_id": ^user_id,
                              "from_user_name": ^pseudo,
                              "id": _,
                              "index": "dovi_1",
                              "inserted_at": _,
                              "msg": "hello",
                              "room_id": ^private_room_id,
                              "size": 0.0,
                              "status": 1,
                              "to_user_id": ^user_id2,
                              "to_user_name": ^pseudo2,
                              "type": -1,
                              "url_blur": nil,
                              "url_original": nil,
                              "url_thumb": nil}

    assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{"friendship_last_action": -1,
                              "friendship_status": 0,
                              "from_user_id": ^user_id,
                              "from_user_name": ^pseudo,
                              "id": _,
                              "index": "dovi_1",
                              "inserted_at": _,
                              "msg": "hello",
                              "room_id": ^private_room_id,
                              "size": 0.0,
                              "status": 1,
                              "to_user_id": ^user_id2,
                              "to_user_name": ^pseudo2,
                              "type": -1,
                              "url_blur": nil,
                              "url_original": nil,
                              "url_thumb": nil}, topic: room_name}
    PandaPhoenix.Endpoint.unsubscribe(room_name)
  end


  test "send message without room but already have one with success" do
    private_room = Repo.insert! %PrivateRoom{}
    private_room_id = private_room.id

    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, is_online: true}

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name = "private_channel:" <> "#{user_id2}"
    Repo.insert! %UserAuthentication{user_id: user_id2, token: "123456", is_online: true}

    Repo.insert! %PrivateRoomUser{room_id: private_room_id, user_id: user_id}
    Repo.insert! %PrivateRoomUser{room_id: private_room_id, user_id: user_id2}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})

    PandaPhoenix.Endpoint.subscribe(room_name)
    :timer.sleep(2000)

    push socket, "news:msg", %{"index": "dovi_1",
                  "user_name": pseudo,
                  "private_room_id": -1,
                  "message": "hello",
                  "to_user_id": user_id2,
                  "to_user_name": pseudo2,
                  "data_id": -1}

    assert_push "news:msg", %{"friendship_last_action": -1,
                              "friendship_status": 0,
                              "from_user_id": ^user_id,
                              "from_user_name": ^pseudo,
                              "id": _,
                              "index": "dovi_1",
                              "inserted_at": _,
                              "msg": "hello",
                              "size": 0.0,
                              "room_id": ^private_room_id,
                              "status": 1,
                              "to_user_id": ^user_id2,
                              "to_user_name": ^pseudo2,
                              "type": -1,
                              "url_blur": nil,
                              "url_original": nil,
                              "url_thumb": nil}

    assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{"friendship_last_action": -1,
                              "friendship_status": 0,
                              "from_user_id": ^user_id,
                              "from_user_name": ^pseudo,
                              "id": _,
                              "index": "dovi_1",
                              "inserted_at": _,
                              "msg": "hello",
                              "size": 0.0,
                              "room_id": ^private_room_id,
                              "status": 1,
                              "to_user_id": ^user_id2,
                              "to_user_name": ^pseudo2,
                              "inserted_at": _,
                              "type": -1,
                              "url_blur": nil,
                              "url_original": nil,
                              "url_thumb": nil}, topic: room_name}
    PandaPhoenix.Endpoint.unsubscribe(room_name)
  end

  test "send message with with friendship with success" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, is_online: true}

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name = "private_channel:" <> "#{user_id2}"
    Repo.insert! %UserAuthentication{user_id: user_id2, token: "123456", is_online: true}

    not_friend = UserFriendship.friendship_not_friend

    Repo.insert! %UserFriendship{user_id: user_id, to_user_id: user_id2, status: not_friend, last_action: user_id}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})

    PandaPhoenix.Endpoint.subscribe(room_name)
    :timer.sleep(2000)

    push socket, "news:msg", %{"index": "dovi_1",
                  "user_name": pseudo,
                  "private_room_id": -1,
                  "message": "hello",
                  "to_user_id": user_id2,
                  "to_user_name": pseudo2,
                  "data_id": -1}

    assert_push "news:msg", %{"friendship_last_action": ^user_id,
                              "friendship_status": ^not_friend,
                              "from_user_id": ^user_id,
                              "from_user_name": ^pseudo,
                              "id": _,
                              "index": "dovi_1",
                              "inserted_at": _,
                              "msg": "hello",
                              "room_id": _,
                              "size": 0.0,
                              "status": 1,
                              "to_user_id": ^user_id2,
                              "to_user_name": ^pseudo2,
                              "type": -1,
                              "url_blur": nil,
                              "url_original": nil,
                              "url_thumb": nil}

    assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{"friendship_last_action": ^user_id,
                              "friendship_status": ^not_friend,
                              "from_user_id": ^user_id,
                              "from_user_name": ^pseudo,
                              "id": _,
                              "index": "dovi_1",
                              "inserted_at": _,
                              "msg": "hello",
                              "room_id": _,
                              "size": 0.0,
                              "status": 1,
                              "to_user_id": ^user_id2,
                              "to_user_name": ^pseudo2,
                              "type": -1,
                              "url_blur": nil,
                              "url_original": nil,
                              "url_thumb": nil}, topic: ^room_name}
    PandaPhoenix.Endpoint.unsubscribe(room_name)
  end

  test "send message with with friendship 'friendship_blocked_before_be_friend' with error" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name = "private_channel:" <> "#{user_id2}"

    not_friend = UserFriendship.friendship_blocked_before_be_friend

    Repo.insert! %UserFriendship{user_id: user_id, to_user_id: user_id2, status: not_friend, last_action: user_id}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})

    PandaPhoenix.Endpoint.subscribe(room_name)

    :timer.sleep(2000)

    push socket, "news:msg", %{"index" => "dovi_1",
                  "user_name" => pseudo,
                  "private_room_id" => -1,
                  "message" => "hello",
                  "to_user_id" => user_id2,
                  "to_user_name" => pseudo2,
                  "data_id" => -1}

    assert_push "news:msg:error", %{"error" => %{code: 3, msg: "message blocked", type: 6}, "index" => "dovi_1", "user" => ^user_id}

    refute_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{}}
    refute_receive %Phoenix.Socket.Broadcast{event: "news:msg:error", payload: %{}}

    PandaPhoenix.Endpoint.unsubscribe(room_name)
  end

  test "receive message with error" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name2 = "private_channel:" <> "#{user_id2}"

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})
    :timer.sleep(1000)

    PandaPhoenix.Endpoint.subscribe(room_name2)
    :timer.sleep(1000)

    message_received = PrivateMessage.message_received
    push socket, "news:msg:confirmation", %{"messages" => [%{"id" => -1, "status" => message_received}]}
    :timer.sleep(1000)

    assert_receive %Phoenix.Socket.Message{event: "news:msg:pending", payload: %{backup: [], user_id: user_id}, ref: nil, topic: room_name}
    refute_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{messages: [%{id: -1, status: message_received}]}, topic: room_name2}
    refute_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{}}

    PandaPhoenix.Endpoint.unsubscribe(room_name2)
  end

  test "receive message with success" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    room_name = "private_channel:" <> "#{user_id}"

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name2 = "private_channel:" <> "#{user_id2}"

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})
    :timer.sleep(1000)

    PandaPhoenix.Endpoint.subscribe(room_name2)
    :timer.sleep(1000)

    Repo.insert! %PrivateRoomUser{user_id: user_id, room_id: 1}
    Repo.insert! %PrivateRoomUser{user_id: user_id2, room_id: 1}
    message = Repo.insert! %PrivateMessage{msg: "message",
                                          status: PrivateMessage.message_sending,
                                          from_user_id: user_id2,
                                          type: 0,
                                          room_id: 1,
                                          index_row: pseudo2 <> "#{user_id2}"}
    message_id = message.id
    message_received = PrivateMessage.message_received

    push socket, "news:msg:confirmation", %{"messages" => [%{"id" => message_id, "status" => message_received}]}
    :timer.sleep(1000)

    assert_receive %Phoenix.Socket.Message{event: "news:msg:pending", payload: %{backup: [], user_id: user_id}}
    assert_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{messages: [%{id: message_id, status: message_received}]}}
    refute_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{}}

    PandaPhoenix.Endpoint.unsubscribe(room_name2)
  end

  test "read message with error" do

    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token, is_online: true}

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name2 = "private_channel:" <> "#{user_id2}"
    Repo.insert! %UserAuthentication{user_id: user_id2, token: "123456", is_online: true}

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})
    :timer.sleep(1000)

    PandaPhoenix.Endpoint.subscribe(room_name2)
    :timer.sleep(1000)

    message_read = PrivateMessage.message_read

    push socket, "news:msg:confirmation", %{"messages" => [%{"id" => -1, "status" => message_read}]}
    :timer.sleep(1000)

    assert_receive %Phoenix.Socket.Message{event: "news:msg:pending", payload: %{backup: [], user_id: user_id}}
    refute_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{messages: [%{id: -1, status: message_read}]}}
    refute_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{}}

    PandaPhoenix.Endpoint.unsubscribe(room_name2)

  end

  test "read message with success" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}
    room_name = "private_channel:" <> "#{user_id}"

    pseudo2 = "dovi2"
    user2 = Repo.insert! %User{pseudo: pseudo2}
    user_id2 = user2.id
    room_name2 = "private_channel:" <> "#{user_id2}"

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "private_channel:" <> "#{user_id}", %{"subject" => token, "body" => pseudo})
    :timer.sleep(1000)

    PandaPhoenix.Endpoint.subscribe(room_name2)
    :timer.sleep(1000)

    Repo.insert! %PrivateRoomUser{user_id: user_id, room_id: 1}
    Repo.insert! %PrivateRoomUser{user_id: user_id2, room_id: 1}
    message = Repo.insert! %PrivateMessage{msg: "message",
                                          status: PrivateMessage.message_sending,
                                          from_user_id: user_id2,
                                          type: 0,
                                          room_id: 1,
                                          index_row: pseudo2 <> "#{user_id2}"}
    message_id = message.id
    message_read = PrivateMessage.message_read

    push socket, "news:msg:confirmation", %{"messages" => [%{"id" => message_id, "status" => message_read}]}
    :timer.sleep(1000)

    assert_receive %Phoenix.Socket.Message{event: "news:msg:pending", payload: %{backup: [], user_id: user_id}}
    assert_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{messages: [%{id: message_id, status: message_read}]}}
    refute_receive %Phoenix.Socket.Broadcast{event: "news:msg:confirmation", payload: %{}}

    PandaPhoenix.Endpoint.unsubscribe(room_name2)
  end

end
