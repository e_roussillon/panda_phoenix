defmodule PandaPhoenix.SaleChannelTest do
  use PandaPhoenix.ChannelCase

  alias PandaPhoenix.User
  alias PandaPhoenix.PublicRoom
  alias PandaPhoenix.PublicRoomLocalization
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.Watcher.Channel.ChannelWatcher


  test "connect with success but join room with error" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    %{token: _, room: _, room_connect: room_connect} = init_rooms

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    assert {:error, %{reason: "unauthorized"}} = subscribe_and_join(socket, "sale_channel:" <> "#{room_connect.id}", %{"subject" => "", "body" => ""})
  end

  test "sale connect with success and force disconnection" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    %{token: token_room, room: _, room_connect: room_connect} = init_rooms

    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, _} = subscribe_and_join(socket, "sale_channel:" <> "#{room_connect.id}", %{"subject" => token, "body" => token_room})

    ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.UserSocketWatcher, user_id)
    ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.SaleWatcher, user_id)

    :timer.sleep(2000)

    assert ChannelWatcher.get_uid(PandaPhoenix.Watcher.Channel.UserSocketWatcher, user_id) == :not_found
    assert ChannelWatcher.get_uid(PandaPhoenix.Watcher.Channel.SaleWatcher, user_id) == :not_found
  end

  test "send message with error" do
    pseudo = "dovi"
    user = Repo.insert! %User{pseudo: pseudo}
    user_id = user.id
    token = Token.sign_user(user_id)
    Repo.insert! %UserAuthentication{user_id: user_id, token: token}

    %{token: token_room, room: room, room_connect: room_connect} = init_rooms

    room_name0 = "sale_channel:" <> "#{Enum.at(room, 0) |> Map.fetch!(:id)}"
    room_name1 = "sale_channel:" <> "#{Enum.at(room, 1) |> Map.fetch!(:id)}"
    room_name2 = "sale_channel:" <> "#{Enum.at(room, 2) |> Map.fetch!(:id)}"
    room_name3 = "sale_channel:" <> "#{Enum.at(room, 3) |> Map.fetch!(:id)}"
    room_name4 = "sale_channel:" <> "#{Enum.at(room, 4) |> Map.fetch!(:id)}"
    room_name5 = "sale_channel:" <> "#{Enum.at(room, 5) |> Map.fetch!(:id)}"
    room_name6 = "sale_channel:" <> "#{Enum.at(room, 6) |> Map.fetch!(:id)}"

    PandaPhoenix.Endpoint.subscribe(room_name0)
    PandaPhoenix.Endpoint.subscribe(room_name1)
    PandaPhoenix.Endpoint.subscribe(room_name2)
    PandaPhoenix.Endpoint.subscribe(room_name3)
    PandaPhoenix.Endpoint.subscribe(room_name4)
    PandaPhoenix.Endpoint.subscribe(room_name5)
    PandaPhoenix.Endpoint.subscribe(room_name6)


    {:ok, socket} = connect(UserSocket, %{"token" => token})
    {:ok, _, socket} = subscribe_and_join(socket, "sale_channel:" <> "#{room_connect.id}", %{"subject" => token, "body" => token_room})

    push socket, "news:msg", %{"message" => "hello",
                               "user" => pseudo,
                               "index" => 1}


    assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{from_user_id: ^user_id,
                                                                           from_user_name: ^pseudo,
                                                                           id: _,
                                                                           index: 1,
                                                                           is_general: false,
                                                                           msg: "hello",
                                                                           inserted_at: _}, topic: room_name0}
     assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{from_user_id: ^user_id,
                                                                            from_user_name: ^pseudo,
                                                                            id: _,
                                                                            index: 1,
                                                                            is_general: false,
                                                                            msg: "hello",
                                                                            inserted_at: _}, topic: room_name1}
     assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{from_user_id: ^user_id,
                                                                            from_user_name: ^pseudo,
                                                                            id: _,
                                                                            index: 1,
                                                                            is_general: false,
                                                                            msg: "hello",
                                                                            inserted_at: _}, topic: room_name2}
     assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{from_user_id: ^user_id,
                                                                            from_user_name: ^pseudo,
                                                                            id: _,
                                                                            index: 1,
                                                                            is_general: false,
                                                                            msg: "hello",
                                                                            inserted_at: _}, topic: room_name3}
     assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{from_user_id: ^user_id,
                                                                            from_user_name: ^pseudo,
                                                                            id: _,
                                                                            index: 1,
                                                                            is_general: false,
                                                                            msg: "hello",
                                                                            inserted_at: _}, topic: room_name4}
    assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{from_user_id: ^user_id,
                                                                           from_user_name: ^pseudo,
                                                                           id: _,
                                                                           index: 1,
                                                                           is_general: false,
                                                                           msg: "hello",
                                                                           inserted_at: _}, topic: room_name5}
    assert_receive %Phoenix.Socket.Broadcast{event: "news:msg", payload: %{from_user_id: ^user_id,
                                                                           from_user_name: ^pseudo,
                                                                           id: _,
                                                                           index: 1,
                                                                           is_general: false,
                                                                           msg: "hello",
                                                                           inserted_at: _}, topic: room_name6}
   assert_broadcast "news:msg", %{from_user_id: ^user_id,
                                  from_user_name: ^pseudo,
                                  id: _,
                                  index: 1,
                                  is_general: false,
                                  msg: "hello",
                                  inserted_at: _}

    PandaPhoenix.Endpoint.unsubscribe(room_name0)
    PandaPhoenix.Endpoint.unsubscribe(room_name1)
    PandaPhoenix.Endpoint.unsubscribe(room_name2)
    PandaPhoenix.Endpoint.unsubscribe(room_name3)
    PandaPhoenix.Endpoint.unsubscribe(room_name4)
    PandaPhoenix.Endpoint.unsubscribe(room_name5)
    PandaPhoenix.Endpoint.unsubscribe(room_name6)
  end


  defp init_rooms do
    Repo.get!(PublicRoom, 1)
                  |> PublicRoom.changeset(%{lft: 0, rgt: 15})
                  |> Repo.update!
    Repo.insert! %PublicRoom{name: "brazil_1_country", original_name: "brazil", type: "country", lft: 1, rgt: 14}
    Repo.insert! %PublicRoom{name: "são-paulo_3_state", original_name: "são paulo", type: "state", lft: 2, rgt: 13}
    Repo.insert! %PublicRoom{name: "mogi-das-cruzes_4_county", original_name: "mogi das cruzes", type: "county", lft: 3, rgt: 12}
    Repo.insert! %PublicRoom{name: "mogi-das-cruzes_5_city", original_name: "mogi das cruzes", type: "city", lft: 4, rgt: 11}
    Repo.insert! %PublicRoom{name: "NONE_6_neighborhood", original_name: "NONE", type: "neighborhood", lft: 5, rgt: 10}
    Repo.insert! %PublicRoom{name: "rua-tenente-manoel-alves-dos-anjos_7_street", original_name: "rua tenente manoel alves dos anjos", type: "street", lft: 6, rgt: 9}
    last_room = Repo.insert! %PublicRoom{name: "338_8_street_number", original_name: "338", type: "street_number", lft: 7, rgt: 8}
    Repo.insert! %PublicRoomLocalization{public_room_id: last_room.id, latitude: "10.333", longitude: "10.333", api_result: "", result: ""}

    case PublicRoom.query_path_from_public_room_id_to_root(last_room.id) do
      {:ok, public_room} ->
        %{token: Token.sign_public_room(public_room), room: public_room, room_connect: last_room}
      _ ->
        %{token: "", room: ""}
    end
  end

end
