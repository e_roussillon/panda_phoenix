ExUnit.start

# Mix.Task.run "ecto.create", ~w(-r PandaPhoenix.Repo --quiet)
# Mix.Task.run "ecto.migrate", ~w(-r PandaPhoenix.Repo --quiet)
Ecto.Adapters.SQL.Sandbox.mode(PandaPhoenix.Repo, :manual)
