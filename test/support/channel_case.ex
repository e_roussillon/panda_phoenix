defmodule PandaPhoenix.ChannelCase do
  @moduledoc """
  This module defines the test case to be used by
  channel tests.

  Such tests rely on `Phoenix.ChannelTest` and also
  imports other functionality to make it easier
  to build and query models.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with channels
      use Phoenix.ChannelTest

      alias PandaPhoenix.Token
      alias PandaPhoenix.UserSocket
      alias PandaPhoenix.Repo
      alias PandaPhoenix.Logger

      import Ecto
      import Ecto.Changeset
      import Ecto.Query, only: [from: 1, from: 2]


      # The default endpoint for testing
      @endpoint PandaPhoenix.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(PandaPhoenix.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(PandaPhoenix.Repo, {:shared, self()})
    end

    :ok
  end

  # def clean_up_stray_processes() do
  #   Task.Supervisor.children(MyApp.TaskSupervisor)
  #   |> Enum.each(fn pid ->
  #     Process.exit(pid, :kill)
  #   end)
  # end
end
