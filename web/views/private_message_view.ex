defmodule PandaPhoenix.PrivateMessageView do
  @moduledoc false

  use PandaPhoenix.Web, :view

  alias PandaPhoenix.Data
  # alias PandaPhoenix.Logger

  # def render("index.json", %{private_messages: private_messages}) do
  #   %{data: render_many(private_messages, PandaPhoenix.PrivateMessageView, "private_message.json")}
  # end

  def render("show.json", %{private_message: private_message, user: user}) do
    # render_one(%{private_message: private_message, user: from_user, index: index}, PandaPhoenix.PrivateMessageView, "private_message.json")
    render("private_message.json", %{private_message: private_message, user: user})
  end

  def render("private_message.json", %{private_message: private_message, user: user}) do
    %{ id: private_message.id,
       index: private_message.index_row,
       room_id: private_message.room_id,
       msg: private_message.msg,
       status: private_message.status,
       from_user_id: Map.fetch!(user, "from_user_id"),
       from_user_name: Map.fetch!(user, "from_user_name"),
       to_user_id: Map.fetch!(user, "to_user_id"),
       to_user_name: Map.fetch!(user, "to_user_name"),
       friendship_status: Map.fetch!(user, "friendship_status"),
       friendship_last_action: Map.fetch!(user, "friendship_last_action"),
       url_thumb: private_message.url_thumb,
       url_original: private_message.url_original,
       url_blur: private_message.url_blur,
       type: get_type(private_message.type),
       size: get_float(private_message.size),
       inserted_at: get_inserted_at(%{private_message: private_message, user: user}) }
  end

  defp get_float(size) do
    if is_nil(size) do
      0.0
    else
      Float.floor(size, 2)
    end
  end

  defp get_type(type) do
    if is_nil(type) do
      Data.data_none
    else
      type
    end
  end

  defp get_inserted_at(%{private_message: private_message, user: user}) do
    case private_message.from_user_id == Map.fetch!(user, "from_user_id") && is_nil(private_message.type) == false && private_message.type >= Data.data_image do
      true ->
        private_message.data_inserted_at
      false ->
        private_message.inserted_at
    end

  end
end
