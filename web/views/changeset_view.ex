defmodule PandaPhoenix.ChangesetView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  alias PandaPhoenix.ErrorChangeset

  #
  # PRIVATE
  #

  defp translate_errors(changeset) do
    map = Ecto.Changeset.traverse_errors(changeset, &translate_error/1)

    map
      |> Map.keys()
      |> translate_errors(map, [])
      |> ErrorChangeset.init_error()
  end

  defp translate_errors([], _, result) do
    result
  end

  defp translate_errors([h|t], map, result) do
    item = %{field: h, message: Map.get(map, h)}
    translate_errors(t, map, List.insert_at(result, 0, item))
  end

  #
  # API
  #

  def render("error.json", %{changeset: changeset}) do
    # When encoded, the changeset returns its errors
    # as a JSON object. So we just pass it forward.
    translate_errors(changeset)
    # %{errors: changeset}
  end

  def render("error.json", %{type: type, code: number, msg: message, details: details}) do
    # When encoded, the changeset returns its errors
    # as a JSON object. So we just pass it forward.
    %{type: type, code: number, msg: message, details: details}
  end

  def render("error.json", %{type: type, code: number, msg: message}) do
    # When encoded, the changeset returns its errors
    # as a JSON object. So we just pass it forward.
    %{type: type, code: number, msg: message}
  end

end
