defmodule PandaPhoenix.BackupView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  def render("show.json", %{backup: backup, user_id: user_id}) do
    %{user_id: user_id,
      backup: backup}
  end

end
