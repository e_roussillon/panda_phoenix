defmodule PandaPhoenix.UserView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  alias PandaPhoenix.UserFriendship

  def render("index.json", %{users: users}) do
    render_many(users, PandaPhoenix.UserView, "user.json")
  end

  def render("index.json", %{paginate: page}) do
    # When encoded, the changeset returns its errors
    # as a JSON object. So we just pass it forward.
    list = render_many(page.entries, PandaPhoenix.UserView, "user.json")
    %{entries: list, page_number: page.page_number, page_size: page.page_size, total_entries: page.total_entries, total_pages: page.total_pages}
  end

  def render("show.json", %{user: user}) do
    render_one(user, PandaPhoenix.UserView, "user.json")
  end

  def render("user.json", %{user: user}) do
    case Map.has_key?(user, :email) do
      true ->
        %{id: user.id,
          pseudo: user.pseudo,
          email: user.email,
          birthday: user.birthday,
          gender: user.gender,
          description: user.description,
          url_thumb: user.url_thumb,
          url_original: user.url_original,
          url_blur: user.url_blur,
          updated_at: user.updated_at}
      false ->
        case Map.has_key?(user, :description) do
          true ->
            %{id: user.id,
              pseudo: user.pseudo,
              birthday: user.birthday,
              gender: user.gender,
              description: user.description,
              url_thumb: user.url_thumb,
              url_original: user.url_original,
              url_blur: user.url_blur,
              friendship_status: init_status(user.friendship_status),
              friendship_last_action: user.friendship_last_action,
              is_online: init_online(user.is_online),
              last_seen: user.last_seen,
              reported: init_online(user.reported),
              updated_at: user.updated_at}
          false ->
            %{id: user.id,
              pseudo: user.pseudo,
              url_thumb: user.url_thumb,
              url_original: user.url_original,
              url_blur: user.url_blur,
              friendship_status: init_status(user.friendship_status),
              friendship_last_action: user.friendship_last_action,
              is_online: init_online(user.is_online),
              last_seen: user.last_seen,
              updated_at: user.updated_at}
        end
    end
  end

  defp init_online(value) do
    case value do
      nil ->
        false
      true ->
        true
      false ->
        false
      _ ->
        true
    end
  end

  defp init_status(value) do
    case value do
      nil ->
        UserFriendship.friendship_not_friend
      _ ->
        value
    end
  end

end
