defmodule PandaPhoenix.GenericMessageView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view


  def render("show.json", %{index: index, error: error}) do
    %{index: index,
      error: error.errors}
  end

  # def render("show.json", %{index: index, response: response}) do
  #   %{index: index,
  #     reponse: response}
  # end

end
