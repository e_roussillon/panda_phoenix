defmodule PandaPhoenix.MessageSenderView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  def render("index.json", %{number: number}) do
    %{
      number: number
    }
  end

end
