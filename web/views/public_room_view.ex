defmodule PandaPhoenix.PublicRoomView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  # def render("index.json", %{public_rooms: public_rooms}) do
  #   %{data: render_many(public_rooms, PandaPhoenix.PublicRoomView, "public_room.json")}
  # end
  #
  # def render("show.json", %{public_room: public_room}) do
  #   %{data: render_one(public_room, PandaPhoenix.PublicRoomView, "public_room.json")}
  # end

  def render("token.json", %{token: token, public_room: public_rooms}) do
    %{token: token, rooms: render_many(public_rooms, PandaPhoenix.PublicRoomView, "public_room.json")}
  end

  def render("public_room.json", %{public_room: public_room}) do
    %{id: public_room.id,
      name: public_room.name,
      original_name: public_room.original_name,
      type: public_room.type
    }
  end
end
