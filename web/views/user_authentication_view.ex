defmodule PandaPhoenix.UserAuthenticationView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  def render("show.json", %{user_authentication: user_authentication}) do
    %{token: user_authentication.token,
      refresh_token: user_authentication.refresh_token}
  end

end
