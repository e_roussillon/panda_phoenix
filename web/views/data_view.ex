defmodule PandaPhoenix.DataView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  # def render("index.json", %{datas: datas}) do
  #   render_many(datas, PandaPhoenix.DataView, "data.json")
  # end

  def render("show.json", %{data: data}) do
    render_one(data, PandaPhoenix.DataView, "data.json")
  end

  def render("data.json", %{data: data}) do
    %{id: data.id,
      url_thumb: data.url_thumb,
      url_original: data.url_original,
      url_blur: data.url_blur,
      type: data.type,}
  end
end
