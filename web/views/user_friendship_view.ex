defmodule PandaPhoenix.UserFriendshipView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  # def render("index.json", %{user_friendships: user_friendships}) do
  #   render_many(user_friendships, PandaPhoenix.UserFriendshipView, "user_friendship.json")
  # end

  def render("show.json", %{user_friendship: user_friendship}) do
    render_one(user_friendship, PandaPhoenix.UserFriendshipView, "user_friendship.json")
  end

  def render("user_friendship.json", %{user_friendship: user_friendship}) do
    %{id: user_friendship.id,
      user_id: user_friendship.user_id,
      to_user_id: user_friendship.to_user_id,
      status: user_friendship.status,
      last_action: user_friendship.last_action}
  end
end
