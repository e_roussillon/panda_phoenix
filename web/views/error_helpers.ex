defmodule PandaPhoenix.ErrorHelpers do
  @moduledoc false

  use Phoenix.HTML
  alias PandaPhoenix.Logger


  def error_tag(form, field) do
    Logger.error(__ENV__, "error_tag", [%{errors: form}, %{field: field}])
    if error = form.source.errors[field] do
      content_tag :span, translate_error(error), class: "help-block"
    end
  end

  def translate_error({msg, opts}) do
    # Because error messages were defined within Ecto, we must
    # call the Gettext module passing our Gettext backend. We
    # also use the "errors" domain as translations are placed
    # in the errors.po file. On your own code and templates,
    # this could be written simply as:
    #
    #     dngettext "errors", "1 file", "%{count} files", count
    #
    if count = opts[:count] do
      Gettext.dngettext(PandaPhoenix.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(PandaPhoenix.Gettext, "errors", msg, opts)
    end
  end

  def translate_error(msg) do
    Gettext.dgettext(PandaPhoenix.Gettext, "errors", msg)
  end
end
