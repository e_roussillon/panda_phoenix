defmodule PandaPhoenix.UserFilterView do
  @moduledoc false
  
  use PandaPhoenix.Web, :view

  # def render("index.json", %{user_filters: user_filters}) do
  #   render_many(user_filters, PandaPhoenix.UserFilterView, "user_filter.json")
  # end

  def render("show.json", %{user_filter: user_filter}) do
    render_one(user_filter, PandaPhoenix.UserFilterView, "user_filter.json")
  end

  def render("user_filter.json", %{user_filter: user_filter}) do
    %{id: user_filter.id,
      name: user_filter.name,
      is_highlight: user_filter.is_highlight,
      user_id: user_filter.user_id}
  end
end
