defmodule PandaPhoenix.PublicMessageView do
  @moduledoc false

  use PandaPhoenix.Web, :view
  alias PandaPhoenix.Data

  # def render("index.json", %{public_messages: public_messages}) do
  #   render_many(public_messages, PandaPhoenix.PublicMessageView, "public_message.json")
  # end

  def render("show.json", %{public_message: public_message, data: data, user_name: user_name, index: index}) do
    %{id: public_message.id,
      msg: public_message.msg,
      from_user_id: public_message.from_user_id,
      from_user_name: user_name,
      url_thumb: data.url_thumb,
      url_original: data.url_original,
      url_blur: data.url_blur,
      type: get_type(data.type),
      inserted_at: public_message.inserted_at |> PandaPhoenix.DateUtil.ecto_to_timestamp,
      is_general: public_message.is_general,
      index: index}
  end

  def render("show.json", %{public_message: public_message, user_name: user_name, index: index}) do
    %{id: public_message.id,
      msg: public_message.msg,
      from_user_id: public_message.from_user_id,
      from_user_name: user_name,
      url_thumb: nil,
      url_original: nil,
      url_blur: nil,
      type: get_type(nil),
      inserted_at: public_message.inserted_at |> PandaPhoenix.DateUtil.ecto_to_timestamp,
      is_general: public_message.is_general,
      index: index}
  end

  defp get_type(type) do
    if is_nil(type) do
      Data.data_none
    else
      type
    end
  end

end
