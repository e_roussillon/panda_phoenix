defmodule PandaPhoenix.Plug.RateLimit do
  @moduledoc false

  import Plug.Conn, only: [put_resp_content_type: 2, send_resp: 3, halt: 1]

  alias PandaPhoenix.ErrorAuth
  # alias PandaPhoenix.Logger

  @rate_limit_interval_seconds Application.get_env(:panda_phoenix, :rate_limit_interval_seconds) * 1000
  @rate_limit_max_requests Application.get_env(:panda_phoenix, :rate_limit_max_requests)

  def init(options) do
    options
  end

  def call(conn, _) do
    case check_rate(conn) do
      {:ok, _count}   -> conn # Do nothing, allow execution to continue
      _ ->
        render_error(conn)
    end
  end

  defp check_rate(conn) do
    case Application.get_env(:panda_phoenix, :environment) do
      :test ->
          {:ok, 0}
      _other ->
        ExRated.check_rate(bucket_name(conn), @rate_limit_interval_seconds, @rate_limit_max_requests)
    end
  end

  defp inspect_bucket(conn) do
    ExRated.inspect_bucket(bucket_name(conn), @rate_limit_interval_seconds, @rate_limit_max_requests)
  end

  # Bucket name should be a combination of ip address and request path, like so:
  #
  # "127.0.0.1:/api/v1/authorizations"
  defp bucket_name(conn) do
    path = Enum.join(conn.path_info, "/")
    ip   = conn.remote_ip |> Tuple.to_list |> Enum.join(".")
    "#{ip}:#{path}"
  end

  defp render_error(conn) do
    {_, _, time_ml, _, _} = inspect_bucket(conn)

    result = PandaPhoenix.ChangesetView.render("error.json", ErrorAuth.init_error_rating_limit(%{time: round(time_ml/1000)}))
    conn
      |> put_resp_content_type("application/json")
      |> send_resp(:unauthorized, Poison.encode!(result))
      |> halt
  end
end
