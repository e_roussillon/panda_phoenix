defmodule PandaPhoenix.Plug.Authenticate do
  @moduledoc false
  
  import Plug.Conn

  alias PandaPhoenix.Token
  alias PandaPhoenix.ErrorAuth

  def init(options) do
    options
  end

  def call(conn, _) do
    map = Enum.into(conn.req_headers, %{})
    case validate_token(conn, map["authorization"]) do
      :duplicate_user ->
        result = PandaPhoenix.ChangesetView.render("error.json", ErrorAuth.init_error_duplicate_user())
        conn
          |> put_resp_content_type("application/json")
          |> send_resp(:conflict, Poison.encode!(result))
          |> halt
      :missing ->
        result = PandaPhoenix.ChangesetView.render("error.json", ErrorAuth.init_error_token_not_found())
        conn
          |> put_resp_content_type("application/json")
          |> send_resp(:not_found, Poison.encode!(result))
          |> halt
      :invalid ->
        result = PandaPhoenix.ChangesetView.render("error.json", ErrorAuth.init_error_token_invalid())
        conn
          |> put_resp_content_type("application/json")
          |> send_resp(:unauthorized, Poison.encode!(result))
          |> halt
      {:reported, %{time: time}, _} ->
        result = PandaPhoenix.ChangesetView.render("error.json", ErrorAuth.init_error_user_blocked(%{time: time}))
        conn
          |> put_resp_content_type("application/json")
          |> send_resp(:unauthorized, Poison.encode!(result))
          |> halt
      {:authenticated, user_id} ->
        conn |> assign(:user_id, user_id)
    end
  end

  defp validate_token(_, token) do
    case token do
      nil ->
        :missing
      _ ->
        Token.verify_user(%{token: String.replace_prefix(token, "Bearer ", ""), with_disconnection: true, can_active: false})
    end
  end

end
