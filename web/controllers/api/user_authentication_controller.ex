defmodule PandaPhoenix.UserAuthenticationController do
  @moduledoc """
  Provide method to login, logout and renew token of a user
  """

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.ErrorAuth
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.User
  alias PandaPhoenix.Token
  alias PandaPhoenix.Watcher.Reported.ReportedWatcher

  #
  # API
  #

  @doc """
  Login a user

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'password: string' \

      -d '{"email": string}' \

      'http://localhost:4000/api/v1/auth'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'password: string' -d '{"email": "string"}' 'http://localhost:4000/api/v1/auth'

  ## Return
      {"token":"string","refresh_token":"string"}
  """
  def login(conn, %{"email" => email}) do
    map = Enum.into(conn.req_headers, %{})
    check_password = map["password"]
    query = User.query_user_by_email_and_pws(email)
    case Repo.one(query) do
      {user_id, password, active} ->
        case check_password(password, check_password)  do
          true ->
            case validate_user(email) do
              {:reported, %{time: time}} ->
                conn
                  |> put_status(:unauthorized)
                  |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_user_blocked(%{time: time}))
              :ok ->
                case active do
                  true ->
                    user_params = Map.new()
                                  |> Map.put("token", Token.sign_user(user_id))
                                  |> Map.put("refresh_token", Token.sign_user_refrech(user_id))
                                  |> Map.put("is_online", false)
                                  |> Map.put("user_id", user_id)
                    ReportedWatcher.force_disconnect_duplicat(user_id)

                    case update(%{"id" =>user_id, "user_authentication" => user_params}) do
                      {:ok, %{user_authentication: user_authentication}} ->
                        conn
                          |> put_status(:created)
                          |> render(PandaPhoenix.UserAuthenticationView, "show.json", user_authentication: user_authentication)
                      _ ->
                        conn
                          |> put_status(:unprocessable_entity)
                          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_user_not_found())
                    end

                  false ->
                    user = Repo.get!(User, user_id)
                    PandaPhoenix.Email.welcome_email(user)
                    conn
                      |> put_status(:unprocessable_entity)
                      |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_not_active())
                end

            end
          _ ->
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_user_not_found())
        end
      _ ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_user_not_found())
    end
  end

  @doc """
  Renew token user

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      --header 'Authorization_Refesh: Bearer token' \

      'http://localhost:4000/api/v1/auth/renew'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' --header 'Authorization_Refesh: Bearer token' 'http://localhost:4000/api/v1/auth/renew'

  ## Return
      {"token":"string","refresh_token":"string"}
  """
  def renew_token(conn, _param) do
    map = Enum.into(conn.req_headers, %{})
    token = map["authorization"]
    token_refesh = map["authorization_refesh"]

    case is_nil(token) || is_nil(token_refesh) do
      true ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_token_not_renew())
      false ->
        case Token.verify_for_renew(token |> String.replace_prefix("Bearer ", "") , token_refesh |> String.replace_prefix("Bearer ", "")) do
          %{token: token, refresh_token: refresh_token, user_id: user_id} ->
            user_params = Map.new()
                          |> Map.put("token", token)
                          |> Map.put("refresh_token", refresh_token)
                          |> Map.put("user_id", user_id)

            case update(%{"id" =>user_id, "user_authentication" => user_params}) do
              {:ok, %{user_authentication: user_authentication}} ->
                conn
                  |> put_status(:created)
                  |> render(PandaPhoenix.UserAuthenticationView, "show.json", user_authentication: user_authentication)
              _ ->
                conn
                  |> put_status(:unprocessable_entity)
                  |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_token_not_renew())
            end
          :invalid ->
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_token_not_renew())
        end
    end
  end

  @doc """
  Set push token user

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"token": string, os: string}'

      'http://localhost:4000/api/v1/push_token'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"token": "string", "os": "android|ios"}' 'http://localhost:4000/api/v1/push_token'

  ## Return
      {"token":"string","refresh_token":"string"}
  """
  def push_token(conn, %{"token" => token, "os" => os}) do
    case is_nil(token) || String.length(token) == 0 || is_nil(os) || (os != "ios" && os != "android") do
      true ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorAuth.init_error_push_param_missing)
      false ->
        case update_auth_user(conn.assigns.user_id, %{push_token: token, os: os}) do
          {:ok, _user_authentication} ->
            send_resp(conn, :ok, "{}")
          {:error, changeset} ->
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", %{changeset: changeset})
        end
    end
  end

  @doc """
  Logout user

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      'http://localhost:4000/api/v1/logout'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' 'http://localhost:4000/api/v1/logout'

  ## Return
      {"token":"string","refresh_token":"string"}
  """
  def logout(conn, _param) do
    update_auth_user(conn.assigns.user_id, %{push_token: "", os: ""})
    send_resp(conn, :ok, "{}")
  end

  #
  # PUBLIC
  #

  @doc false
  def insert_user_token(user_id, user_params) do
    token = Token.sign_user(user_id)
    token_refrech = Token.sign_user_refrech(user_id)
    case Repo.one(UserAuthentication.query_auth_by_user_id(user_id)) do
      nil ->
        user_params = user_params
                        |> Map.put("token", token)
                        |> Map.put("refresh_token", token_refrech)
                        |> Map.put("user_id", user_id)
        changeset = UserAuthentication.changeset(%UserAuthentication{}, user_params)

        case Repo.insert(changeset) do
          {:ok, user_authentication} ->
            {:ok, %{user_authentication: user_authentication}}
          {:error, changeset} ->
            {:error, changeset}
        end
     user_auth ->
        update(%{"id" => user_auth.user_id, "user_authentication" => %{"token" => token, "refresh_token" => token_refrech}})
    end
  end

  @doc false
  def update_auth_user(user_id, %{last_seen: last_seen, is_online: is_online}) do
    update_auth(user_id, %{"last_seen" => last_seen, "is_online" => is_online})
  end

  @doc false
  def update_auth_user(user_id, %{push_token: push_token, os: os}) do
    update_auth(user_id, %{"push_token" => push_token, "os" => os})
  end

  @doc false
  def update_auth_user(user_id, %{is_online: is_online}) do
    update_auth(user_id, %{"is_online" => is_online})
  end

  @doc false
  def update_auth_user(user_id, %{room: room}) do
    update_auth(user_id, %{"room" => room})
  end

  @doc false
  def update_auth_user(user_id, %{token: token}) do
    update_auth(user_id, %{"token" => token})
  end

  #
  # PRIVATE
  #

  defp update_auth(user_id, param) do
    case Repo.one(UserAuthentication.query_auth_by_user_id(user_id)) do
      nil ->
        :not_found
      user_auth ->
        changeset = UserAuthentication.changeset(user_auth, param)
        case Repo.update(changeset) do
          {:ok, user_authentication} ->
            {:ok, %{user_authentication: user_authentication}}
          {:error, changeset} ->
            {:error, changeset}
        end
    end
  end

  defp update(%{"id" => user_id, "user_authentication" => user_authentication_params}) do
    user_authentication = Repo.one!(UserAuthentication.query_auth_by_user_id(user_id))
    changeset = UserAuthentication.changeset(user_authentication, user_authentication_params)

    case Repo.update(changeset) do
      {:ok, user_authentication} ->
        {:ok, %{user_authentication: user_authentication}}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  defp check_password(crypt_password, password) do
    case password  do
      nil -> Comeonin.Bcrypt.dummy_checkpw()
      _ -> Comeonin.Bcrypt.checkpw(password, crypt_password)
    end
  end

  defp validate_user(email) do
    query = User.query_user_by_email_and_pws(email)
    case Repo.one(query) do
      nil ->
        :ok
      {user_id, _, _} ->
        case ReportedWatcher.get(user_id, true, true) do
          {:error, %{time: time}} ->
            {:reported, %{time: time}}
          _ ->
            :ok
        end
    end
  end

end
