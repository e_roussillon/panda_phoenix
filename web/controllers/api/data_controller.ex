defmodule PandaPhoenix.DataController do
  @moduledoc """
  Upload image to server
  """

  use PandaPhoenix.Web, :controller
  alias PandaPhoenix.Data
  alias PandaPhoenix.ImageUploader
  alias PandaPhoenix.ErrorData

  plug :scrub_params, "data" when action in [:create, :update]

  #
  # API
  #

  @doc """
  Uplaod image to server

  ## Example
      curl -X POST \

      --header 'Authorization: Bearer token' \

      -F 'size=integer' -F 'data=@PATH_IMAGE' \

      'http://localhost:4000/api/v1/upload/image'

  ## Console
      curl -X POST --header 'Authorization: Bearer token' -F 'size=integer' -F 'data=@PATH_IMAGE' 'http://localhost:4000/api/v1/upload/image'

  ## Return

  """
  def upload_image(conn, %{"size" => image_size, "path" => plug_path}) do
    changeset = Data.changeset(%Data{})
    case Repo.insert(changeset) do
      {:ok, data} ->
        upload_image_s3(conn, %{id: data.id, size: image_size, plug: plug_path})
      {:error, changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  @doc false
  def upload_broadcast(:url, %{url: url}) do
    changeset = Data.changeset(%Data{}, %{"url_thumb" => url, "url_original" => url, "url_blur" => url, "type" => Data.data_type("website")})
    case Repo.insert(changeset) do
      {:ok, data} ->
        {:ok, data}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  #
  # Private
  #

  defp upload_image_s3(conn, %{id: id, size: image_size, plug: plug}) do
    case ImageUploader.store({plug, id}) do
      {:ok, image_name} ->
        list = ImageUploader.urls({image_name, id})
        Logger.debug(__ENV__, "info image_name", [%{"image_name" => image_name}, %{"list" => list}])

        thumb = Map.fetch!(list, :thumb)
        original = Map.fetch!(list, :original)
        blur = Map.fetch!(list, :blur)
        param = %{"url_thumb" => thumb, "url_original" => original, "url_blur" => blur, "type" => Data.data_type(plug.content_type), "size" => image_size}

        changeset = Data.changeset(Repo.get!(Data, id), param)
        case Repo.update(changeset) do
          {:ok, data} ->
            conn
              |> put_status(:created)
              |> render("show.json", data: data)
          {:error, changeset} ->
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
        end
      other ->
        Logger.debug(__ENV__, "info other", [%{"other" => other}])
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorData.init_error_upload_data)
    end
  end
end
