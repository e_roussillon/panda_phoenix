defmodule PandaPhoenix.BackupController do
  @moduledoc """
  Synchronization's user datas
  """

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.Backup
  alias PandaPhoenix.DateUtil
  alias PandaPhoenix.UserAuthentication


  #
  # API
  #

  @doc """
  Backup all information (need to be use after login)
      - user information
      - filters
      - conversations + message with/without data
      - friendship

  ## Example
      curl -X GET \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      'http://localhost:4000/api/v1/auth/backup'

  ## Console
      curl -X GET --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' 'http://localhost:4000/api/v1/auth/backup'

  ## Return
      {"user_id": 1, "backup": [string (sql)]}
  """
  def backup(conn, _) do
    user_id = conn.assigns.user_id

    backup = List.insert_at([], 0, Task.async(fn -> Backup.query_backup_current_user(user_id) end))
       |> List.insert_at(0, Task.async(fn -> Backup.query_backup_filters(user_id, nil) end))
       |> List.insert_at(0, Task.async(fn -> Backup.query_backup_chat_not_friend_users_light(user_id, nil) end))
       |> List.insert_at(0, Task.async(fn -> Backup.query_backup_friendship_users_light(user_id, nil) end))
       |> List.insert_at(0, Task.async(fn -> Backup.query_backup_private_rooms(user_id, nil) end))
       |> List.insert_at(0, Task.async(fn -> Backup.query_backup_private_rooms_user(user_id, nil) end))
       |> List.insert_at(0, Task.async(fn -> Backup.query_backup_private_message(user_id, nil) end))
       |> start_backup()

     conn
       |> put_status(:created)
       |> render(PandaPhoenix.BackupView, "show.json", %{backup: backup, user_id: user_id})
  end

  @doc """
  Backup all information missing (use for private chat)
      - conversations + message with/without data
      - friendship

  ## Example
      curl -X GET \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      'http://localhost:4000/api/v1/pending/backup'

  ## Console
      curl -X GET --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' 'http://localhost:4000/api/v1/pending/backup'

  ## Return
      {"user_id": 1, "backup": [string (sql)]}
  """
  def backup_pending_data(conn, _) do
    user_id = conn.assigns.user_id

     conn
       |> put_status(:created)
       |> render(PandaPhoenix.BackupView, "show.json", %{backup: backup_pending(user_id), user_id: user_id})
  end

  #
  # Public
  #

  @doc false
  def backup_pending(user_id) do

    case Repo.one(UserAuthentication.query_auth_by_user_id(user_id)) do
      nil ->
        []
      auth ->
        date = get_current_date(auth.last_seen)
        List.insert_at([], 0, Task.async(fn -> Backup.query_backup_private_message_pending(user_id) end))
           |> List.insert_at(0, Task.async(fn -> Backup.query_backup_private_rooms_user_pending(user_id) end))
           |> List.insert_at(0, Task.async(fn -> Backup.query_backup_private_rooms_pending(user_id) end))
           |> List.insert_at(0, Task.async(fn -> Backup.query_backup_chat_not_friend_users_light_pending(user_id) end))
           |> List.insert_at(0, Task.async(fn -> Backup.query_backup_friendship_users_light(user_id, date) end))
           |> start_backup()
    end
  end

  @doc false
  def backup_pending_light(user_id) do
    case Repo.one(UserAuthentication.query_auth_by_user_id(user_id)) do
      nil ->
        []
      auth ->
        date = get_current_date(auth.last_seen)
        List.insert_at([], 0, Task.async(fn -> Backup.query_backup_private_message_pending_only_sent(user_id) end))
           |> List.insert_at(0, Task.async(fn -> Backup.query_backup_friendship_users_light(user_id, date) end))
           |> start_backup()
    end
  end

  #
  # PRIVATE
  #

  defp start_backup(tasks) do
    tasks_with_results = Task.yield_many(tasks, 3000)

    results = Enum.map(tasks_with_results, fn {task, res} ->
      res || Task.shutdown(task, :brutal_kill)
    end)

    iterate_result(results, []) |> Enum.uniq()
  end

  defp iterate_result([], result) do
    result
  end

  defp iterate_result([{:ok, h}|t], result) do
    case h do
       {:ok, %{result: result_sql, table: table_name}} ->
         line =
           case result_sql do
             [] -> result
             item -> result ++ get_line_formated(item, table_name, [])
           end
         iterate_result(t, line)
       _ ->
         iterate_result(t, result)
    end
  end

  defp get_line_formated([], _table_name, result) do
    result
  end

  defp get_line_formated([h|t], table_name, result) do
    list = Map.keys(h)
    new_result = "INSERT OR REPLACE INTO " <> table_name <> " " <> get_column_formated(list, length(list), table_name, "") <> " VALUES " <> get_values_formated(list, length(list), h, "") <> ";"

    get_line_formated(t, table_name, result ++ [new_result])
  end

  defp get_column_formated([], _list_size, _table_name, result) do
    result <> ")"
  end

  defp get_column_formated([h|t], list_size, table_name, result) do
    new_result =
      case list_size == length(t) + 1 do
        true ->
          case h do
            :id ->  result <> "(" <> table_name <> "_" <> to_string(h)
            key -> result <> "(" <> to_string(key)
          end
        false ->
          case h do
            :id ->  result <> ", " <> table_name <> "_" <> to_string(h)
            key -> result <> ", " <> to_string(key)
          end
      end

    get_column_formated(t, list_size, table_name, new_result)
  end

  defp get_values_formated([], _list_size, _map, result) do
    "(" <> result <> ")"
  end

  defp get_values_formated([h|t], list_size, map, result) do
    new_result =
      case list_size == length(t) + 1 do
        true ->
          val = Map.get(map, h) |> get_value_formated()
        false ->
          val = Map.get(map, h) |> get_value_formated()
          result <> ", " <> val
      end

    get_values_formated(t, list_size, map, new_result)
  end

  defp get_value_formated(value) do
    case value do
      nil -> "NULL"
      true -> "1"
      false -> "0"
      int when is_integer(int) -> Integer.to_string(int)
      float when is_float(float) -> Float.to_string(float)
      string when is_binary(string) -> "'" <> String.replace(string, "'", "''") <> "'"
      {year, month, day} -> "'" <> DateUtil.format_date({{year, month, day}}) <> "'"
      {{year, month, day}, {h, m, s, _}} -> "'" <> DateUtil.format_date_time({{year, month, day}, {h, m, s}}) <> "'"
      other ->
        case is_date?(other) do
          true ->
            case Ecto.Date.dump(other) do
              {:ok, {year, month, day}} -> "'" <> DateUtil.format_date({{year, month, day}}) <> "'"
            end
          false ->
            case is_datetime?(other) do
              true ->
                case Ecto.DateTime.dump(other) do
                  {:ok, {{year, month, day}, {h, m, s, _}}} -> "'" <> DateUtil.format_date_time({{year, month, day}, {h, m, s}}) <> "'"
                end
            end
        end
    end
  end


  defp is_datetime?(%Ecto.DateTime{}) do
    true
  end

  defp is_datetime?(_) do
    false
  end

  defp is_date?(%Ecto.Date{}) do
    true
  end

  defp is_date?(_) do
    false
  end

  defp get_current_date(date) do
    case date do
      nil ->
        Ecto.DateTime.utc()
      date ->
        date
    end
  end

end
