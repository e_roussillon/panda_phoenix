defmodule PandaPhoenix.UserFilterController do
  @moduledoc """
  Provide method to create/update/delete filter
  """
  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.UserFilter

  plug :scrub_params, "user_filter" when action in [:create, :update]

  #
  # API
  #

  @doc """
  Create filter

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"user_filter": {"name": "string", "is_highlight": boolean}}' \

      'http://localhost:4000/api/v1/filters'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"user_filter": {"name": "string", "is_highlight": boolean}}' 'http://localhost:4000/api/v1/filters'

  ## Return
      { "user_id": integer, "name": "string", "is_highlight": boolean, "id": integer }
  """
  def create(conn, %{"user_filter" => user_filter_params}) do
    user_filter_params = Map.put(user_filter_params, "user_id", conn.assigns[:user_id])
    changeset = UserFilter.changeset(%UserFilter{}, user_filter_params)

    case changeset.valid? do
      true ->
        case check_to_active(%{"user_filter" => user_filter_params}) do
          {:does_exit, %{"id" => id}} ->
            conn
             |> update(%{"id" => id, "user_filter" => user_filter_params})
          :does_not_exit ->
            case Repo.insert(changeset) do
              {:ok, user_filter} ->
                conn
                  |> put_status(:created)
                  |> render("show.json", user_filter: user_filter)
              {:error, changeset} ->
                conn
                  |> put_status(:unprocessable_entity)
                  |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
            end
        end

      false ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  @doc """
  Update filter

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"id": integer, "user_filter": {"name": "string", "is_highlight": boolean, "active": boolean }}' \

      'http://localhost:4000/api/v1/filters'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"id": integer, "user_filter": {"name": "string", "is_highlight": boolean, "active": boolean }}' 'http://localhost:4000/api/v1/filters/ID'

  ## Return
      { "user_id": integer, "name": "string", "is_highlight": boolean, "id": integer }
  """
  def update(conn, %{"id" => id, "user_filter" => user_filter_params}) do
    user_filter = Repo.get!(UserFilter, id)

    user_filter_params =
      case Map.has_key?(user_filter_params, "active") do
        true ->  user_filter_params
        false -> Map.put(user_filter_params, "active", true)
      end

    user_filter_params =
      case Map.has_key?(user_filter_params, "user_id") do
        true ->  user_filter_params
        false -> Map.put(user_filter_params, "user_id", conn.assigns[:user_id])
      end

    changeset = UserFilter.changeset(user_filter, user_filter_params)

    case Repo.update(changeset) do
      {:ok, user_filter} ->
        conn
          |> put_status(:ok)
          |> render("show.json", user_filter: user_filter)
      {:error, changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_filter = Repo.get!(UserFilter, id)
    changeset = UserFilter.changeset(user_filter, %{"active" => false})

    case Repo.update(changeset) do
      {:ok, _} ->
        send_resp(conn, :ok, "{}")
        # conn
        #   |> put_status(:ok)
        #   |> render("show.json", user_filter: user_filter)
      {:error, changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  # def user_filters(conn, %{"id" => id}) do
  #   query = UserFilter.query_user_filters(id)
  #   case Repo.all(query) do
  #     [h|t] ->
  #       render(conn, "index.json", %{user_filters: [h|t]})
  #     [] ->
  #       render(conn, "index.json", %{user_filters: []})
  #     _ ->
  #       conn
  #         |> put_status(:unprocessable_entity)
  #         |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUser.init_error_user_not_found())
  #   end
  # end

  #
  # PUBLIC
  #

  # def index(conn, _params) do
  #   user_filters = Repo.all(UserFilter)
  #   render(conn, "index.json", user_filters: user_filters)
  # end
  #
  # def show(conn, %{"id" => id}) do
  #   user_filter = Repo.get!(UserFilter, id)
  #   render(conn, "show.json", user_filter: user_filter)
  # end

  #
  # PRIVATE
  #

  defp check_to_active(%{"user_filter" => user_filter_params}) do
    case UserFilter.query_filter_by_user_id_and_name(user_filter_params["user_id"], user_filter_params["name"]) do
       [{id, active}] when active == false ->
         {:does_exit, %{"id" => id}}
        _ ->
          :does_not_exit
    end
  end

end
