defmodule PandaPhoenix.PublicRoomLocalizationController do
  @moduledoc """
  Manager plublic room
  """
  use PandaPhoenix.Web, :controller

  # UTIL
  alias PandaPhoenix.LocalisationUtil
  alias PandaPhoenix.ErrorLocalization

  # CONTROLLER
  alias PandaPhoenix.PublicRoomController

  # MODEL
  alias PandaPhoenix.GenericRoom
  alias PandaPhoenix.PublicRoomLocalization

  plug :scrub_params, "public_room_localization" when action in [:create, :update]

  #
  # API
  #

  @doc """
  Get all room from you GEO

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"latitude": "string", "longitude": "string"}' \

      'http://localhost:4000/api/v1/find_room'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"latitude": "string", "longitude": "string"}' 'http://localhost:4000/api/v1/find_room'

  ## Return

  """
  def find_room(conn, generic_room_params) do
    changeset = GenericRoom.changeset(%GenericRoom{}, generic_room_params)

    case changeset.valid? do
      true ->
        case PublicRoomLocalization.query_public_room_localized_by_lat_and_long(generic_room_params["latitude"], generic_room_params["longitude"]) do
          {:ok, %{:public_room_id => id}} when id == -1 ->
            conn
              |> put_status(:not_found)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorLocalization.init_error_country_not_mapped())
          {:ok, %{:public_room_id => id}} ->
            conn
              |> PublicRoomController.show(%{"id" => id})
          {:ok, _} ->
            LocalisationUtil.init_path(conn, generic_room_params["latitude"], generic_room_params["longitude"])
          _ ->
            conn
              |> put_status(:not_found)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorLocalization.init_error_country_not_mapped())
        end
      false ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  #
  # PUBLIC
  #

  @doc false
  def create(conn, %{"public_room_localization" => public_room_localization_params, "api_result" => _, "result" => result}) do
    changeset = PublicRoomLocalization.changeset(%PublicRoomLocalization{}, public_room_localization_params)

    case Repo.insert(changeset) do
      {:ok, public_room_localization} ->
        if result == "" do
          conn
            |> put_status(:not_found)
            |> render(PandaPhoenix.ChangesetView, "error.json", ErrorLocalization.init_error_country_not_mapped())
        else
          conn
            |> PublicRoomController.find_or_create_path_rooms(public_room_localization.id, result)
        end
      {:error, changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  # def error_room(conn, error) do
  #   conn
  #   |> put_status(:unprocessable_entity)
  #   |> render(PandaPhoenix.ChangesetView, "error.json", error: error)
  # end

  @doc false
  def update(conn, %{"id" => id, "public_room_localization" => public_room_localization_params}) do
    public_room_localization = Repo.get!(PublicRoomLocalization, id)
    changeset = PublicRoomLocalization.changeset(public_room_localization, public_room_localization_params)

    case Repo.update(changeset) do
      {:ok, public_room_localization} ->
        conn
          |> PublicRoomController.show(%{"id" => public_room_localization.public_room_id})
      {:error, changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  #
  # PRIVATE
  #

  # defp index(conn, _params) do
  #   public_room_localization = Repo.all(PublicRoomLocalization)
  #   render(conn, "index.json", public_room_localization: public_room_localization)
  # end
  #
  # defp show(conn, %{"id" => id}) do
  #   public_room_localization = Repo.get!(PublicRoomLocalization, id)
  #   render(conn, "show.json", public_room_localization: public_room_localization)
  # end
  #
  # defp delete(conn, %{"id" => id}) do
  #   public_room_localization = Repo.get!(PublicRoomLocalization, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(public_room_localization)
  #
  #   send_resp(conn, :ok, "{}")
  # end

end
