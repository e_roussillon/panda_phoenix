defmodule PandaPhoenix.UserController do
  use PandaPhoenix.Web, :controller
  @moduledoc """
  Provide method to register or update a user with `create` and `update`
  """

  alias PandaPhoenix.User
  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.UserPasswordController
  alias PandaPhoenix.ErrorUser
  alias PandaPhoenix.Email
  alias PandaPhoenix.User
  plug :scrub_params, "user" when action in [:create, :update]

  @doc """
  Register a user

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'password: 123456' \

      -d '{"user": {"pseudo": "string","email": "string","birthday": "string"}}' \

      'http://localhost:4000/api/v1/register'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'password: 123456' -d '{"user": {"pseudo": "string","email": "string","birthday": "string"}}' 'http://localhost:4000/api/v1/register'

  ## Return
      {"token":"string","refresh_token":"string"}
  """
  def create(conn, %{"user" => user_params}) do
    map = Enum.into(conn.req_headers, %{})
    changeset = User.changeset(%User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, user} ->
        user_params = Map.new()
                        |> Map.put("password", map["password"])
                        |> Map.put("user_id", user.id)

        case UserPasswordController.insert_user_password(user, user_params) do
          {:ok, %{user_authentication: user_authentication}} ->
            Email.welcome_email(user)
            conn
              |> put_status(:created)
              |> render(PandaPhoenix.UserAuthenticationView, "show.json", %{user_authentication: user_authentication})
          {:error, changeset} ->
            Logger.warn(__ENV__, "will delete user", [%{changeset: changeset}])
            Repo.delete!(user)
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
        end
      {:error, changeset} ->
        Logger.warn(__ENV__, "error insert", [%{changeset: changeset}])
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  @doc """
  Register a user

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"id": integer, "user": {"pseudo": "string", "email": "string", "birthday": "string", "gender": integer, "data_id": integer}}' \

      'http://localhost:4000/api/v1/register'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"id": integer, "user": {"pseudo": "string", "email": "string", "birthday": "string", "gender": integer, "data_id": integer}}' 'http://localhost:4000/api/v1/register'

  ## Return
      {"id": integer, "pseudo": string, "birthday": date, "gender": integer, "description": string, "url_thumb": url, "url_original": url, "url_blur": url, "friendship_status": integer, "friendship_last_action": integer, "is_online": bool, "last_seen": datetime, "reported": bool, "updated_at": datetime}
  """
  def update(conn, %{"id" => id, "user" => user_params}) do

    case update(%{"id" => id, "user" => user_params}) do
      {:ok, user} ->
        query = User.query_backup_current_user(user.id)
        case Repo.all(query) do
          [user] ->
            render(conn, "show.json", user: user)
          _ ->
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUser.init_error_user_not_found())
        end
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  @doc """
  Reset password a user

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      -d '{"email": string}' \

      'http://localhost:4000/api/v1/reset_password'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{"email": string}' 'http://localhost:4000/api/v1/reset_password'

  ## Return
      {}
  """
  def reset_password(conn, %{"email" => email}) do
    case PandaPhoenix.EmailValidator.validate_email(email) do
      :ok ->
        case Repo.get_by(User, email: email) do
          nil ->
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUser.init_error_user_email_not_valid())
          user ->
              Email.reset_password_email(user)
              send_resp(conn, :ok, "{}")
        end
      {:error, error} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUser.init_error_user_email_not_valid())
    end
  end

  #
  # PUBLIC
  #

  @doc false
  def show(conn, %{"id" => id}) do
    {to_user_id, _ } = Integer.parse(id)
    query = UserFriendship.query_user_friendship_full_desc(conn.assigns[:user_id], to_user_id)
    case Repo.all(query) do
      [user] ->
        render(conn, "show.json", user: user)
      _ ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUser.init_error_user_not_found())
    end
  end

  @doc false
  def update(%{"id" => id, "user" => user_params}) do
    user = Repo.get!(User, id)
    changeset = User.changeset(user, user_params)

    case Repo.update(changeset) do
      {:ok, user} ->
        {:ok, user}
      {:error, changeset} ->
        {:error, changeset}
    end
  end
end
