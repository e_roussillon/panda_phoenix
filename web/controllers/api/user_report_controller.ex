defmodule PandaPhoenix.UserReportController do
  @moduledoc """
  Report user
  """
  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.UserReport
  alias PandaPhoenix.Watcher.Reported.ReportedWatcher

  plug :scrub_params, "user_report" when action in [:create]

  @doc """
  Find friendship by pseudo

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"user_report": {"user_id_reported": integer}}' \

      'http://localhost:4000/api/v1/report'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"user_report": {"user_id_reported": integer}}' 'http://localhost:4000/api/v1/report'

  ## Return
    No content
  """
  def create(conn, %{"user_report" => user_report}) do
    user_report = Map.put(user_report, "user_id_reportor", conn.assigns[:user_id])
    changeset = UserReport.changeset(%UserReport{}, user_report)
    case Repo.insert(changeset) do
      {:ok, user_report} ->
        ReportedWatcher.add(user_report.user_id_reported)
        conn
          |> send_resp(:ok, "{}")
      {:error, changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

end
