defmodule PandaPhoenix.UserFriendshipController do
  @moduledoc """
  Provide method to manager friendship with user
  """

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.User
  alias PandaPhoenix.ErrorUserFriendship
  alias PandaPhoenix.ErrorUser
  alias PandaPhoenix.PrivateChannel
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.PushNotificationController
  alias PandaPhoenix.UserView

  plug :scrub_params, "user_friendship" when action in [:create, :update]

  #
  # API
  #
  @doc """
  Add/Update friendship

      status can be:
          - friendship_not_friend, do: 0
          - friendship_pending, do: 1
          - friendship_cancel_request, do: 2
          - friendship_refused_before_be_friend, do: 3
          - friendship_blocked_before_be_friend, do: 4
          - friendship_friend, do: 5
          - friendship_refused_after_be_friend, do: 6
          - friendship_blocked_after_be_friend, do: 7
          - friendship_unblocked, do: 8

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"to_user_id": integer, "status": integer}' \

      'http://localhost:4000/api/v1/update_friendship'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"to_user_id": integer, "status": integer}' 'http://localhost:4000/api/v1/update_friendship'

  ## Return
      {"url_thumb": url,"url_original": url,"url_blur": url, "updated_at": timestamps, "reported": boolean, "pseudo": "string", "last_seen": timestamps, "is_online": boolean, "id": integer, "gender": integer, "friendship_status": integer, "friendship_last_action": integer, "description": string, "birthday": string (2014-10-23)}%
  """
  def update_friendship(conn, user_friendships) do
    user_friendships = Map.put(user_friendships, "user_id", conn.assigns[:user_id])
    changeset = UserFriendship.changeset_start(%UserFriendship{}, user_friendships)

    case changeset.valid? do
      true ->
        check_state_friendship(conn, user_friendships)
      false ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  @doc """
  Find friendship by pseudo

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"pseudo": "string", "page": integer}' \

      'http://localhost:4000/api/v1/find_friendships'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"pseudo": "string", "page": integer}' 'http://localhost:4000/api/v1/find_friendships'

  ## Return
      { "total_pages": integer, "total_entries": 1, "page_size": 30, "page_number": 1, "entries": [{ "url_thumb": url, "url_original": url, "url_blur": url, "updated_at": timestamps, "pseudo": "string", "last_seen": timestamps, "is_online": boolean, "id": integer, "friendship_status": integer, "friendship_last_action": integer }]}
  """
  def find(conn, %{"pseudo" => pseudo, "page" => page}) do
    if page > 0 do
      paginate = UserFriendship.query_user_friendship_by_pseudo(conn.assigns[:user_id], pseudo, page)
      conn
        |> put_status(:ok)
        |> render(UserView, "index.json", %{paginate: paginate})
    else
      conn
        |> put_status(:unprocessable_entity)
        |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_find_page_param())
    end
  end

  #
  # Private
  #

  defp check_state_friendship(conn, user_friendships) do
    user_id = conn.assigns[:user_id]
    to_user_id = user_friendships["to_user_id"]
    case Repo.one(User.query_user(to_user_id)) do
      nil ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUser.init_error_user_not_found())
      _ ->
        query = UserFriendship.query_user_friendship(user_id, to_user_id)
        case Repo.all(query) do
            [] ->
              new_status = Map.get(user_friendships, "status")
              if new_status == UserFriendship.friendship_pending || new_status == UserFriendship.friendship_blocked_before_be_friend do
                create(conn, %{"user_friendship" => %{"user_id" => user_id, "to_user_id" => to_user_id, "status" => new_status, "last_action" => user_id}})
              else
                conn
                  |> put_status(:unprocessable_entity)
                  |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible())
              end
            [result] ->
              update_state_friendship(conn, result, user_friendships, user_id)
            _ ->
              conn
                |> put_status(:unprocessable_entity)
                |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible())
        end
    end
  end

  defp create(conn, %{"user_friendship" => user_friendship_params}) do
    changeset = UserFriendship.changeset(%UserFriendship{}, user_friendship_params)

    case Repo.insert(changeset) do
      {:ok, user_friendship} ->
        send_boradcast(%{current_user_id: conn.assigns[:user_id], user_friendship: user_friendship})
        send_back_friendship(conn, %{current_user_id: conn.assigns[:user_id], user_friendship: user_friendship})

      {:error, changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  defp send_back_friendship(conn, %{current_user_id: current_user_id, user_friendship: user_friendship}) do
    user_id = get_user_id_from_friendship(current_user_id, user_friendship)

    case Repo.one(UserFriendship.query_user_friendship_full_desc(current_user_id, user_id)) do
      nil ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible())
      user ->
        conn
          |> put_status(:created)
          |> render(UserView, "show.json", %{user: user})
    end
  end

  defp send_boradcast(%{current_user_id: current_user_id, user_friendship: user_friendship}) do
    user_id = get_user_id_from_friendship(current_user_id, user_friendship)

    case Repo.one(UserFriendship.query_user_friendship_full_desc(user_id, current_user_id)) do
      nil ->
        Logger.warn(__ENV__, "Can not found friendship to send boardcast", [%{current_user_id: current_user_id}, %{user_friendship: user_friendship}])
      user ->
        response = UserView.render("show.json", %{user: user})
        case Repo.one(UserAuthentication.query_auth_by_user_id(user_id)) do
          nil -> Logger.error(__ENV__, "Cannot found user")
          auth ->
            case auth.is_online do
              true ->
                PandaPhoenix.Endpoint.broadcast_from(self(), "private_channel:#{user_id}", PrivateChannel.channel_name(:friendship), response)
                Logger.warn(__ENV__, "user online - send broadcast friendship", [%{user_id: user_id}, %{response: response}])
              false ->
                if user.friendship_status == UserFriendship.friendship_pending do
                  Logger.warn(__ENV__, "user offline and status `friendship_pending` - send push notif friendhship ", [%{user: user}, %{response: response}])
                  PushNotificationController.push(:friendship, %{user_id: auth.user_id, os: auth.os, token: auth.push_token, playload: response})
                else
                  Logger.warn(__ENV__, "DO NOT SEND NOTIF - user offline and status different `friendship_pending` - user will get update next connection", [%{user: user}, %{response: response}])
                end
            end
        end
        user
    end
  end

  defp update_friendship(conn, old_user_friendship, _, user_id, new_status) do
    case update(%{"user_friendship" => old_user_friendship, "user_friendship_params" => %{"status" => new_status, "last_action" => user_id}}) do
      {:ok, user_friendship} ->
        send_boradcast(%{current_user_id: conn.assigns[:user_id], user_friendship: user_friendship})
        send_back_friendship(conn, %{current_user_id: conn.assigns[:user_id], user_friendship: user_friendship})

      {:error, changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  defp update(%{"user_friendship" => user_friendship, "user_friendship_params" => user_friendship_params}) do
    changeset = UserFriendship.changeset(user_friendship, user_friendship_params)

    case Repo.update(changeset) do
      {:ok, user_friendship} ->
        {:ok, user_friendship}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  defp update_state_friendship(conn, user_friendships, user_friendships_param, user_id) do
    friendship_not_friend = UserFriendship.friendship_not_friend
    friendship_pending = UserFriendship.friendship_pending
    friendship_cancel_request = UserFriendship.friendship_cancel_request
    friendship_refused_before_be_friend = UserFriendship.friendship_refused_before_be_friend
    friendship_blocked_before_be_friend = UserFriendship.friendship_blocked_before_be_friend
    friendship_friend = UserFriendship.friendship_friend
    friendship_refused_after_be_friend = UserFriendship.friendship_refused_after_be_friend
    friendship_blocked_after_be_friend = UserFriendship.friendship_blocked_after_be_friend
    friendship_unblocked = UserFriendship.friendship_unblocked

    last_action = user_friendships.last_action
    new_status = Map.get(user_friendships_param, "status")

    case new_status do
      x when x == friendship_not_friend ->
        Logger.warn(__ENV__, "Try to set the friendship as 'UserFriendship.friendship_not_friend'", [%{"user_friendships_param" => user_friendships_param}, %{"user_friendships" => user_friendships}])
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible)

      x when x == friendship_pending ->
        case user_friendships.status do
          y when y == friendship_not_friend ->
            Logger.debug("------> user_friendships.status == friendship_not_friend | #{friendship_not_friend}")
            update_friendship(conn, user_friendships, user_friendships_param, user_id, new_status)
          y when y == friendship_cancel_request ->
            Logger.debug("------> user_friendships.status == friendship_cancel_request | #{friendship_cancel_request}")
            update_friendship(conn, user_friendships, user_friendships_param, user_id, new_status)
          y when y == friendship_refused_before_be_friend ->
            Logger.debug("------> user_friendships.status == friendship_refused_before_be_friend | #{friendship_refused_before_be_friend}")
            update_friendship(conn, user_friendships, user_friendships_param, user_id, new_status)
          _ ->
            Logger.warn(__ENV__, "Try to set the friendship as 'UserFriendship.friendship_pending'", [%{"user_friendships_param" => user_friendships_param}, %{"user_friendships" => user_friendships}])
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible)
        end

      x when x == friendship_cancel_request ->
        Logger.debug("------> user_friendships.status == friendship_cancel_request | #{friendship_cancel_request}")
        case user_friendships.status do
          y when y == friendship_pending ->
            update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_cancel_request)
          y when y == friendship_cancel_request ->
            update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_not_friend)
          y when y == friendship_refused_before_be_friend ->
            update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_not_friend)
          _ ->
            Logger.warn(__ENV__, "Try to set the friendship as 'UserFriendship.friendship_cancel_request'", [%{"user_friendships_param" => user_friendships_param}, %{"user_friendships" => user_friendships}])
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible)
        end

      x when x == friendship_refused_before_be_friend ->
        Logger.debug("------> user_friendships.status == friendship_refused_before_be_friend | #{friendship_refused_before_be_friend}")
        case user_friendships.status do
          y when y == friendship_pending ->
            update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_not_friend)
          y when y == friendship_refused_before_be_friend ->
            update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_not_friend)
          _ ->
            Logger.warn(__ENV__, "Try to set the friendship as 'UserFriendship.friendship_friend'", [%{"user_friendships_param" => user_friendships_param}, %{"user_friendships" => user_friendships}])
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible)
        end

      x when x == friendship_blocked_before_be_friend ->
        Logger.debug("------> user_friendships.status == friendship_blocked_before_be_friend | #{friendship_blocked_before_be_friend}")
        update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_blocked_before_be_friend)

      x when x == friendship_friend ->
        Logger.debug("------> user_friendships.status == friendship_friend | #{friendship_friend}")
        case user_friendships.status do
          y when y == friendship_pending ->
            update_friendship(conn, user_friendships, user_friendships_param, user_id, new_status)
          _ ->
            Logger.warn(__ENV__, "Try to set the friendship as 'UserFriendship.friendship_friend'", [%{"user_friendships_param" => user_friendships_param}, %{"user_friendships" => user_friendships}])
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible)
        end

      x when x == friendship_refused_after_be_friend ->
        Logger.debug("------> user_friendships.status == friendship_refused_after_be_friend | #{friendship_refused_after_be_friend}")
        update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_not_friend)

      x when x == friendship_blocked_after_be_friend ->
        Logger.debug("------> user_friendships.status == friendship_blocked_after_be_friend | #{friendship_blocked_after_be_friend}")
        update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_blocked_after_be_friend)

      x when x == friendship_unblocked ->
        Logger.debug("------> user_friendships.status == friendship_unblocked | #{friendship_unblocked}")
        case user_friendships.status do
          y when y == friendship_blocked_before_be_friend and last_action == user_id ->
            update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_not_friend)
          y when y == friendship_blocked_after_be_friend and last_action == user_id ->
            update_friendship(conn, user_friendships, user_friendships_param, user_id, UserFriendship.friendship_friend)
          _ ->
            Logger.warn(__ENV__, "Try to set the friendship as 'UserFriendship.friendship_unblocked'", [%{"user_friendships_param" => user_friendships_param}, %{"user_friendships" => user_friendships}])
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible)
        end

      other ->
        Logger.warn(__ENV__, "Try to set the friendship as '#{other}' ", [%{"user_friendships_param" => user_friendships_param}, %{"user_friendships" => user_friendships}])
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorUserFriendship.init_error_action_imposible)
    end
  end

 defp get_user_id_from_friendship(current_user_id, user_friendship) do
   case user_friendship.user_id == current_user_id do
     true ->
       user_friendship.to_user_id
     false ->
       user_friendship.user_id
   end
 end

end
