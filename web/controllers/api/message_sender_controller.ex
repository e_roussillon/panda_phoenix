defmodule PandaPhoenix.MessageSenderController do
  @moduledoc """
  Send a confirmation or broadcast message
  """
  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.User
  alias PandaPhoenix.ErrorMessageSender
  alias PandaPhoenix.PublicRoom
  alias PandaPhoenix.PublicMessageController
  alias PandaPhoenix.DateUtil
  alias PandaPhoenix.PublicMessageView
  alias PandaPhoenix.PrivateMessageController
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateChannel
  alias PandaPhoenix.DataController

  #
  # API
  #
  @doc """
  Confimration list of messages for a private conversation

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"messages": [{"id" => id, "status" => status }]}' \

      'http://localhost:4000/api/v1/confirmation/messages'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"messages": [{"id": integer, "status": integer }]}' 'http://localhost:4000/api/v1/confirmation/messages'

  ## Return

  """
  def private_messages_confirmation(conn, %{"messages" => messages}) do
    private_messages_confirmation(messages, Map.new(), conn.assigns.user_id)
    send_resp(conn, :ok, "{}")
  end

  @doc """
  Broadcast message to public rooms

  ## Example
      curl -X POST \

      --header 'Content-Type: application/json' \

      --header 'Accept: application/json' \

      --header 'Authorization: Bearer token' \

      -d '{"room_id": integer, "message": "string", "url": "string", "is_general_room": boolean}' \

      'http://localhost:4000/api/v1/broadcast/message'

  ## Console
      curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer token' -d '{"room_id": integer, "message": "string", "url": "string", "is_general_room": boolean}' 'http://localhost:4000/api/v1/broadcast/message'

  ## Return

  """
  def public_message(conn, %{"room_id" => room_id,
                             "message" => msg,
                             "url" => url,
                             "is_general_room" => is_general_room}) do
    user_id = conn.assigns.user_id

    with [user]                <- Repo.all(User.query_user_by_id(user_id)),
         :broadcast_ok         <- check_user_permission(user),
         {:ok, room}           <- check_room(room_id),
         {:ok, data}           <- DataController.upload_broadcast(:url, %{url: url}),
         {:ok, public_message} <- PublicMessageController.create(%{"public_message" => %{"from_user_id" => user_id,
                                                                     "is_broadcast" => true,
                                                                       "is_general" => is_general_room,
                                                                              "msg" => msg,
                                                                          "data_id" => data.id,
                                                                   "public_room_id" => room.id}}) do

      index = "#{user.pseudo}_#{DateUtil.timestamp}"
      msg = PublicMessageView.render("show.json", %{public_message: public_message, data: data, user_name: user.pseudo, index: index})
      PandaPhoenix.Endpoint.broadcast_from(self(), "broadcast_channel:#{room.id}", "news:msg", msg)
      send_resp(conn, :ok, "{}")

    else
      []                   ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(PandaPhoenix.ChangesetView, "error.json", ErrorMessageSender.init_error_user_not_found)
      :broadcast_error     ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorMessageSender.init_error_user_not_grant)
      :error               ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorMessageSender.init_error_room_not_found)
      {:ok, []}            ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorMessageSender.init_error_send_message)
      {:error, _changeset} ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorMessageSender.init_error_param_invalid)
    end
  end

  #
  # Public
  #

  @doc false
  def private_messages_confirmation([], result, _user_id) do
    Map.keys(result)
      |> Enum.each( fn(user_id) ->
        Logger.info(__ENV__, "broadcast confirmation message for user_id #{user_id}", [%{channel: "private_channel:#{user_id}"},%{topic: PrivateChannel.channel_name(:confirmation)},%{messages: Map.get(result, user_id, [])}])
        PandaPhoenix.Endpoint.broadcast_from(self(),
                                             "private_channel:#{user_id}",
                                             PrivateChannel.channel_name(:confirmation),
                                             %{messages: Map.get(result, user_id, [])})
      end)
  end

  @doc false
  def private_messages_confirmation([h|t], result, user_id) do
    new_result =
      case h do
        %{"id" => id, "status" => status } ->
          case PrivateMessageController.update(%{"id" => id, "private_message" => %{"status" => status}}) do
            {:error, _error} ->
              result

            private_message ->
              case Repo.one(PrivateRoomUser.query_other_user_by_message_id_and_user_id(user_id, private_message.id)) do
                nil ->
                  result

                user ->
                  list = Map.get(result, user.user_id, [])
                  Map.put(result, user.user_id, list ++ [%{id: private_message.id, status: private_message.status}])
              end
          end
        _ ->
          result
      end
    private_messages_confirmation(t, new_result, user_id)
  end

  #
  # Private
  #

  defp check_user_permission(user) do
      if user.can_broadcast do
        :broadcast_ok
      else
        :broadcast_error
      end
  end

  defp check_room(room_id) do
    case Repo.get(PublicRoom, room_id) do
      nil ->
        :error
      room ->
        {:ok, room}
    end
  end

end
