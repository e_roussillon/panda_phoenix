defmodule PandaPhoenix.HomeController do
  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.Token
  alias PandaPhoenix.UserController
  alias PandaPhoenix.ResetPassword
  alias PandaPhoenix.UserPassword
  alias PandaPhoenix.Token
  alias PandaPhoenix.UserPasswordController
  alias PandaPhoenix.ExtraInfos

  def index(conn, _params) do
    conn
      |> render("index.html")
  end

  def active_user(conn, %{"token" => token}) do
    case Token.verify_active_user(token) do
      {:authenticated, user_id} ->
        case UserController.update(%{"id" => user_id, "user" => %{"active" => true}}) do
          {:ok, user} ->
            render(conn, "reponse.html", success: true, title: "Congrats!", message: "Compte activited")
          {:error, changeset} ->
            render(conn, "reponse.html", success: false, title: "Whoops!", message: "Activation fail")
        end
      :invalid ->
        render(conn, "reponse.html", success: false, title: "Whoops!", message: "Activation fail")
    end
  end

  def reset(conn, %{"token" => token}) do
    case check_token(conn, token) do
      :invalid ->
        render(conn, "reponse.html", success: false, title: "Whoops!", message: "token invalid")
      {:ok, _} ->
        reset_password = %ResetPassword{new_password: "", confirm_password: ""}
        changeset = ResetPassword.init_changeset(reset_password)
        render(conn, "reset.html", action: "/reset/#{token}/password", reset_password: reset_password, changeset: changeset)
    end
  end

  def reset_password(conn, %{"token" => token, "reset_password" => reset_password}) do
    case check_token(conn, token) do
      :invalid ->
        render(conn, "reponse.html", success: false, title: "Whoops!", message: "token invalid")
      {:ok, user_id} ->
        changeset = ResetPassword.changeset(%ResetPassword{}, reset_password)
        case changeset.valid? do
          true ->
            case UserPasswordController.update(%{"user_id" => user_id, "user_password" => %{"password" => reset_password["new_password"], "reset_password_token" => ""}}) do
              :ok ->
                render(conn, "reponse.html", success: true, title: "Congrats!", message: "Password set")
              {:error, changeset} ->
                render(conn, "reponse.html", success: false, title: "Whoops!", message: "token invalid")
            end

          false ->
            render(conn, "reset.html", action: "/reset/#{token}/password", reset_password: reset_password, changeset: changeset)
        end
    end
  end

  def terms(conn, %{"language" => lang}) do
    query = ExtraInfos.query_extra_info_terms(lang)
    case Repo.one(query) do
      nil ->
        conn |> Phoenix.Controller.redirect(to: "/en/terms")
      term ->
        render(conn, "term_and_condition.html", content: term.content)
    end

  end


  #
  #  Private
  #

  defp check_token(conn, token) do
    case Token.verify_reset_password(token) do
      {:authenticated, user_id} ->
        case Repo.one(UserPassword.query_user_reset_password(user_id, token)) do
          nil ->
            :invalid
          _ ->
            {:ok, user_id}
        end
      :invalid ->
        :invalid
    end
  end

end
