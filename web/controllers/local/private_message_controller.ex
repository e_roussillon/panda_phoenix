defmodule PandaPhoenix.PrivateMessageController do
  @moduledoc false

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.PrivateMessage
  alias PandaPhoenix.ErrorPrivateMessage
  alias PandaPhoenix.PrivateRoomUserController
  alias PandaPhoenix.PrivateRoomController

  plug :scrub_params, "private_message" when action in [:create, :update]

  def create(%{"private_message" => private_message_params, "private_room_user" => private_room_users, "private_room" => private_room}) do
    case create(%{"private_message" => private_message_params}) do
      {:ok, private_message} ->
        {:ok, private_message}

      {:error, changeset} ->
        PrivateRoomUserController.delete(private_room_users)
        PrivateRoomController.delete(%{"private_room" => private_room})
        {:error, changeset}
    end
  end

  def create(%{"private_message" => private_message_params}) do
    changeset = PrivateMessage.changeset(%PrivateMessage{}, private_message_params)

    case Repo.insert(changeset) do
      {:ok, private_message} ->
        {:ok, Repo.one!(PrivateMessage.query_message_with_data(private_message.id))}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def update(%{"id" => id, "private_message" => private_message_params}) do
    case Repo.get(PrivateMessage, id) do
      nil ->
        {:error, ErrorPrivateMessage.init_error_message_not_found}

      private_message ->
        changeset = PrivateMessage.changeset(private_message, private_message_params)
        Repo.update!(changeset)
    end
  end

  # def index(conn, _params) do
  #   private_messages = Repo.all(PrivateMessage)
  #   render(conn, "index.json", private_messages: private_messages)
  # end



  # def show(conn, %{"id" => id}) do
  #   private_message = Repo.get!(PrivateMessage, id)
  #   render(conn, "show.json", private_message: private_message)
  # end

  # def delete(conn, %{"id" => id}) do
  #   private_message = Repo.get!(PrivateMessage, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(private_message)
  #
  #   send_resp(conn, :ok, "{}")
  # end

  # def delete(%{"private_message" => private_message}) do
  #   Repo.delete(private_message)
  # end
end
