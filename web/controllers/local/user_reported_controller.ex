defmodule PandaPhoenix.UserReportedController do
  @moduledoc false

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.UserReported

  plug :scrub_params, "user_reported" when action in [:create, :update]

  # def user_reported_remove(conn, %{"user_id" => user_id}) do
  #   ReportedWatcher.remove(user_id)
  #   conn
  #   |> put_status(:created)
  #   |> render("show.json", user_reported: nil)
  # end
  #

  def show(user_id) do
    query = UserReported.query_reported_by_uid(user_id)
    case Repo.one(query) do
      nil ->
        :error
      user_reported ->
        {:ok, user_reported}
    end
  end

  def user_reported(user_id) do
    query = UserReported.query_reported_by_uid(user_id)
    case Repo.one(query) do
      nil ->
        create_report(user_id)
      user_reported ->
        {:ok, user_reported}
    end
  end

  def user_reported(:reset, user_id) do
    query = UserReported.query_reported_by_uid(user_id)
    case Repo.one(query) do
      nil ->
        :ok
      user_reported ->
        update_report(user_reported, %{count: user_reported.count, is_used: true, is_active: false})
    end
  end

  def update_report(user_reported, %{count: count, is_used: is_used, is_active: is_active, notified: notified}) do
    changeset = UserReported.changeset(user_reported, %{"count" => count, "is_used" => is_used, "is_active" => is_active, "notified" => notified})
    case Repo.update(changeset) do
      {:ok, user_reported} ->
        {:ok, user_reported}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def update_report(user_reported, %{count: count, is_used: is_used, is_active: is_active}) do
    changeset = UserReported.changeset(user_reported, %{"count" => count, "is_used" => is_used, "is_active" => is_active})
    case Repo.update(changeset) do
      {:ok, user_reported} ->
        {:ok, user_reported}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def update_report(user_reported, %{count: count, is_used: is_used, notified: notified}) do
    changeset = UserReported.changeset(user_reported, %{"count" => count, "is_used" => is_used, "notified" => notified})
    case Repo.update(changeset) do
      {:ok, user_reported} ->
        {:ok, user_reported}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def update_report(user_reported, %{count: count, is_used: is_used}) do
    changeset = UserReported.changeset(user_reported, %{"count" => count, "is_used" => is_used})
    case Repo.update(changeset) do
      {:ok, user_reported} ->
        {:ok, user_reported}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def update_report(user_reported, %{count: count, is_active: is_active}) do
    changeset = UserReported.changeset(user_reported, %{"count" => count, "is_active" => is_active})
    case Repo.update(changeset) do
      {:ok, user_reported} ->
        {:ok, user_reported}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  defp create_report(user_id) do
    changeset = UserReported.changeset(%UserReported{}, %{"user_id" => user_id, "count" => 0})
    case Repo.insert(changeset) do
      {:ok, user_reported} ->
        {:ok, user_reported}
      {:error, changeset} ->
        {:error, changeset}
    end
  end



  # def update_report(user_reported, count) do
  #   case update_report(user_reported, %{count: count, is_used: false, is_active: false, notified: 0}) do
  #     {:ok, user_reported} ->
  #       {:ok, user_reported}
  #       # conn
  #       #  |> render("show.json", user_reported: user_reported)
  #     {:error, changeset} ->
  #       {:error, changeset}
  #       # conn
  #       #   |> put_status(:unprocessable_entity)
  #       #   |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end

  #
  # def delete(conn, %{"id" => id}) do
  #   user_reported = Repo.get!(UserReported, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(user_reported)
  #
  #   send_resp(conn, :ok, "{}")
  # end
end
