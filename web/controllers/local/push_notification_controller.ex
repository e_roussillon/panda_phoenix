defmodule PandaPhoenix.PushNotificationController do
  @moduledoc false
  use PandaPhoenix.Web, :controller

  import Pigeon.APNS.Notification
  alias PandaPhoenix.BackupController

  def notif_backup, do: 0
  def notif_message, do: 1
  def notif_friendship, do: 2

  def push(:msg, %{user_id: user_id, os: os, token: token, msg: msg, playload: playload}) do
    case is_nil(token) || String.length(token) == 0 || is_nil(os) || String.length(os) == 0 || is_nil(user_id)|| user_id < 0 do
      true ->
        Logger.error(__ENV__, "ERROR - Push Notif Message - one or more param are null or empty", [%{token: token}, %{os: os}, %{msg: msg}, %{user_id: user_id}])
        :error
      false ->
        os_type = os |> String.trim |> String.to_atom
        push_message(%{user_id: user_id, os: os_type, token: String.trim(token), msg: msg, playload: playload})
    end
  end

  def push(:friendship, %{user_id: user_id, os: os, token: token, playload: playload}) do
    case is_nil(token) || String.length(token) == 0 || is_nil(os) || String.length(os) == 0 do
      true ->
        Logger.error(__ENV__, "ERROR - Push Notif Friendship - one or more param are null or empty", [%{token: token}, %{os: os}])
        :error
      false ->
        os_type = os |> String.trim |> String.to_atom
        push_friendship(%{user_id: user_id, os: os_type, token: String.trim(token), playload: playload})
    end
  end

  #
  # Private
  #

  defp push_message(%{user_id: user_id, os: os, token: token, msg: msg, playload: playload}) do
    case os do
      :ios ->
        case BackupController.backup_pending_light(user_id) do
          [_item] ->
            new(msg, token)
              |> put_sound("default")
              |> put_content_available
              |> put_mutable_content
              |> put_custom(%{"type" => notif_message, "data" => playload})
              |> Pigeon.APNS.push()
            Logger.info(__ENV__, "iOS - Push Notif Message Sent", [%{token: token}, %{os: os}, %{msg: msg}, %{playload: playload}])
          list ->
            new("", token)
              |> put_custom(%{"type" => notif_backup})
              |> Pigeon.APNS.push()
            Logger.info(__ENV__, "iOS - Push Notif Backup (Message) Sent", [%{list: list}, %{token: token}, %{os: os}, %{msg: msg}, %{playload: playload}])
        end
        :ok
      :android ->
        #TODO - ANDROID PUSH
        :not_implemented
      _ ->
        Logger.error(__ENV__, "ERROR - Push Notif Message - os type not catched", [%{token: token}, %{os: os}, %{msg: msg}, %{playload: playload}])
        :error
    end
  end

  defp push_friendship(%{user_id: user_id, os: os, token: token, playload: playload}) do
    case os do
      :ios ->
        case BackupController.backup_pending_light(user_id) do
          [_item] ->
            new("", token)
              |> put_sound("default")
              |> put_content_available
              |> put_mutable_content
              |> put_custom(%{"type" => notif_friendship, "data" => playload})
              |> Pigeon.APNS.push()
            Logger.info(__ENV__, "iOS - Push Notif Friendship Sent", [%{token: token}, %{os: os}, %{playload: playload}])
          list ->
            new("", token)
              |> put_custom(%{"type" => notif_backup})
              |> Pigeon.APNS.push()
            Logger.info(__ENV__, "iOS - Push Notif Backup (Friendship) Sent", [%{list: list}, %{token: token}, %{os: os}, %{playload: playload}])
        end
        :ok
      :android ->
        #TODO - ANDROID PUSH
        :not_implemented
      _ ->
        Logger.error(__ENV__, "ERROR - Push Notif Friendship - os type not catched", [%{token: token}, %{os: os}, %{playload: playload}])
        :error
    end
  end

end
