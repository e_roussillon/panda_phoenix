defmodule PandaPhoenix.UserPasswordController do
  @moduledoc false

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.UserPassword
  alias PandaPhoenix.UserAuthenticationController

  plug :scrub_params, "user_password" when action in [:update]

  # def index(conn, _params) do
  #   user_passwords = Repo.all(UserPassword)
  #   render(conn, "index.json", user_passwords: user_passwords)
  # end
  #
  # def create(conn, %{"user_password" => user_password_params}) do
  #   changeset = UserPassword.changeset(%UserPassword{}, user_password_params)
  #
  #   case Repo.insert(changeset) do
  #     {:ok, user_password} ->
  #       conn
  #       |> put_status(:created)
  #       |> put_resp_header("location", user_password_path(conn, :show, user_password))
  #       |> render("show.json", user_password: user_password)
  #     {:error, changeset} ->
  #       conn
  #       |> put_status(:unprocessable_entity)
  #       |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end
  #
  # def show(conn, %{"id" => id}) do
  #   user_password = Repo.get!(UserPassword, id)
  #   render(conn, "show.json", user_password: user_password)
  # end

  #
  # Public
  #

  def insert_user_password(user, user_params) do
    changeset = UserPassword.changeset(%UserPassword{}, user_params)

    case Repo.insert(changeset) do
      {:ok, _} ->
        UserAuthenticationController.insert_user_token(user.id, user_params)
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def update(%{"user_id" => user_id, "user_password" => user_password_params}) do
    user_password = Repo.get_by!(UserPassword, user_id: user_id)
    changeset = UserPassword.changeset(user_password, user_password_params)

    case Repo.update(changeset) do
      {:ok, user_password} ->
        :ok
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  # def delete(conn, %{"id" => id}) do
  #   user_password = Repo.get!(UserPassword, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(user_password)
  #
  #   send_resp(conn, :ok, "{}")
  # end
end
