defmodule PandaPhoenix.PrivateRoomUserController do
  @moduledoc false

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.PrivateRoomController
  alias PandaPhoenix.PrivateMessageController
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateRoom
  alias PandaPhoenix.ErrorDB
  alias PandaPhoenix.ErrorPrivateMessage

  plug :scrub_params, "private_room_user" when action in [:create, :update]

  #
  # API
  #

  #
  # Public
  #

  def found_room_by_user_and_send_message(%{private_room_id: private_room_id,
                                                    user_id: user_id,
                                                 to_user_id: to_user_id,
                                                    message: message,
                                                     status: status,
                                                      index: index,
                                                    data_id: data_id}) do

    # case message do
    #   nil ->
    #     {:error, ErrorPrivateMessage.init_error_message_empty()}
    #
    #   message ->
    #     case String.length(message) > 0 && data_id <= 0 do
    #       false ->
    #         {:error, ErrorPrivateMessage.init_error_message_empty()}
    #
    #       true ->
            case private_room_id do
              private_room_id when is_integer(private_room_id) == false ->
                {:error, ErrorPrivateMessage.init_error_room_invalid()}

              private_room_id when private_room_id <= 0 or private_room_id == nil ->
                case PrivateRoom.query_private_room_with_friend_id_by_user_id(user_id, to_user_id) do
                  {:ok, nil} ->
                    private_room_params = %{"name" => Integer.to_string(user_id) <> "_" <> Integer.to_string(to_user_id)}

                    case PrivateRoomController.create(%{"private_room" => private_room_params}) do
                      {:ok, private_room} ->
                        private_room_user = %{"room_id" => private_room.id, "user_id" => user_id}
                        private_room_user2 = %{"room_id" => private_room.id, "user_id" => to_user_id}

                        case create_list(%{"private_room_user" => [private_room_user, private_room_user2], "private_room" => private_room, "result" => []}) do
                          {:ok, private_room_users} ->
                            private_message_params = %{"room_id" => private_room.id, "msg" => message, "from_user_id" => user_id, "status" => status, "index_row" => index, "data_id" => data_id}
                            PrivateMessageController.create(%{"private_message" => private_message_params, "private_room_user" => private_room_users, "private_room" => private_room})

                          {:error, changeset} ->
                            {:error, changeset}
                        end

                      {:error, changeset} ->
                        {:error, changeset}
                    end
                  {:ok, %{pseudo: _, room_id: room_id, user_thumb: _}} ->
                    private_message_params = %{"room_id" => room_id, "msg" => message, "from_user_id" => user_id, "status" => status, "index_row" => index, "data_id" => data_id}
                    PrivateMessageController.create(%{"private_message" => private_message_params})

                  _ ->
                    {:error, ErrorDB.init_error()}
                end
              private_room_id when private_room_id > 0 ->
                private_message_params = %{"room_id" => private_room_id, "msg" => message, "from_user_id" => user_id, "status" => status, "index_row" => index, "data_id" => data_id}
                PrivateMessageController.create(%{"private_message" => private_message_params})
            end
        # end
  #   end
  end

  # def index(conn, _params) do
  #   private_room_users = Repo.all(PrivateRoomUser)
  #   render(conn, "index.json", private_room_users: private_room_users)
  # end
  #
  # def create(conn, %{"private_room_user" => private_room_user_params}) do
  #   changeset = PrivateRoomUser.changeset(%PrivateRoomUser{}, private_room_user_params)
  #
  #   case Repo.insert(changeset) do
  #     {:ok, private_room_user} ->
  #       conn
  #       |> put_status(:created)
  #       # |> put_resp_header("location", private_room_user_path(conn, :show, private_room_user))
  #       |> render("show.json", private_room_user: private_room_user)
  #     {:error, changeset} ->
  #       conn
  #       |> put_status(:unprocessable_entity)
  #       |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end
  #
  # def show(conn, %{"id" => id}) do
  #   private_room_user = Repo.get!(PrivateRoomUser, id)
  #   render(conn, "show.json", private_room_user: private_room_user)
  # end
  #
  # def update(conn, %{"id" => id, "private_room_user" => private_room_user_params}) do
  #   private_room_user = Repo.get!(PrivateRoomUser, id)
  #   changeset = PrivateRoomUser.changeset(private_room_user, private_room_user_params)
  #
  #   case Repo.update(changeset) do
  #     {:ok, private_room_user} ->
  #       render(conn, "show.json", private_room_user: private_room_user)
  #     {:error, changeset} ->
  #       conn
  #       |> put_status(:unprocessable_entity)
  #       |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   private_room_user = Repo.get!(PrivateRoomUser, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(private_room_user)
  #
  #   send_resp(conn, :ok, "{}")
  # end

  def delete([]) do

  end

  def delete([h|t]) do
    Repo.delete(h)
    delete(t)
  end

  #
  # Private
  #

  defp create_list(%{"private_room_user" => [], "private_room" => _private_room, "result" => private_room_users}) do
    {:ok, private_room_users}
  end

  defp create_list(%{"private_room_user" => [h | t], "private_room" => private_room, "result" => result}) do
    changeset = PrivateRoomUser.changeset(%PrivateRoomUser{}, h)
    case Repo.insert(changeset) do
      {:ok, private_room_user} ->
        create_list(%{"private_room_user" => t, "private_room" => private_room, "result" => result ++ [private_room_user]})
      {:error, changeset} ->
        delete(result)
        PrivateRoomController.delete(%{"private_room" => private_room})
        Logger.error(__ENV__, "Imposible to create 'private_room_user' into DB", [%{"private_room_user_params" => h}, %{"changeset" => changeset}])
        {:error, changeset}
    end
  end

end
