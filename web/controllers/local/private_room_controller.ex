defmodule PandaPhoenix.PrivateRoomController do
  @moduledoc false

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.PrivateRoom

  plug :scrub_params, "private_room" when action in [:create, :update]

  #
  # Public
  #

  # def index(conn, _params) do
  #   private_rooms = Repo.all(PrivateRoom)
  #   render(conn, "index.json", private_rooms: private_rooms)
  # end

  def create(%{"private_room" => private_room_params}) do
    changeset = PrivateRoom.changeset(%PrivateRoom{}, private_room_params)

    case Repo.insert(changeset) do
      {:ok, private_room} ->
        {:ok, private_room}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  # def show(conn, %{"id" => id}) do
  #   private_room = Repo.get!(PrivateRoom, id)
  #   render(conn, "show.json", private_room: private_room)
  # end

  # def update(conn, %{"id" => id, "private_room" => private_room_params}) do
  #   private_room = Repo.get!(PrivateRoom, id)
  #   changeset = PrivateRoom.changeset(private_room, private_room_params)
  #
  #   case Repo.update(changeset) do
  #     {:ok, private_room} ->
  #       render(conn, "show.json", private_room: private_room)
  #     {:error, changeset} ->
  #       conn
  #       |> put_status(:unprocessable_entity)
  #       |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end
  #
  # def delete(conn, %{"id" => id}) do
  #   private_room = Repo.get!(PrivateRoom, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(private_room)
  #
  #   send_resp(conn, :ok, "{}")
  # end

  def delete(%{"private_room" => private_room}) do
    Repo.delete(private_room)
  end
end
