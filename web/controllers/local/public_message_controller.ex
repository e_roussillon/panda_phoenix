defmodule PandaPhoenix.PublicMessageController do
  @moduledoc false

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.PublicMessage

  plug :scrub_params, "public_message" when action in [:create, :update]

  # def index(conn, _params) do
  #   public_messages = Repo.all(PublicMessage)
  #   render(conn, "index.json", public_messages: public_messages)
  # end

  def create(%{"public_message" => public_message_params}) do
    changeset = PublicMessage.changeset(%PublicMessage{}, public_message_params)

    case Repo.insert(changeset) do
      {:ok, public_message} ->
        {:ok, public_message}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  # def show(conn, %{"id" => id}) do
  #   public_message = Repo.get!(PublicMessage, id)
  #   render(conn, "show.json", public_message: public_message)
  # end

  # def update(conn, %{"id" => id, "public_message" => public_message_params}) do
  #   public_message = Repo.get!(PublicMessage, id)
  #   changeset = PublicMessage.changeset(public_message, public_message_params)
  #
  #   case Repo.update(changeset) do
  #     {:ok, public_message} ->
  #       render(conn, "show.json", public_message: public_message)
  #     {:error, changeset} ->
  #       conn
  #       |> put_status(:unprocessable_entity)
  #       |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   public_message = Repo.get!(PublicMessage, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(public_message)
  #
  #   send_resp(conn, :ok, "{}")
  # end
end
