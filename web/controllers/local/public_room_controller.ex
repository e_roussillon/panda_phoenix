defmodule PandaPhoenix.PublicRoomController do
  @moduledoc false

  use PandaPhoenix.Web, :controller

  alias PandaPhoenix.PublicRoom
  alias PandaPhoenix.ErrorDB

  plug :scrub_params, "public_room" when action in [:create, :update]

  def show(conn, %{"id" => id}) do
    case PublicRoom.query_path_from_public_room_id_to_root(id) do
      {:ok, public_room} ->
        render(conn, PandaPhoenix.PublicRoomView, "token.json", %{token: Token.sign_public_room(public_room), public_room: public_room})
      # _ ->
        # conn
        #   |> put_status(:not_found)
        #   |> render(PandaPhoenix.ChangesetView, "error.json", ErrorPublicRoom.init_error_room_not_found())
    end


  end

  def find_or_create_path_rooms(conn, public_room_localization_id, generic_room) do
    last_id = find_or_create_rooms(1, conn, generic_room[:country], "country")
      |> find_or_create_rooms(conn, generic_room[:state], "state")
      |> find_or_create_rooms(conn, generic_room[:county], "county")
      |> find_or_create_rooms(conn, generic_room[:city], "city")
      |> find_or_create_rooms(conn, generic_room[:neighborhood], "neighborhood")
      |> find_or_create_rooms(conn, generic_room[:street], "street")
      |> find_or_create_rooms(conn, generic_room[:street_number], "street_number")

    conn
      |> PandaPhoenix.PublicRoomLocalizationController.update(%{"id" => public_room_localization_id, "public_room_localization" => %{"public_room_id" => last_id}})
  end

  #
  # PRIVATE METHODE
  #

  defp find_or_create_rooms(root_room_id, conn, room_name, room_type) do
    case PublicRoom.query_immediate_nodes_below_public_room_id_and_name(root_room_id, room_name, room_type) do
      {:ok, %{depth: _, id: id, name: _}} ->
        id

      {:ok, _} ->
        case PublicRoom.insert_public_room(root_room_id, room_name, room_type) do
          {:ok, %{insert_public_room: id}} ->
            {row, _} = Integer.parse(id)
            row

          _ ->
            conn
              |> put_status(:unprocessable_entity)
              |> render(PandaPhoenix.ChangesetView, "error.json", ErrorDB.init_error())
        end

      _ ->
        conn
          |> put_status(:unprocessable_entity)
          |> render(PandaPhoenix.ChangesetView, "error.json", ErrorDB.init_error())
    end
  end

  # def index(conn, _params) do
  #   public_rooms = Repo.all(PublicRoom)
  #   render(conn, "index.json", public_rooms: public_rooms)
  # end

  # def create(conn, %{"public_room" => public_room_params}) do
  #   changeset = PublicRoom.changeset(%PublicRoom{}, public_room_params)
  #
  #   case Repo.insert(changeset) do
  #     {:ok, public_room} ->
  #       conn
  #         |> put_status(:created)
  #         # |> put_resp_header("location", public_room_path(conn, :show, public_room))
  #         |> render("show.json", public_room: public_room)
  #     {:error, changeset} ->
  #       conn
  #         |> put_status(:unprocessable_entity)
  #         |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end

  # def update(conn, %{"id" => id, "public_room" => public_room_params}) do
  #   public_room = Repo.get!(PublicRoom, id)
  #   changeset = PublicRoom.changeset(public_room, public_room_params)
  #
  #   case Repo.update(changeset) do
  #     {:ok, public_room} ->
  #       render(conn, "show.json", public_room: public_room)
  #     {:error, changeset} ->
  #       conn
  #         |> put_status(:unprocessable_entity)
  #         |> render(PandaPhoenix.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   public_room = Repo.get!(PublicRoom, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(public_room)
  #
  #   send_resp(conn, :ok, "{}")
  # end

end
