defmodule PandaPhoenix.BroadcastChannel do
  use PandaPhoenix.Web, :channel

  alias PandaPhoenix.Watcher.Channel.ChannelWatcher

  def join("broadcast_channel:" <> room_id, payload, socket) do
    Logger.info(__ENV__, "broadcast_channel:#{room_id}", [%{payload: payload}, %{socket: socket}])
    case authorized(socket, payload, room_id) do
      {:ok, socket} ->
        {:ok, socket}
      {:reported, %{time: time}} ->
        {:error, %{reason: "blocked", time: time}}
      :error ->
        Logger.error(__ENV__, "error", [%{room_id: room_id}, %{payload: payload}, %{socket: socket}])
        {:error, %{reason: "unauthorized"}}
    end
  end

  defp authorized(socket, %{"subject" => token, "body" => room_token}, room_id) do
    case Token.verify_user(%{token: token, user_id: socket.assigns.user_id, with_disconnection: false}) do
      {:authenticated, _} ->
        case Token.verify_public_room(room_token) do
            {:authenticated, public_room} ->
              ChannelWatcher.monitor(self(), {PandaPhoenix.Watcher.Channel.BroadcastWatcher, %{user_id: socket.assigns.user_id, room_id: room_id, room: public_room}})
              {:ok, assign(socket, :room, public_room)}
            :invalid ->
              ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.BroadcastWatcher, socket.assigns.user_id)
              :error
        end
      {:reported, %{time: time}} ->
        {:reported, %{time: time}}
      _ ->
        :error
    end
  end
end
