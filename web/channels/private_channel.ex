defmodule PandaPhoenix.PrivateChannel do
  use PandaPhoenix.Web, :channel

  alias PandaPhoenix.Watcher.Channel.ChannelWatcher
  alias PandaPhoenix.PrivateRoomUserController
  alias PandaPhoenix.PrivateMessageView
  alias PandaPhoenix.ChangesetView
  alias PandaPhoenix.PrivateMessage
  alias PandaPhoenix.ErrorPrivateMessage
  alias PandaPhoenix.ErrorDB
  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.PushNotificationController
  alias PandaPhoenix.BackupController
  alias PandaPhoenix.BackupView


  def news_msg, do: "news:msg"
  def news_msg_error, do: "news:msg:error"
  def news_msg_confirmation, do: "news:msg:confirmation"
  def news_users_connect, do: "news:users:connect"
  def news_user_connect, do: "news:user:connect"
  def news_user_disconnect, do: "news:user:disconnect"
  def news_user_duplicat, do: "news:user:duplicate"
  def news_user_reported, do: "news:user:reported"
  def news_user_friendship, do: "news:user:friendship"
  def news_msg_pending, do: "news:msg:pending"

  def channel_name(type) do
    case type do
      :msg ->
        news_msg
      :error ->
        news_msg_error
      :confirmation ->
        news_msg_confirmation
      :reported ->
        news_user_reported
      :friendship ->
        news_user_friendship
      :connect_list ->
        news_users_connect
      :connect ->
        news_user_connect
      :disconnect ->
        news_user_disconnect
      :duplicat ->
        news_user_duplicat
      :pending ->
        news_msg_pending
    end
  end


  def join("private_channel:" <> _, payload, socket) do
    case authorized(socket, payload) do
      {:ok, socket} ->
        send self(), :after_join
        {:ok, socket}
      {:reported, %{time: time}} ->
        {:error, %{reason: "blocked", time: time}}
      :error ->
        {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_in("news:msg", %{"index" => index,
                               "user_name" => user_name,
                               "private_room_id" => private_room_id,
                               "message" => msg,
                               "to_user_id" => to_user_id,
                               "to_user_name" => to_user_name,
                               "data_id" => data_id}, socket) do
     user_id = socket.assigns[:user_id]

    Logger.debug(__ENV__, "news:msg", [%{ "index" => index},
                                 %{"user_name" => user_name},
                                 %{"private_room_id" => private_room_id},
                                 %{"message" => msg},
                                 %{"to_user_id" => to_user_id},
                                 %{"to_user_name" => to_user_name},
                                 %{"data_id" => data_id}])

     query = UserFriendship.query_user_friendship(user_id, to_user_id)
     case Repo.all(query) do
         [] ->
           to_user = %{"to_user_id" => to_user_id,
                        "to_user_name" => to_user_name,
                        "friendship_status" => UserFriendship.friendship_not_friend,
                        "friendship_last_action" => -1,
                        "from_user_id" => user_id,
                        "from_user_name" => user_name}

           send_message(socket, %{private_room_id: private_room_id,
                                        user_id: user_id,
                                     to_user_id: to_user_id,
                                        message: msg,
                                         status: PrivateMessage.message_sent,
                                          index: index,
                                          to_user: to_user,
                                          data_id: data_id})
         [result] ->
           status = result.status
           if status == UserFriendship.friendship_blocked_before_be_friend || status == UserFriendship.friendship_blocked_after_be_friend do
             Logger.warn(__ENV__, "message is blocked", [%{result: result}])
             response = ChangesetView.render("error.json", ErrorPrivateMessage.init_error_message_blocked())
             push(socket, "news:msg:error", %{"index" => index, "user" => user_id, "error" => response})
           else
             to_user = %{ "to_user_id" => to_user_id,
                          "to_user_name" => to_user_name,
                          "friendship_status" => result.status,
                          "friendship_last_action" => result.last_action,
                          "from_user_id" => user_id,
                          "from_user_name" => user_name}

             send_message(socket, %{private_room_id: private_room_id,
                                          user_id: user_id,
                                       to_user_id: to_user_id,
                                          message: msg,
                                           status: PrivateMessage.message_sent,
                                            index: index,
                                            to_user: to_user,
                                            data_id: data_id})
           end
         _ ->
          push(socket, channel_name(:error), %{"index" => index, "user" => user_id, "error" => ChangesetView.render("error.json", ErrorDB.init_error())})
     end


    {:noreply, socket}
  end

  def handle_in("news:msg:confirmation", %{"messages" => messages}, socket) do
    Logger.info(__ENV__, "receive confirmation from user_id #{socket.assigns[:user_id]}", [%{"messages" => messages}])
    send self(), {:send_confirmation, messages}
    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do
    user_id = socket.assigns[:user_id]
    backup = BackupController.backup_pending(user_id)
    Logger.warn(__ENV__, "after_join send backup for user_id #{user_id}", [%{backup: backup}])
    push socket, channel_name(:pending), BackupView.render("show.json", %{backup: backup, user_id: user_id})
    {:noreply, socket}
  end

  def handle_info({:send_confirmation, messages}, socket) do
    PandaPhoenix.MessageSenderController.private_messages_confirmation(messages, Map.new, socket.assigns[:user_id])
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized(socket, %{"subject" => token, "body" => body}) do
    case Token.verify_user(%{token: token, user_id: socket.assigns.user_id, with_disconnection: true}) do
      {:authenticated, user_id} ->
        ChannelWatcher.monitor(self(), {PandaPhoenix.Watcher.Channel.PrivateWatcher, %{user_id: user_id}})
        {:ok, assign(socket, :user_name, body)}
      {:reported, %{time: time}} ->
          {:reported, %{time: time}}
      _ ->
        :error
    end
  end

  # defp update_message(id: id, user_id: _user_id, status: status) do
  #   case PrivateMessageController.update(%{"id" => id, "private_message" => %{status: status}}) do
  #     {:error, %{type: type, code: code, msg: message}} ->
  #       response = ChangesetView.render("error.json", %{type: type, code: code, msg: message})
  #       {:error, response}
  #
  #     private_room_user ->
  #       {:ok, private_room_user}
  #   end
  # end

  defp send_message(socket, %{private_room_id: private_room_id,
                               user_id: user_id,
                            to_user_id: to_user_id,
                               message: msg,
                                status: status,
                                 index: index,
                                 to_user: to_user,
                                 data_id: data_id}) do
    case PrivateRoomUserController.found_room_by_user_and_send_message(%{private_room_id: private_room_id,
                                                                                 user_id: user_id,
                                                                              to_user_id: to_user_id,
                                                                                 message: msg,
                                                                                  status: status,
                                                                                   index: index,
                                                                                 data_id: data_id}) do
      {:ok, message} ->
        response = PrivateMessageView.render("show.json", %{private_message: message, user: to_user})
        push(socket, news_msg, response)

        case Repo.one(UserAuthentication.query_auth_by_user_id(to_user_id)) do
          nil -> Logger.error(__ENV__, "Cannot found user")
          auth ->
            case auth.is_online do
              true ->
                Logger.warn(__ENV__, "user online - send broadcast to his room", [%{channel: "private_channel:#{to_user_id}"}, %{response: response}])
                PandaPhoenix.Endpoint.broadcast_from(self(), "private_channel:#{to_user_id}", channel_name(:msg), response)
              false ->
                Logger.warn(__ENV__, "user offline - send push notif", [%{channel: "private_channel:#{to_user_id}"}, %{response: response}])
                PushNotificationController.push(:msg, %{user_id: auth.user_id, os: auth.os, token: auth.push_token, msg: msg, playload: response})
            end
        end
      {:error, %{type: type, code: number, msg: message}} ->
        response = ChangesetView.render("error.json", %{type: type, code: number, msg: message})
        push(socket, channel_name(:error), %{"index" => index, "user" => to_user, "error" => response})

      {:error, error} ->
        response = ChangesetView.render("error.json", %{changeset: error})
        push(socket, channel_name(:error), %{"index" => index, "user" => to_user, "error" => response})
    end
  end
end
