defmodule PandaPhoenix.SaleChannel do
  use PandaPhoenix.Web, :channel

  alias PandaPhoenix.Watcher.Channel.ChannelWatcher
  alias PandaPhoenix.PublicMessageController
  alias PandaPhoenix.GenericMessageView
  alias PandaPhoenix.PublicMessageView
  alias PandaPhoenix.ChangesetView

  def join("sale_channel:" <> room_id, payload, socket) do
    case authorized(socket, payload, room_id) do
      {:ok, socket} ->
        {:ok, socket}
      {:reported, %{time: time}} ->
        {:error, %{reason: "blocked", time: time}}
      :error ->
        {:error, %{reason: "unauthorized"}}
    end
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (general_channel:lobby).
  def handle_in("news:msg", %{"message" => msg, "user" => user_name, "index" => index}, socket) do
    user_id = socket.assigns[:user_id]
    room = socket.assigns[:room]
    topic_id = String.split(socket.topic, ":") |> List.last()

    case PublicMessageController.create(%{"public_message" => %{"from_user_id" => user_id, "is_broadcast" => true, "msg" => msg, "public_room_id" => topic_id, "is_general" => false}}) do
      {:error, changeset} ->
        push(socket, "news:msg:error", GenericMessageView.render("show.json",  %{index: index, error: ChangesetView.render("error.json", %{changeset: changeset})}))

      {:ok, public_message} ->
        msg = PublicMessageView.render("show.json", %{public_message: public_message, user_name: user_name, index: index})
        broadcast! socket, "news:msg", msg
        Enum.each(room, fn(%{id: id, type: type, type: type, name: _}) ->
          if topic_id != "#{id}" do
            PandaPhoenix.Endpoint.broadcast_from(self(), "sale_channel:#{id}", "news:msg", msg)
          end
         end)
    end
    {:noreply, socket}
  end

  # # This is invoked every time a notification is being broadcast
  # # to the client. The default implementation is just to push it
  # # downstream but one could filter or change the event.
  # def handle_in("news:msg:outside", payload, socket) do
  #   broadcast socket, "news:msg", payload
  #   {:noreply, socket}
  # end
  #
  # # This is invoked every time a notification is being broadcast
  # # to the client. The default implementation is just to push it
  # # downstream but one could filter or change the event.
  # def handle_out(event, payload, socket) do
  #   push socket, event, payload
  #   {:noreply, socket}
  # end

  # Add authorization logic here as required.
  defp authorized(socket, %{"subject" => token, "body" => room_token}, room_id) do
    case Token.verify_user(%{token: token, user_id: socket.assigns.user_id, with_disconnection: false}) do
      {:authenticated, _} ->
        case Token.verify_public_room(room_token) do
            {:authenticated, public_room} ->
              ChannelWatcher.monitor(self(), {PandaPhoenix.Watcher.Channel.SaleWatcher, %{user_id: socket.assigns.user_id, room_id: room_id, room: public_room}})
              {:ok, assign(socket, :room, public_room)}
            :invalid ->
              ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.SaleWatcher, socket.assigns.user_id)
              :error
        end
      {:reported, %{time: time}} ->
        {:reported, %{time: time}}
      _ ->
        :error
    end
  end
end
