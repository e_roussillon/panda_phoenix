defmodule PandaPhoenix.GeneralChannel do
  use PandaPhoenix.Web, :channel

  alias PandaPhoenix.Watcher.Channel.ChannelWatcher
  alias PandaPhoenix.PublicMessageController
  alias PandaPhoenix.GenericMessageView
  alias PandaPhoenix.PublicMessageView
  alias PandaPhoenix.ChangesetView
  alias PandaPhoenix.PrivateChannel

  def join("general_channel:" <> room_id, payload, socket) do
    case authorized(socket, payload, room_id) do
      {:ok, socket} ->
        {:ok, socket}
      {:reported, %{time: time}} ->
        {:error, %{reason: "blocked", time: time}}
      :error ->
        Logger.error(__ENV__, "error", [%{room_id: room_id}, %{payload: payload}, %{socket: socket}])
        {:error, %{reason: "unauthorized"}}
    end
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (general_channel:lobby).
  def handle_in("news:msg", %{"message" => msg, "user" => user_name, "index" => index}, socket) do
    user_id = socket.assigns[:user_id]
    room = socket.assigns[:room]
    topic_id = String.split(socket.topic, ":") |> List.last()

    case PublicMessageController.create(%{"public_message" => %{"from_user_id" => user_id, "is_broadcast" => false, "msg" => msg, "public_room_id" => topic_id}}) do
      {:error, changeset} ->
        push(socket, "news:msg:error", GenericMessageView.render("show.json",  %{index: index, error: ChangesetView.render("error.json", %{changeset: changeset})}))

      {:ok, public_message} ->
        msg = PublicMessageView.render("show.json", %{public_message: public_message, user_name: user_name, index: index})
        broadcast! socket, "news:msg", msg
        Logger.debug(__ENV__, "room", [%{topic_id: topic_id}, %{room: room}])
        Enum.each(room, fn(%{id: id, type: type, type: type, name: _}) ->
          if topic_id != "#{id}" do
            Logger.debug(__ENV__, "room", [%{id: id}, %{topic_id: topic_id}])
            PandaPhoenix.Endpoint.broadcast_from(self(), "general_channel:#{id}", PrivateChannel.channel_name(:msg), msg)
          end
        end)
    end
    {:noreply, socket}
  end

  defp authorized(socket, %{"subject" => token, "body" => room_token}, room_id) do
    case Token.verify_user(%{token: token, user_id: socket.assigns.user_id, with_disconnection: false}) do
      {:authenticated, _} ->
        case Token.verify_public_room(room_token) do
            {:authenticated, public_room} ->
              ChannelWatcher.monitor(self(), {PandaPhoenix.Watcher.Channel.GeneralWatcher, %{user_id: socket.assigns.user_id, room_id: room_id, room: public_room}})
              {:ok, assign(socket, :room, public_room)}
            :invalid ->
              ChannelWatcher.demonitor_uid(PandaPhoenix.Watcher.Channel.GeneralWatcher, socket.assigns.user_id)
              :error
        end
      {:reported, %{time: time}} ->
        {:reported, %{time: time}}
      _ ->
        :error
    end
  end
end
