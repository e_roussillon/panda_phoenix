defmodule PandaPhoenix.UserSocket do

  alias PandaPhoenix.Token
  alias PandaPhoenix.Logger
  alias PandaPhoenix.Watcher.Channel.ChannelWatcher
  alias PandaPhoenix.Watcher.Reported.ReportedWatcher

  use Phoenix.Socket


  ## Channels
  channel "general_channel:*", PandaPhoenix.GeneralChannel
  channel "sale_channel:*", PandaPhoenix.SaleChannel
  channel "private_channel:*", PandaPhoenix.PrivateChannel
  channel "broadcast_channel:*", PandaPhoenix.BroadcastChannel

  ## Transports
  # transport :websocket, Phoenix.Transports.WebSocket, serializer: MsgPack
  transport :websocket, Phoenix.Transports.WebSocket,
    timeout: 45_000
  # transport :longpoll, Phoenix.Transports.LongPoll

  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.
  def connect(%{"token" => token}, socket) do
    case Token.verify_user(%{token: token, with_disconnection: false, can_active: false}) do
      {:authenticated, user_id} ->
        ChannelWatcher.monitor(self(), {PandaPhoenix.Watcher.Channel.UserSocketWatcher, %{user_id: user_id}})
        {:ok, assign(socket, :user_id, user_id)}
      {:reported, %{time: _}, user_id} ->
        case ReportedWatcher.is_active?(user_id) do
          :true ->
            :error
          :false ->
            ChannelWatcher.monitor(self(), {PandaPhoenix.Watcher.Channel.UserSocketWatcher, %{user_id: user_id}})
            {:ok, assign(socket, :user_id, user_id)}
        end
      _ ->
        Logger.error(__ENV__, "connect - error", [%{"token" => token}, %{socket: socket}])
        :error
    end
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "users_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     PandaPhoenix.Endpoint.broadcast("users_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  def id(socket), do: "users_socket:#{socket.assigns.user_id}"
end
