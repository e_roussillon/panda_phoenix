defmodule PandaPhoenix.PrivateMessage do
  @moduledoc false
  
  use PandaPhoenix.Web, :model

  alias PandaPhoenix.PrivateMessage
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.Data

  schema "private_messages" do
    field :room_id, :integer
    field :msg, :string
    field :status, :integer, default: -1
    field :from_user_id, :integer
    field :type, :integer, default: -1
    field :data_id, :integer
    field :index_row, :string

    timestamps
  end

  #
  # Status
  #

  def message_sending, do: 0
  def message_sent, do: 1
  def message_received, do: 2
  def message_read, do: 3
  def messages_confirmed, do: 4

  @required_fields ~w(room_id from_user_id index_row)
  @optional_fields ~w(status type data_id msg)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:index_row)
  end

  def query_message_with_data(message_id) do
    from pm in PrivateMessage,
      left_join: d in Data, on: d.id == pm.data_id,
      where: pm.id == ^message_id,
      select: %{id: pm.id, room_id: pm.room_id, from_user_id: pm.from_user_id, msg: pm.msg, status: pm.status, index_row: pm.index_row, inserted_at: fragment("extract(epoch from ?)", pm.inserted_at), url_thumb: d.url_thumb, url_original: d.url_original, url_blur: d.url_blur, type: d.type, size: d.size, data_inserted_at: fragment("extract(epoch from ?)", d.inserted_at)}
  end

  def query_backup_private_message_pending(user_id) do
    from pm in PrivateMessage,
          inner_join: pru in PrivateRoomUser, on: pm.room_id == pru.room_id and pru.user_id == ^user_id,
          where: pru.user_id == ^user_id and (pm.from_user_id == ^user_id and pm.status == ^PrivateMessage.message_read) or (pm.from_user_id != ^user_id and pm.status == ^PrivateMessage.message_sent),
          order_by: [pm.id, pm.room_id, pm.from_user_id, pm.msg, pm.status, pm.type, pm.index_row, pm.data_id, pm.updated_at],
          select: %{id: pm.id, room_id: pm.room_id, from_user_id: pm.from_user_id, msg: pm.msg, status: pm.status, type: pm.type, index_row: pm.index_row, data_id: pm.data_id, inserted_at: fragment("extract(epoch from ?)", pm.inserted_at)}
  end

  def query_backup_private_message_pending_only_sent(user_id) do
    from pm in PrivateMessage,
          inner_join: pru in PrivateRoomUser, on: pm.room_id == pru.room_id and pru.user_id == ^user_id,
          where: pru.user_id == ^user_id and (pm.from_user_id != ^user_id and pm.status == ^PrivateMessage.message_sent),
          order_by: [pm.id, pm.room_id, pm.from_user_id, pm.msg, pm.status, pm.type, pm.index_row, pm.data_id, pm.updated_at],
          select: %{id: pm.id, room_id: pm.room_id, from_user_id: pm.from_user_id, msg: pm.msg, status: pm.status, type: pm.type, index_row: pm.index_row, data_id: pm.data_id, inserted_at: fragment("extract(epoch from ?)", pm.inserted_at)}
  end
end
