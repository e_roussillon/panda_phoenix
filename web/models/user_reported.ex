defmodule PandaPhoenix.UserReported do
  @moduledoc false
  
  use PandaPhoenix.Web, :model

  alias PandaPhoenix.UserReported

  schema "user_reported" do
    field :user_id, :integer
    field :count, :integer
    field :is_used, :boolean, default: false
    field :is_active, :boolean, default: false
    field :notified, :integer, default: 0

    timestamps
  end

  @required_fields ~w(user_id count)
  @optional_fields ~w(is_used is_active notified)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def query_reported_by_uid(user_id) do
    from u in UserReported,
          where: u.user_id == ^user_id
  end

  # def query_reported_not_use() do
  #   from u in UserReported,
  #         where: u.is_used == false
  # end
end
