defmodule PandaPhoenix.PublicMessage do
  @moduledoc false
  
  use PandaPhoenix.Web, :model

  schema "public_messages" do
    field :from_user_id, :integer
    field :is_broadcast, :boolean, default: false
    field :is_general, :boolean, default: true
    field :data_id, :integer, default: -1
    field :msg, :string
    field :public_room_id, :integer

    timestamps
  end

  @required_fields ~w(from_user_id msg public_room_id)
  @optional_fields ~w(data_id is_broadcast is_general)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

end
