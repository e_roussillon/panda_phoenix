defmodule PandaPhoenix.ResetPassword do
  @moduledoc false

  use PandaPhoenix.Web, :model

  schema "reset_password" do
    field :new_password, :string, virtual: true
    field :confirm_password, :string, virtual: true
  end

  @required_fields ~w(new_password confirm_password)
  @optional_fields ~w()

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields, @optional_fields)
    |> validate_password(params)
    |> validate_length(:new_password, min: 6)
  end

  def init_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields, @optional_fields)
  end

  defp validate_password(changeset, params) do
    psw = params["new_password"]
    cpsw = params["confirm_password"]

    new_c =
      if (psw != nil and cpsw != nil and "#{psw}" == "#{cpsw}") do
        changeset
      else
        add_error(changeset, :new_password, "password are not equal") |> add_error(:confirm_password, "password are not equal")
      end

    new_c
  end

end
