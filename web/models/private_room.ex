defmodule PandaPhoenix.PrivateRoom do
  @moduledoc false
  
  use PandaPhoenix.Web, :model

  alias PandaPhoenix.PrivateRoom
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateMessage

  schema "private_rooms" do
    field :name, :string
    field :is_group, :boolean, default: false
    field :data_id, :integer

    timestamps
  end

  @required_fields ~w(name)
  @optional_fields ~w(is_group data_id)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  #
  # SQL
  #

  def query_backup_private_rooms(user_id, date) do
    from r in PrivateRoom,
          inner_join: ru in PrivateRoomUser, on: r.id == ru.room_id,
          where: ru.user_id == ^user_id and r.updated_at >= ^date,
          group_by: [r.id, r.name, r.is_group, r.data_id, r.updated_at],
          select: %{id: r.id, name: r.name, is_group: r.is_group, data_id: r.data_id, updated_at: r.updated_at}
  end

  def query_backup_private_rooms_pending(user_id) do
    from r in PrivateRoom,
          inner_join: ru in PrivateRoomUser, on: r.id == ru.room_id,
          inner_join: pm in PrivateMessage, on: pm.room_id == r.id,
          where: ru.user_id == ^user_id and (pm.from_user_id == ^user_id and pm.status == ^PrivateMessage.message_read) or (pm.from_user_id != ^user_id and pm.status == ^PrivateMessage.message_sent),
          group_by: [r.id, r.name, r.is_group, r.data_id, r.updated_at],
          select: %{id: r.id, name: r.name, is_group: r.is_group, data_id: r.data_id, updated_at: r.updated_at}
  end

  def query_private_room_with_friend_id_by_user_id(user_id, to_user_id) do
    Repo
      |> Ecto.Adapters.SQL.query("SELECT
          private_rooms.id AS room_id,
          users.pseudo,
          user_datas.url_thumb AS user_thumb
      FROM
        private_rooms AS private_rooms,
        private_room_users AS private_room_users,
        (SELECT
              rooms.id AS room_id
          FROM
              private_room_users as chats
              JOIN private_rooms as rooms ON chats.room_id = rooms.id
          WHERE
              chats.room_id = rooms.id AND
              chats.user_id = $1
        ) room_users,
        users LEFT JOIN datas AS user_datas ON users.data_id = user_datas.id
WHERE
          private_rooms.is_group = false AND
          room_users.room_id = private_rooms.id AND
          private_room_users.room_id = private_rooms.id AND
          private_room_users.user_id = users.id AND
          private_room_users.user_id = $2
          GROUP BY private_rooms.id, users.pseudo, user_datas.url_thumb", [user_id, to_user_id])
      |> query_to_tuple_and_get_fisrt()
  end
end
