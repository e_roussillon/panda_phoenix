defmodule PandaPhoenix.ExtraInfos do
  @moduledoc false

  use PandaPhoenix.Web, :model

  alias PandaPhoenix.ExtraInfos

  schema "extra_infos" do
    field :content, :string
    field :language, :string
    field :type, :string

    timestamps
  end

  @required_fields ~w(content language type)
  @optional_fields ~w()

  defp term_and_condition, do: "term_and_condition"

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def query_extra_info_terms(language) do
    query_extra_info(language, term_and_condition)
  end

  defp query_extra_info(language, type) do
    from u in ExtraInfos,
          where: u.language == ^language and u.type == ^type
  end

end
