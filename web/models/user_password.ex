defmodule PandaPhoenix.UserPassword do
  @moduledoc false

  use PandaPhoenix.Web, :model
  alias PandaPhoenix.UserPassword

  schema "user_passwords" do
    field :user_id, :integer
    field :password, :string
    field :can_broadcast, :boolean
    field :reset_password_token, :string

    timestamps
  end

  @required_fields ~w(user_id password)
  @optional_fields ~w(can_broadcast reset_password_token)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> validate_length(:password, min: 6)
    |> generate_encrypted_password(params)
  end

  defp generate_encrypted_password(current_changeset, params) do
    psw = params["password"]

    new_c =
      if (psw != nil and String.length(psw) >= 6) do
        force_change(current_changeset, :password, Comeonin.Bcrypt.hashpwsalt(params["password"]))
      else
        current_changeset
      end

    new_c
  end


  def query_user_reset_password(user_id, reset_password_token) do
    from u in UserPassword,
          where: u.user_id == ^user_id and u.reset_password_token == ^reset_password_token,
          select: %{user_id: u.user_id}
  end

end
