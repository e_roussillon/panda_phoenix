defmodule PandaPhoenix.PrivateRoomUser do
  @moduledoc false
  
  use PandaPhoenix.Web, :model

  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateMessage

  schema "private_room_users" do
    field :room_id, :integer
    field :user_id, :integer

    timestamps
  end

  @required_fields ~w(room_id user_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  #
  # sql
  #

  def query_backup_private_room_user(user_id, date) do
    from pru in PrivateRoomUser,
        inner_join: r in fragment("SELECT p_r.id
                                    FROM private_rooms p_r
                                        RIGHT JOIN private_room_users ON p_r.id = room_id
                                    WHERE user_id = ?", ^user_id), on: r.id == pru.room_id,
        where: pru.updated_at >= ^date,
        group_by: [pru.user_id, pru.room_id, pru.updated_at],
        order_by: pru.room_id,
        select: %{user_id: pru.user_id, room_id: pru.room_id, updated_at: pru.updated_at}
  end

  def query_backup_private_rooms_user_pending(user_id) do
    from pm in PrivateMessage,
          inner_join: pru in PrivateRoomUser, on: pm.room_id == pru.room_id,
          inner_join: r in fragment("SELECT p_r.id
                                FROM private_rooms p_r
                                    RIGHT JOIN private_room_users ON p_r.id = room_id
                                WHERE user_id = ?", ^user_id), on: r.id == pru.room_id,
          where: (pm.from_user_id == ^user_id and pm.status == ^PrivateMessage.message_read) or (pm.from_user_id != ^user_id and pm.status == ^PrivateMessage.message_sent),
          group_by: [pru.user_id, pru.room_id, pru.updated_at],
          order_by: pru.room_id,
          select: %{user_id: pru.user_id, room_id: pru.room_id, updated_at: pru.updated_at}
  end

  def query_other_user_by_message_id_and_user_id(user_id, message_id) do
    from pru in PrivateRoomUser,
        join: pm in PrivateMessage, on: pru.room_id == pm.room_id,
        where: pm.id == ^message_id and pru.user_id != ^user_id,
        select: %{user_id: pru.user_id}
  end
end
