defmodule PandaPhoenix.PublicRoomLocalization do
  @moduledoc false

  use PandaPhoenix.Web, :model

  schema "public_room_localization" do
    field :public_room_id, :integer, default: -1
    field :latitude, :string
    field :longitude, :string
    field :api_result, :string
    field :result, :string, default: ""
    field :google_result, :string, default: ""

    timestamps
  end

  @required_fields ~w(latitude longitude api_result)
  @optional_fields ~w(public_room_id result google_result)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> update_change(:api_result, &(String.replace(&1, "%", "")))
    |> update_change(:result, &(String.replace(&1, "%", "")))
    |> update_change(:google_result, &(String.replace(&1, "%", "")))
    |> unique_constraint(:latitude_longitude)
  end

  def query_public_room_localized_by_lat_and_long(latitude, longitude) do
    Repo
      |> Ecto.Adapters.SQL.query("SELECT public_room_id FROM public_room_localization WHERE latitude = $1 AND longitude = $2", [latitude, longitude])
      |> query_to_tuple_and_get_fisrt()
    # query = from l in PublicRoomLocalization,
    #       where: l.latitude == ^latitude and l.longitude == ^longitude,
    #       select: {l.id}
  end
end
