defmodule PandaPhoenix.Data do
  @moduledoc false

  use PandaPhoenix.Web, :model
  use Arc.Ecto.Schema
  alias PandaPhoenix.Logger

  schema "datas" do
    field :url_thumb, :string
    field :url_original, :string
    field :url_blur, :string
    field :type, :integer, default: -1
    field :size, :float, default: 0.0

    timestamps
  end

  def data_none, do: -1
  def data_image, do: 0
  def data_website, do: 1

  def data_type(type) do
    case type do
      "image/jpeg" ->
        data_image
      "website" ->
        data_website
      other ->
        Logger.debug(__ENV__, "data type", [%{type: other}])
        data_none
    end
  end

  @required_fields ~w()
  @optional_fields ~w(url_thumb url_original url_blur type size)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
