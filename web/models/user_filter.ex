defmodule PandaPhoenix.UserFilter do
  @moduledoc false
  
  use PandaPhoenix.Web, :model

  alias PandaPhoenix.UserFilter
  alias PandaPhoenix.User

  schema "user_filters" do
    field :name, :string
    field :is_highlight, :boolean, default: false
    field :user_id, :integer
    field :active, :boolean, default: true

    timestamps
  end

  @required_fields ~w(name is_highlight user_id)
  @optional_fields ~w(active)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:name_user_id)
  end

  def query_filter_by_user_id_and_name(user_id, name) do
    query = from u in UserFilter,
          where: u.name == ^name and u.user_id == ^user_id,
          select: {u.id, u.active}
    Repo.all(query)

  end

  def query_backup_user_filters(user_id, date) do
    from u in User,
          join: f in UserFilter, on: f.user_id == u.id,
          where: u.id == ^user_id and f.active == true and f.updated_at >= ^date,
          select: %{id: f.id, user_id: u.id, name: f.name, is_highlight: f.is_highlight, updated_at: f.updated_at}
  end
end
