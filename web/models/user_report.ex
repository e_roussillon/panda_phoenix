defmodule PandaPhoenix.UserReport do
  @moduledoc false
  
  use PandaPhoenix.Web, :model

  schema "user_report" do
    field :user_id_reportor, :integer
    field :user_id_reported, :integer

    timestamps
  end

  @required_fields ~w(user_id_reportor user_id_reported)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> check_ids(params)
    |> unique_constraint(:user_id_reportor_user_id_reported)
  end

  defp check_ids(changeset, params) do
    user_id_reportor =
      case Map.get(params, "user_id_reportor") do
        nil ->
          Map.get(params, :user_id_reportor)
        other ->
          other
      end

    user_id_reported =
      case Map.get(params, "user_id_reported") do
        nil ->
          Map.get(params, :user_id_reported)
        other ->
          other
      end

    if user_id_reportor == user_id_reported do
      changeset
        |> add_error(:user_id_reportor, "can not be equal to other")
        |> add_error(:user_id_reported, "can not be equal to other")
    else
      changeset
    end

  end

end
