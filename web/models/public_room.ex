defmodule PandaPhoenix.PublicRoom do
  @moduledoc false
  
  use PandaPhoenix.Web, :model

  schema "public_rooms" do
    field :name, :string
    field :original_name, :string
    field :type, :string
    field :lft, :integer
    field :rgt, :integer

    timestamps
  end

  @required_fields ~w(name original_name type lft rgt)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def name_room(room_parent_id, original_name \\ "", type) do
     new_name = original_name |> String.downcase |> String.replace(" ", "-")
     new_name <> "_" <> Integer.to_string(room_parent_id) <> "_" <> type
  end

  def query_path_from_public_room_id_to_root(room_parent_id) do
    Repo
      |> Ecto.Adapters.SQL.query("SELECT parent.id, parent.original_name, parent.name, parent.type FROM public_rooms AS node, public_rooms AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.id = $1::integer ORDER BY node.lft", [room_parent_id])
      |> query_to_tuple()
  end

  def query_immediate_nodes_below_public_room_id_and_name(room_parent_id, original_name, type) do
    name = name_room(room_parent_id, original_name, type)
    Repo
      |> Ecto.Adapters.SQL.query("SELECT node.id, node.name, (COUNT(parent.name) - (sub_tree.depth + 1)) AS depth FROM public_rooms AS node, public_rooms AS parent, public_rooms AS sub_parent, (SELECT node.name, (COUNT(parent.name) - 1) AS depth FROM public_rooms AS node, public_rooms AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.id = $1 GROUP BY node.name, node.type, node.lft ORDER BY node.lft) AS sub_tree WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.lft BETWEEN sub_parent.lft AND sub_parent.rgt AND sub_parent.name = sub_tree.name AND node.name = $2 GROUP BY node.name, node.type, node.id, sub_tree.depth ORDER BY node.lft", [room_parent_id, name])
      |> query_to_tuple_and_get_fisrt()
  end

  def query_nodes_below_public_room_id(room_parent_id) do
    Repo
      |> Ecto.Adapters.SQL.query("SELECT node.id, node.name, (COUNT(parent.name) - (sub_tree.depth + 1)) AS depth FROM public_rooms AS node, public_rooms AS parent, public_rooms AS sub_parent, (SELECT node.name, (COUNT(parent.name) - 1) AS depth FROM public_rooms AS node, public_rooms AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.id = $1 GROUP BY node.name, node.type, node.lft ORDER BY node.lft) AS sub_tree WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.lft BETWEEN sub_parent.lft AND sub_parent.rgt AND sub_parent.name = sub_tree.name GROUP BY node.name, node.type, node.id, sub_tree.depth ORDER BY node.lft", [room_parent_id])
      |> query_to_tuple()
  end

  def query_immediate_nodes_below_public_room_id(room_parent_id) do
    Repo
      |> Ecto.Adapters.SQL.query("SELECT node.id, node.name, (COUNT(parent.name) - (sub_tree.depth + 1)) AS depth FROM public_rooms AS node, public_rooms AS parent, public_rooms AS sub_parent, (SELECT node.name, (COUNT(parent.name) - 1) AS depth FROM public_rooms AS node, public_rooms AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.id = $1 GROUP BY node.name, node.type, node.lft ORDER BY node.lft) AS sub_tree WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.lft BETWEEN sub_parent.lft AND sub_parent.rgt AND sub_parent.name = sub_tree.name GROUP BY node.name, node.type, node.id, sub_tree.depth HAVING (COUNT(parent.name) - (sub_tree.depth + 1)) = 1 ORDER BY node.lft;", [room_parent_id])
      |> query_to_tuple()
  end

  def insert_public_room(room_parent_id, original_name, type) do
    new_name = original_name |> String.downcase
    name = name_room(room_parent_id, original_name, type)
    Repo
     |> Ecto.Adapters.SQL.query("SELECT insert_public_room($1, $2, $3, $4)", [room_parent_id, new_name, name, type])
     |> query_to_tuple_and_get_fisrt()
  end

end
