defmodule PandaPhoenix.UserAuthentication do
  @moduledoc false

  use PandaPhoenix.Web, :model

  alias PandaPhoenix.UserAuthentication

  schema "user_authentications" do
    field :user_id, :integer
    field :token, :string
    field :refresh_token, :string
    field :room, :string
    field :last_seen, Ecto.DateTime
    field :is_online, :boolean, default: false
    field :push_token, :string
    field :os, :string

    timestamps
  end

  @required_fields ~w(user_id token refresh_token)
  @optional_fields ~w(room last_seen is_online push_token os)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:user_id)
    |> unique_constraint(:token)
    |> unique_constraint(:refresh_token)
  end

  def query_auth_by_user_id(user_id) do
    new_user_id =
      case user_id do
        nil ->
          ""
        other ->
          other
      end

    from u in UserAuthentication,
          where: u.user_id == ^new_user_id,
          select: u
  end

end
