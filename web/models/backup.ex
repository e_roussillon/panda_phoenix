defmodule PandaPhoenix.Backup do
  @moduledoc false
  
  alias PandaPhoenix.Repo
  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.User
  alias PandaPhoenix.UserFilter
  alias PandaPhoenix.PrivateRoom
  alias PandaPhoenix.PrivateRoomUser
  alias PandaPhoenix.PrivateMessage

  import PandaPhoenix.RetoUtil

  def query_backup_current_user(user_id_field) do
    Repo.all(User.query_backup_current_user(user_id_field))
      |> query_to_tuple("users")
  end

  def query_backup_filters(user_id_field, date_field) do
    date = get_current_date(date_field)
    Repo.all(UserFilter.query_backup_user_filters(user_id_field, date))
      |> query_to_tuple("user_filters")
  end

  def query_backup_chat_not_friend_users_light(user_id_field, date_field) do
    date = get_current_date(date_field)

    Repo
      |> Ecto.Adapters.SQL.query("SELECT
            u.id,
            u.pseudo,
            u.status,
            u.last_action,
            u.url_thumb,
            u.url_original,
            u.url_blur,
            extract(epoch from u.updated_at) as updated_at
      FROM private_room_users p_r_u, (
                                      SELECT u_tmp.id,
                                             u_tmp.pseudo,
                                             u1.status,
                                             u1.last_action,
                                             d.url_thumb,
                                             d.url_original,
                                             d.url_blur,
                                             public.check_biggest_date_between_three(u_tmp.updated_at, d.updated_at, u1.updated_at) AS updated_at,
                                             p_r_u_tmp.room_id
                                      FROM users u_tmp
                                            INNER JOIN private_room_users p_r_u_tmp ON u_tmp.id = p_r_u_tmp.user_id
                                            LEFT JOIN datas d ON d.id = u_tmp.data_id
                                            LEFT JOIN user_friendships u1 ON (u1.user_id = u_tmp.id AND u1.to_user_id = $1)
                                      				OR (u1.to_user_id = u_tmp.id AND u1.user_id = $1)
                                      WHERE (u1.status != $2 AND u1.status != $3 AND u1.status != $4 AND u1.status != $5) OR (u1.status IS NULL)
                                      				AND u_tmp.id != $1
                                      GROUP BY u_tmp.id, u_tmp.pseudo, u1.status, u1.last_action, d.url_thumb, d.url_original, d.url_blur, u_tmp.updated_at, d.updated_at, u1.updated_at, p_r_u_tmp.room_id) u
      WHERE p_r_u.user_id = $1
            AND u.room_id = p_r_u.room_id
            AND u.updated_at >= $6
      GROUP BY u.id, u.pseudo, u.status, u.last_action, u.url_thumb, u.url_original, u.url_blur, u.updated_at
      ORDER BY u.updated_at DESC", [user_id_field,
                                    UserFriendship.friendship_pending,
                                    UserFriendship.friendship_blocked_before_be_friend,
                                    UserFriendship.friendship_friend,
                                    UserFriendship.friendship_blocked_after_be_friend,
                                    date])
      |> query_to_tuple("users")
  end

  def query_backup_friendship_users_light(user_id_field, date_field) do
    date = get_current_date(date_field)
    Repo.all(UserFriendship.query_backup_friendship_users_light(user_id_field, date))
      |> query_to_tuple("users")
  end

  def query_backup_private_rooms(user_id_field, date_field) do
    date = get_current_date(date_field)
    Repo.all(PrivateRoom.query_backup_private_rooms(user_id_field, date))
      |> query_to_tuple("private_rooms")
  end

  def query_backup_private_rooms_user(user_id_field, date_field) do
    date = get_current_date(date_field)
    Repo.all(PrivateRoomUser.query_backup_private_room_user(user_id_field, date))
      |> query_to_tuple("private_room_users")
  end

  def query_backup_private_message(user_id_field, date_field) do
    date = get_current_date(date_field)
# WHERE p_m_tmp.row_num < 50
    Repo
      |> Ecto.Adapters.SQL.query("SELECT
          p_m_tmp.id, p_m_tmp.room_id, p_m_tmp.from_user_id, p_m_tmp.msg, p_m_tmp.status, p_m_tmp.type, p_m_tmp.index_row, p_m_tmp.data_id,
          extract(epoch from p_m_tmp.inserted_at) as inserted_at
      FROM (
          SELECT
              p_m.id, p_m.room_id, p_m.from_user_id, p_m.msg, p_m.status, p_m.type, p_m.index_row, p_m.data_id, p_m.inserted_at,
              row_number() over (partition by p_m.room_id) as row_num
          FROM private_messages p_m
              INNER JOIN (
                      SELECT
                          p_r.id
                      FROM private_rooms p_r
                              INNER JOIN private_room_users ON p_r.id = room_id
                      WHERE user_id = $1
              ) room ON room.id = p_m.room_id
          WHERE p_m.updated_at >= $2
          ORDER BY p_m.updated_at ASC
      ) p_m_tmp", [user_id_field, date])
      |> query_to_tuple("private_messages")

  end

  def query_backup_private_message_pending(user_id_field) do
    Repo.all(PrivateMessage.query_backup_private_message_pending(user_id_field))
      |> query_to_tuple("private_messages")
  end

  def query_backup_private_message_pending_only_sent(user_id_field) do
    Repo.all(PrivateMessage.query_backup_private_message_pending_only_sent(user_id_field))
      |> query_to_tuple("private_messages")
  end

  def query_backup_private_rooms_user_pending(user_id_field) do
    Repo.all(PrivateRoomUser.query_backup_private_rooms_user_pending(user_id_field))
      |> query_to_tuple("private_room_users")
  end

  def query_backup_private_rooms_pending(user_id_field) do
    Repo.all(PrivateRoom.query_backup_private_rooms_pending(user_id_field))
      |> query_to_tuple("private_rooms")
  end

  def query_backup_chat_not_friend_users_light_pending(user_id_field) do
    Repo
      |> Ecto.Adapters.SQL.query("SELECT
            u.id,
            u.pseudo,
            u.status,
            u.last_action,
            u.url_thumb,
            u.url_original,
            u.url_blur,
            extract(epoch from u.updated_at) as updated_at
      FROM private_room_users p_r_u, (
                                      SELECT u_tmp.id,
                                             u_tmp.pseudo,
                                             u1.status,
                                             u1.last_action,
                                             d.url_thumb,
                                             d.url_original,
                                             d.url_blur,
                                             public.check_biggest_date_between_three(u_tmp.updated_at, d.updated_at, u1.updated_at) AS updated_at,
                                             p_r_u_tmp.room_id
                                      FROM users u_tmp
                                            INNER JOIN private_room_users p_r_u_tmp ON u_tmp.id = p_r_u_tmp.user_id
                                            LEFT JOIN datas d ON d.id = u_tmp.data_id
                                            LEFT JOIN user_friendships u1 ON (u1.user_id = u_tmp.id AND u1.to_user_id = $1)
                                      				OR (u1.to_user_id = u_tmp.id AND u1.user_id = $1)
                                      WHERE (u1.status != $2 AND u1.status != $3 AND u1.status != $4 AND u1.status != $5) OR (u1.status IS NULL)
                                      				AND u_tmp.id != $1
                                      GROUP BY u_tmp.id, u_tmp.pseudo, u1.status, u1.last_action, d.url_thumb, d.url_original, d.url_blur, u_tmp.updated_at, d.updated_at, u1.updated_at, p_r_u_tmp.room_id) u
            INNER JOIN private_messages p_m ON p_m.from_user_id = u.id
      WHERE p_r_u.user_id = $1
            AND u.room_id = p_r_u.room_id
            AND (p_m.from_user_id = $1 and p_m.status = $6)
            OR (p_m.from_user_id <> $1 and p_m.status = $7)
      GROUP BY u.id, u.pseudo, u.status, u.last_action, u.url_thumb, u.url_original, u.url_blur, u.updated_at
      ORDER BY u.updated_at DESC", [user_id_field,
                                    UserFriendship.friendship_pending,
                                    UserFriendship.friendship_blocked_before_be_friend,
                                    UserFriendship.friendship_friend,
                                    UserFriendship.friendship_blocked_after_be_friend,
                                    PrivateMessage.message_read,
                                    PrivateMessage.message_sent])
      |> query_to_tuple("users")
  end

  defp get_current_date(date_field) do
    case date_field do
      nil ->
        case Ecto.DateTime.cast("2015-01-01T00:00:00Z") |> check_date |> Ecto.DateTime.dump() |> check_date do
          :error -> nil
          date -> date
        end
      _ ->
        case Ecto.DateTime.cast(date_field) |> check_date |> Ecto.DateTime.dump() |> check_date do
          :error -> nil
          date -> date
        end
    end
  end

  defp check_date(date) do
    case date do
      {:ok, date} -> date
      _ -> :error
    end

  end

end
