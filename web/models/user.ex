defmodule PandaPhoenix.User do
  @moduledoc false

  use PandaPhoenix.Web, :model

  alias PandaPhoenix.User
  alias PandaPhoenix.UserPassword
  alias PandaPhoenix.Data

  schema "users" do
    field :pseudo, :string
    field :email, :string
    field :description, :string
    field :birthday, Ecto.Date
    field :gender, :integer, default: 0
    field :data_id, :integer
    field :active, :boolean, default: false

    timestamps
  end

  @required_fields ~w(pseudo email birthday)
  @optional_fields ~w(description data_id gender active)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  site : https://regex101.com/r/uD2jK2/3#javascript
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> validate_format(:email, ~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/)
    |> validate_format(:pseudo, ~r/^[a-z0-9]*$/) #+([_-]{0,1})[a-z0-9]
    |> update_change(:email, &String.downcase/1)
    |> unique_constraint(:email)
    |> unique_constraint(:pseudo)
    |> validate_length(:pseudo, min: 4, max: 30)
    |> validate_birthday(:birthday)
  end

  defp validate_birthday(%{changes: changes}=changeset, field) do
    if date = changes[field] do
      %Ecto.Date{day: d, month: m, year: y} = date
      if do_age({y, m, d}, :now) < 16  do
        changeset
          |> add_error(field, "birthday under 16")
      else
        changeset
      end
    else
      changeset
    end
  end

  defp do_age(birthday, :now) do
    {today, _time} = :calendar.now_to_datetime(:erlang.timestamp)
    calc_diff(birthday, today)
  end
  defp do_age(birthday, date), do: calc_diff(birthday, date)

  @doc false
  defp calc_diff({y1, m1, d1}, {y2, m2, d2}) when m2 > m1 or (m2 == m1 and d2 >= d1) do
    y2 - y1
  end
  defp calc_diff({y1,_,_}, {y2,_,_}), do: (y2 - y1) - 1

  #
  # SQL
  #

  def query_user_by_email_and_pws(email) do
    new_e =
      if email == nil do
        ""
      else
        email
      end

    from u in User,
          join: p in UserPassword, on: u.id == p.user_id,
          where: u.email == ^new_e,
          select: {u.id, p.password, u.active}
  end

  def query_user_by_id(user_id) do
    new_id =
      case is_integer(user_id) do
        true ->
          user_id
        false ->
          -1
      end

    from u in User,
          join: p in UserPassword, on: u.id == p.user_id,
          where: u.id == ^new_id,
          select: %{id: u.id, pseudo: u.pseudo, can_broadcast: p.can_broadcast}
  end

  def query_backup_current_user(user_id) do
    from(u in User,
          left_join: d in Data, on: d.id == u.data_id,
          where: u.id == ^user_id,
          select: %{id: u.id, email: u.email, pseudo: u.pseudo, description: u.description, birthday: u.birthday, gender: u.gender, url_thumb: d.url_thumb, url_original: d.url_original, url_blur: d.url_blur, updated_at: fragment("extract(epoch from public.check_biggest_date_between_two(?, ?))", d.updated_at, u.updated_at)})
  end

  def query_user(user_id) do
    from u in User,
          where: u.id == ^user_id,
          select: %{id: u.id}
  end

  def query_user_data(user_id) do
    from u in User,
          where: u.id == ^user_id,
          select: %{data_id: u.data_id}
  end

end
