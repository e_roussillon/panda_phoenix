defmodule PandaPhoenix.UserFriendship do
  @moduledoc false

  use PandaPhoenix.Web, :model

  alias PandaPhoenix.UserFriendship
  alias PandaPhoenix.User
  alias PandaPhoenix.Data
  alias PandaPhoenix.UserAuthentication
  alias PandaPhoenix.Paginator
  # alias PandaPhoenix.Logger
  alias PandaPhoenix.UserReport

  schema "user_friendships" do
    field :user_id, :integer
    field :to_user_id, :integer
    field :status, :integer
    field :last_action, :integer

    timestamps
  end

  @required_start_fields ~w(to_user_id status)
  @optional_start_fields ~w(user_id last_action)

  @required_fields ~w(user_id to_user_id status last_action)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset_start(model, params \\ :empty) do
    model
    |> cast(params, @required_start_fields, @optional_start_fields)
    |> unique_constraint(:user_id_to_user_id)
    |> unique_constraint(:to_user_id_user_id)
    |> check_users_id(params)
  end

  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:user_id_to_user_id)
    |> unique_constraint(:to_user_id_user_id)
    |> check_users_id(params)
  end

  defp check_users_id(changeset, params) do
    user_id = Map.get(params, "user_id")
    to_user_id = Map.get(params, "to_user_id")

    case {user_id, to_user_id} do
      {nil, nil} ->
        changeset
      {_, nil} ->
        changeset
      {nil, _} ->
        changeset
      {user_id, to_user_id} when user_id == to_user_id ->
        add_error(changeset, :user_id, "User cannot be friend with his self")
      {user_id, to_user_id} when user_id > to_user_id ->
        force_change(changeset, :user_id, to_user_id)
          |>  force_change(:to_user_id, user_id)
      _ ->
        changeset
    end

  end

  #
  # Status
  #

  def friendship_not_friend, do: 0
  def friendship_pending, do: 1
  def friendship_cancel_request, do: 2
  def friendship_refused_before_be_friend, do: 3
  def friendship_blocked_before_be_friend, do: 4
  def friendship_friend, do: 5
  def friendship_refused_after_be_friend, do: 6
  def friendship_blocked_after_be_friend, do: 7
  def friendship_unblocked, do: 8

  #
  # SQL
  #

  def query_user_friendship(user_id, to_user_id) do
    {min_id, max_id} = get_min_value(%{user_id: user_id, to_user_id: to_user_id})

    from u in UserFriendship,
          where: u.user_id == ^min_id and u.to_user_id == ^max_id
  end

  def query_user_friendship_full_desc(user_id, to_user_id) do
    {min_id, max_id} = get_min_value(%{user_id: user_id, to_user_id: to_user_id})

    from u in User,
        left_join: d in Data, on: u.data_id == d.id,
        left_join: uf in UserFriendship, on: uf.user_id == ^min_id and uf.to_user_id == ^max_id,
        left_join: ua in UserAuthentication, on: ua.user_id == u.id,
        left_join: ur in UserReport, on: ur.user_id_reportor == ^user_id and ur.user_id_reported == u.id,
        where: u.id == ^to_user_id,
        order_by: [u.pseudo, u.email],
        select: %{id: u.id, pseudo: u.pseudo, birthday: u.birthday, gender: u.gender, description: u.description, url_thumb: d.url_thumb, url_original: d.url_original, url_blur: d.url_blur, friendship_status: uf.status, friendship_last_action: uf.last_action, is_online: ua.is_online, last_seen: fragment("extract(epoch from ?)", ua.last_seen), reported: ur.id, updated_at: fragment("extract(epoch from public.check_biggest_date_between_three(?, ?, ?))", u.updated_at, uf.updated_at, d.updated_at)}
  end

  def query_backup_friendship_users_light(user_id, date) do
    from(u in User,
        join: uf in UserFriendship, on: (uf.to_user_id == ^user_id and uf.user_id == u.id) or (uf.user_id == ^user_id and uf.to_user_id == u.id),
        join: ua in UserAuthentication, on: ua.user_id == u.id,
        left_join: d in Data, on: d.id == u.data_id,
        where: u.id != ^user_id and (uf.status == ^friendship_pending or uf.status == ^friendship_friend or (uf.status == ^friendship_refused_before_be_friend and uf.last_action != ^user_id) or (uf.status == ^friendship_cancel_request and uf.last_action != ^user_id) or ((uf.status == ^friendship_blocked_before_be_friend or uf.status == ^friendship_blocked_after_be_friend) and uf.last_action == ^user_id)) and fragment("public.check_biggest_date_between_three(?, ?, ?) >= ?", u.updated_at, uf.updated_at, d.updated_at, ^date),
        group_by: [u.id, u.pseudo, d.url_thumb, d.url_original, d.url_blur, uf.last_action, uf.status, ua.is_online, ua.last_seen, ua.updated_at, u.updated_at, uf.updated_at, d.updated_at],
        select: %{id: u.id, pseudo: u.pseudo, url_thumb: d.url_thumb, url_original: d.url_original, url_blur: d.url_blur, last_action: uf.last_action, status: uf.status, is_online: ua.is_online, last_seen: fragment("extract(epoch from ?)", ua.last_seen), updated_at: fragment("extract(epoch from public.check_biggest_date_between_three(?, ?, ?))", u.updated_at, uf.updated_at, d.updated_at)})
  end

  # def query_user_friendships_accepted(user_id, page) do
  #   query_user_friendship_by_status(user_id, friendship_friend(), page)
  # end

  def query_user_friendships_online_accepted(user_id) do
    query_user_friendship_online_by_status(user_id, friendship_friend())
  end

  # def query_user_friendships_pending(user_id, page) do
  #   query_user_friendship_by_status(user_id, friendship_pending(), page)
  # end
  #
  # def query_user_friendships_blocked(user_id, page) do
  #   query_user_friendship_by_status(user_id, friendship_blocked(), page)
  # end



  #
  # Private
  #

  # defp query_user_friendship_by_status(user_id, active, page) do
  #   query_user_friendship_by_status(user_id, active)
  #       |> Repo.paginate(page: page)
  # end
  #
  # defp query_user_friendship_by_status(user_id, active) do
  #   from(u in User,
  #       left_join: d in Data, on: u.data_id == d.id,
  #       inner_join: uf in UserFriendship, on: uf.user_id == u.id or uf.to_user_id == u.id,
  #       left_join: ua in UserAuthentication, on: ua.user_id == u.id,
  #       where: u.id != ^user_id and uf.status == ^active,
  #       order_by: [u.pseudo, u.email],
  #       select: %{id: u.id, pseudo: u.pseudo, url_thumb: d.url_thumb, url_original: d.url_original, url_blur: d.url_blur, friendship_status: uf.status, friendship_last_action: uf.last_action, is_online: ua.is_online, data_updated_at: d.updated_at, uf_updated_at: uf.updated_at, u_updated_at: u.updated_at})
  # end

  defp query_user_friendship_online_by_status(user_id, active) do
    from(u in User,
        left_join: d in Data, on: d.id == u.data_id,
        join: uf in UserFriendship, on: (uf.user_id == u.id and uf.to_user_id == ^user_id) or (uf.to_user_id == u.id and uf.user_id == ^user_id),
        join: ua in UserAuthentication, on: ua.user_id == u.id,
        where: u.id != ^user_id and uf.status == ^active and ua.is_online == true,
        order_by: [u.pseudo, u.email],
        select: %{id: u.id, pseudo: u.pseudo, url_thumb: d.url_thumb, url_original: d.url_original, url_blur: d.url_blur, friendship_status: uf.status, friendship_last_action: uf.last_action, is_online: ua.is_online, last_seen: fragment("extract(epoch from ?)", ua.last_seen), updated_at: fragment("extract(epoch from public.check_biggest_date_between_three(?, ?, ?))", u.updated_at, uf.updated_at, d.updated_at)})
  end

  def query_user_friendship_by_pseudo(user_id, pseudo, page) do
    from(u in User,
        join: uf_tmp in fragment("SELECT u0.\"id\", u1.\"status\", u1.\"last_action\", u1.\"updated_at\"
        			FROM \"users\" AS u0
        			LEFT JOIN \"user_friendships\" AS u1 ON (u1.\"user_id\" = u0.\"id\" AND u1.\"to_user_id\" = ?)
        				OR (u1.\"to_user_id\" = u0.\"id\" AND u1.\"user_id\" = ?)
        			WHERE (u1.\"status\" != ? AND u1.\"status\" != ? AND u1.\"status\" != ? AND u1.\"status\" != ?) OR (u1.\"status\" IS NULL)
        				AND u0.id != ?", ^user_id, ^user_id, ^friendship_pending, ^friendship_blocked_before_be_friend, ^friendship_friend, ^friendship_blocked_after_be_friend, ^user_id), on: uf_tmp.id == u.id,
        left_join: d in Data, on: d.id == u.data_id,
        left_join: ua in UserAuthentication, on: ua.user_id == u.id,
        where: like(u.pseudo, ^("#{pseudo}%")),
        order_by: fragment("length(substring(? FROM '[0-9]+')), ?", u.pseudo, u.pseudo),
        select: %{id: u.id, pseudo: u.pseudo, url_thumb: d.url_thumb, url_original: d.url_original, url_blur: d.url_blur, friendship_status: uf_tmp.status, friendship_last_action: uf_tmp.last_action, is_online: ua.is_online, last_seen: fragment("extract(epoch from ?)", ua.last_seen), updated_at: fragment("extract(epoch from public.check_biggest_date_between_three(?, ?, ?))", u.updated_at, uf_tmp.updated_at, d.updated_at)})
        |> Paginator.new(%{"page" => page})
  end

  defp get_min_value(%{user_id: user_id, to_user_id: to_user_id}) do
    case user_id < to_user_id do
      true ->
        {user_id, to_user_id}
      false ->
        {to_user_id, user_id}
    end
  end

end
