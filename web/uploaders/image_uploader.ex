defmodule PandaPhoenix.ImageUploader do
  @moduledoc false

  use Arc.Definition
  use Arc.Ecto.Definition

  @versions [:original, :thumb, :blur]

  # def __storage, do: Arc.Storage.Local

  # Whitelist file extensions:
  def validate({file, _}) do
    # .gif
    ~w(.jpg .jpeg .png) |> Enum.member?(Path.extname(file.file_name))
  end

  # Define a thumbnail transformation:
  def transform(:thumb, _) do
    {:convert, "-strip -thumbnail 100x100^ -gravity center -extent 100x100 -format jpg", :jpg}
  end

  def transform(:blur, _) do
    # http://www.imagemagick.org/Usage/blur/
    {:convert, "-strip -thumbnail 200x200^ -gravity center -extent 200x200 -channel RGBA -blur 2x6 -format jpg", :jpg}
  end

  # def transform(:blur, _) do
  #   {:convert, "-strip -negate -threshold 0 -thumbnail 200x200^ -gravity center -extent 200x200 -format jpg", :jpg}
  # end

  # Override the persisted filenames:
  def filename(version, {file, _scope}) do
    extname = Path.extname(file.file_name)
    name = Path.basename(file.file_name, extname)
            |> String.downcase()
    "#{Atom.to_string(version)}_#{name}"
  end

  # Override the storage directory:
  def storage_dir(_version, {_file, scope}) do
    case Application.get_env(:panda_phoenix, :environment) do
      :test ->
          "test/uploads/#{scope}"
      :dev ->
          "dev/uploads/#{scope}"
      :heroku ->
          "heroku/uploads/#{scope}"
      _ ->
        "uploads/#{scope}"
    end

  end

  # Provide a default URL if there hasn't been a file uploaded
  # def default_url(version, scope) do
  #   "/images/default_#{version}.png"
  # end

  # Specify custom headers for s3 objects
  # Available options are [:cache_control, :content_disposition,
  #  :content_encoding, :content_length, :content_type,
  #  :expect, :expires, :storage_class, :website_redirect_location]

  def s3_object_headers(_version, {file, _scope}) do
    [content_type: Plug.MIME.path(file.file_name)]
  end
end
