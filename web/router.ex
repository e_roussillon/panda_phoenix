defmodule PandaPhoenix.Router do
  @moduledoc false

  use PandaPhoenix.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
   plug PandaPhoenix.Plug.Authenticate
  end

  pipeline :rate do
   plug PandaPhoenix.Plug.RateLimit
  end

  scope "/", PandaPhoenix do
    pipe_through :browser # Use the default browser stack

    get "/", HomeController, :index
    get "/active", HomeController, :active_user
    get "/reset", HomeController, :reset
    post "/reset/:token/password", HomeController, :reset_password
    get "/:language/terms", HomeController, :terms
  end

  scope "/api", PandaPhoenix do
    pipe_through :api
    pipe_through :rate

    scope "/v1" do

      post "/auth", UserAuthenticationController, :login
      post "/auth/renew", UserAuthenticationController, :renew_token
      resources "/register", UserController, only: [:create]
      post "/reset_password", UserController, :reset_password
    end
  end

  scope "/api", PandaPhoenix do
    pipe_through :api
    pipe_through :auth

    scope "/v1" do

      post "/logout", UserAuthenticationController, :logout
      get "/auth/backup", BackupController, :backup
      get "/pending/backup", BackupController, :backup_pending_data

      post "/confirmation/messages", MessageSenderController, :private_messages_confirmation
      post "/broadcast/message", MessageSenderController, :public_message

      # Filter (Resfull)
      resources "/filters", UserFilterController, only: [:create, :update, :delete]
      # get "/user_filters", UserFilterController, :user_filters

      # Public Room
      post "/find_room", PublicRoomLocalizationController, :find_room

      resources "/users", UserController, only: [:show, :update]

      # Friendship
      post "/update_friendship", UserFriendshipController, :update_friendship
      # post "/list_accepted_friendships", UserFriendshipController, :list_accepted_friendships
      # post "/list_pending_friendships", UserFriendshipController, :list_pending_friendships
      # post "/list_blocked_friendships", UserFriendshipController, :list_blocked_friendships
      post "/find_friendships", UserFriendshipController, :find

      resources "/report", UserReportController, only: [:create]
      post "/push_token", UserAuthenticationController, :push_token

      post "/upload/image", DataController, :upload_image

      # post "/user_reported_remove", UserReportedController, :user_reported_remove
      # post "/user_reported_get", UserReportedController, :user_reported_get

      # resources "/user_socials", UserSocialController, except: [:new, :edit]
      # resources "/datas", DataController, except: [:new, :edit]
      # resources "/public_rooms", PublicRoomController, except: [:new, :edit]
      # resources "/private_rooms", PrivateRoomController, except: [:new, :edit]
      #
      # resources "/user_filters", UserFilterController, except: [:new, :edit]
      # # resources "/public_room_localization", PublicRoomLocalizationController, except: [:new, :edit]
      #
      # resources "/private_messages", PrivateMessageController, except: [:new, :edit]
      # resources "/private_room_users", PrivateRoomUserController, except: [:new, :edit]
      # resources "/public_messages", PublicMessageController, except: [:new, :edit]
      # resources "/user_friendships", UserFriendshipController, except: [:new, :edit]
      # resources "/user_passwords", UserPasswordController, except: [:new, :edit]
    end
  end

end
