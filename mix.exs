defmodule PandaPhoenix.Mixfile do
  use Mix.Project

  def project do
    [app: :panda_phoenix,
     version: "1.1.5",
     elixir: "~> 1.3.4",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases,
     deps: deps,
     test_coverage: [tool: ExCoveralls],
     preferred_cli_env: coverages]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {PandaPhoenix, []},
     applications: [:phoenix, :phoenix_pubsub, :phoenix_html, :cowboy, :logger, :gettext,
                    :phoenix_ecto, :postgrex, :comeonin, :httpoison, :pigeon, :arc_ecto,
                    :ex_aws, :ex_rated, :arc, :ex2ms, :jsx, :confex, :runtime_tools,
                    :observer, :wx, :bamboo, :bamboo_smtp]]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [{:phoenix, "~> 1.2.1"},
     {:phoenix_pubsub, "~> 1.0"},
     {:phoenix_ecto, "~> 3.0"},
     {:postgrex, "~> 0.11.2"},
     {:phoenix_html, "~> 2.6"},
     {:phoenix_live_reload, "~> 1.0", only: :dev},
     {:gettext, "~> 0.11.0"},
     {:cowboy, "~> 1.0"},
     {:comeonin, "~> 2.5"},
     {:httpoison, "~> 0.9.0"},
     {:excoveralls, "~> 0.5.5", only: :test},
     {:pigeon, "~> 0.10.3"},
     {:poison, "~> 2.1", override: true},
     {:arc_ecto, "~> 0.4.4"},
     {:arc, "0.5.3"},
     {:ex_aws, "~> 0.5.0"},
     {:ex_doc, "~> 0.14", only: :dev},
     {:ex_rated, github: "Douvi/ex_rated"},
     {:distillery, "~> 1.1.2"},
     {:jsx, "~> 2.5"},
     {:confex, "~> 1.4.1"},
     {:bamboo, "~> 0.7"},
     {:bamboo_smtp, "~> 1.2.1"}]
  end

# {:chatterbox, "~> 0.3.0-15-g401cef6", github: "joedevivo/chatterbox"},

  # Aliases are shortcut or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"],
    "test": ["ecto.create --quiet", "ecto.migrate", "test"]]
  end

  defp coverages do
    ["coveralls": :test, "coveralls.detail": :test, "coveralls.post": :test, "coveralls.html": :test]
  end
end
