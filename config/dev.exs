use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :panda_phoenix, PandaPhoenix.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  secret_key_base: "SwN0lF6gpELTyXbVgEJB4MydQfmRXhPU64S08GK3r4KlsgxuaGX/pT1AC/WTiVIM",
  secret_key_refresh_token: "QZWSlF6gpELTyXbVgEJB4MydQfmRXhPU64S08GK3r4KlsgxuaGX/pT1AC/WTiVIM",
  secret_key_base_data: "dkpTiFGxxm/XdHR7YDP5B8qNKk0uZ+iZa42MsKsDqKx1MOFrhfyMes8h28zeMZ2x",
  secret_key_base_activation_user: "QAWSAubJw/z6mfRJwqSOsw/Ww9tzuJmJQL6aHNh2lDOg1WvM44a5PDbOMaAbIWEEL",
  secret_key_base_reset_password: "a!JSLWSw/z6mfRJwqSOsw/Ww9tzuJmJQL6aHNh2lDOg1WvM44a5PDbOMaAbIWEEL",
  secret_key_base_email: "YXOAubJw/z6mfRJwqSOsw/Ww9tzuJmJQL6aHNh2lDOg1WvM44a5PDbOMaAbIWEEL",
  secret_key_base_room: "D8AOCCXvEbZw4WKDhKyb9AmuOg16cweDtybNxWmVrWD7Xs/LgsP7qjI99ia1JrN/",
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../", __DIR__)]]

config :panda_phoenix,
  url_email: "http://localhost:4000/",
  environment: :dev,
  time_sec_force_logout_duplicat: 1,
  time_sec_force_logout: 5,
  time_sec_by_report: 600,
  time_click_max_report: 20,
  google_api_key: "AIzaSyBMAOvtYCO0jLkGBfupovgruB6gdzAcwmc",
  google_api_url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=@LAT,@LNG&sensor=false&language=en&key=@KEY",
  rate_limit_interval_seconds: 300,
  rate_limit_max_requests: 5

config :logger, level: :debug

# Watch static and templates for browser reloading.
config :panda_phoenix, PandaPhoenix.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex)$}
    ]
  ]

config :pigeon,
  apns_mode: :dev,
  apns_cert: {:panda_phoenix, "cert/dev/cert.pem"},
  apns_key: {:panda_phoenix, "cert/dev/key.pem"},
  apns_2197: false

config :ex_aws,
  access_key_id: ["AKIAJAAJYOUTUZHKIVMQ", :instance_role],
  secret_access_key: ["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", :instance_role]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development.
# Do not configure such in production as keeping
# and calculating stacktraces is usually expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :panda_phoenix, PandaPhoenix.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "panda_dev",
  hostname: "localhost",
  pool_size: 10
