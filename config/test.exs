use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :panda_phoenix, PandaPhoenix.Endpoint,
  http: [port: 4001],
  server: false,
  secret_key_base: "SwN0lF6gpELTyXbVgEJB4MydQfmRXhPU64S08GK3r4KlsgxuaGX/pT1AC/WTiVIM",
  secret_key_refresh_token: "QZWSlF6gpELTyXbVgEJB4MydQfmRXhPU64S08GK3r4KlsgxuaGX/pT1AC/WTiVIM",
  secret_key_base_data: "dkpTiFGxxm/XdHR7YDP5B8qNKk0uZ+iZa42MsKsDqKx1MOFrhfyMes8h28zeMZ2x",
  secret_key_base_email: "YXOAubJw/z6mfRJwqSOsw/Ww9tzuJmJQL6aHNh2lDOg1WvM44a5PDbOMaAbIWEEL",
  secret_key_base_activation_user: "QAWSAubJw/z6mfRJwqSOsw/Ww9tzuJmJQL6aHNh2lDOg1WvM44a5PDbOMaAbIWEEL",
  secret_key_base_reset_password: "a!JSLWSw/z6mfRJwqSOsw/Ww9tzuJmJQL6aHNh2lDOg1WvM44a5PDbOMaAbIWEEL",
  secret_key_base_room: "D8AOCCXvEbZw4WKDhKyb9AmuOg16cweDtybNxWmVrWD7Xs/LgsP7qjI99ia1JrN/"

config :panda_phoenix,
  url_email: "http://localhost:4001/",
  environment: :test,
  time_sec_force_logout_duplicat: 0,
  time_sec_force_logout: 0,
  time_sec_by_report: 150,
  time_click_max_report: 1,
  google_api_key: "AIzaSyBMAOvtYCO0jLkGBfupovgruB6gdzAcwmc",
  google_api_url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=@LAT,@LNG&sensor=false&language=en&key=@KEY",
  rate_limit_interval_seconds: 300,
  rate_limit_max_requests: 5

config :ex_aws,
  access_key_id: ["AKIAJAQEAWIDT7QJLRHA", :instance_role],
  secret_access_key: ["lSKI8Kjwn33nn/d77X5+V08MSAiXlTrG3KyW4o18", :instance_role]

# Print only warnings and errors during test
config :logger, level: :error

# Configure your database
config :panda_phoenix, PandaPhoenix.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "panda_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
