# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :panda_phoenix,
  ecto_repos: [PandaPhoenix.Repo]

# Configures the endpoint
config :panda_phoenix, PandaPhoenix.Endpoint,
  url: [host: "localhost"],
  root: Path.dirname(__DIR__),
  render_errors: [accepts: ~w(html json)],
  pubsub: [name: PandaPhoenix.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :arc,
  bucket: "dotpanda",
  virtual_host: true

config :ex_aws,
  region: "us-east-1",
  s3: [
    scheme: "https://",
    host: "s3.amazonaws.com",
    region: "us-east-1"
  ]

config :panda_phoenix, PandaPhoenix.Mailer,
  adapter: Bamboo.SMTPAdapter,
  server: "smtp.dotpanda.co",
  port: 587,
  username: "support",
  password: "zeihaYiphe7chahThiay",
  tls: :if_available, # can be `:always` or `:if_available`
  ssl: false, # can be `true`
  retries: 4

# Configure phoenix generators
config :phoenix, :generators,
  migration: true,
  binary_id: false

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
