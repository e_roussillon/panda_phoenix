# PandaPhoenix

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: http://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

## Status code

https://hexdocs.pm/plug/Plug.Conn.Status.html#code/1-known-status-codes

## Setup Elixir - 1.3.4

  * ` brew install https://raw.githubusercontent.com/Homebrew/homebrew-core/59d7802e40ffbe2c7c840354530bc62b5a1538f9/Formula/elixir.rb`

## Setup DB - 9.5.2

  * `brew install https://raw.githubusercontent.com/Homebrew/homebrew-core/c41ee814a3da12fc508b79fd691f6f97c26cb165/Formula/postgresql.rb`
  * `initdb /usr/local/var/postgres`
  * you may need to start server before create user -> `pg_ctl -D /usr/local/var/postgres -l logfile start`
  * `/usr/local/Cellar/postgresql/<version>/bin/createuser -s postgres`
  * `mkdir -p ~/Library/LaunchAgents`
  * `ln -sfv /usr/local/opt/postgresql/*.plist ~/Library/LaunchAgents`
  * `launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist`
  * `psql -U postgres -h localhost`

## Image Magick

  * `brew update && brew install imagemagick`

## Setup project Heroku
  * `heroku git:remote -a infinite-badlands-61487`

  -> Logs -> `heroku logs --tail`
  -> db -> `heroku pg:psql`

## Cert iOS

  * download your voip_services.cer from Apple Developer
  * add voip_services.cer into keychain
  * Exportvoip_services.cer in .p12 - right click on the file VoIP Service:<bundle_of_your_app> and click Export voip_services.p12.
  * run openssl x509 -in voip_services.cer -inform der -out cert.pem
  * run openssl pkcs12 -nocerts -out key.pem -in voip_services.p12
  * to remove the password run openssl rsa -in key.pem -out key_unencrypted.pem
  * add key_unencrypted.pem and cert.pem into you project at /private/cert/
  * into config.exs you will set you file like this
  * config :pigeon,
    apns_mode: :prod,
    apns_cert: {:panda_phoenix, "cert/beta/cert.pem"},
    apns_key: {:panda_phoenix, "cert/beta/key.pem"},
    apns_2197: true


## Release

  * go to `mix.exs` and update `version: "{VERSION}"`
  * `git push master`
  * add tag `git tag {VERSION}`
  * `git push --tag`
  * run `mix deps.get && npm install && ./node_modules/brunch/bin/brunch b -p && MIX_ENV=prod mix do phoenix.digest, release --env=prod --upgrade`
  * To start
    * go to directory `cd local_deploy/releases/{VERSION}`
    * run `tar xvf panda_phoenix.tar.gz`
    - Interactive: run `HOST=192.168.25.28 PORT=4000 REPLACE_OS_VARS=true NODE_NAME=n1@127.0.0.1 MIX_ENV=prod _build/prod/rel/panda_phoenix/bin/panda_phoenix console`
    - Foreground: run `HOST=192.168.25.28 PORT=4000 REPLACE_OS_VARS=true NODE_NAME=n1@127.0.0.1 MIX_ENV=prod _build/prod/rel/panda_phoenix/bin/panda_phoenix foreground`
    - Daemon: run `HOST=192.168.25.28 PORT=4000 REPLACE_OS_VARS=true NODE_NAME=n1@127.0.0.1 MIX_ENV=prod _build/prod/rel/panda_phoenix/bin/panda_phoenix start`
  * release working
    * remove all files except `local_deploy/releases/{VERSION}/panda_phoenix.tar.gz`
    * got to `rel/config.exs` comment `set vm_args: "vm.args"` and `set sys_config: "sys.config"`
    * Commit modifications

## observer

  * run `:observer.start`

## Docker

  * modify docker `vim Dockerfile`
  * update modofication  `docker build -t dotpanda .`
  * stop docker `docker rm -f dotpanda`
  * start docker `docker run -d --name dotpanda -p 4000:4000 -v /home/dotpanda/dotpanda.app:/home/dotpanda dotpanda`
  * check docker running `docker ps -a`
  * check logs `docker logs -f dotpanda`
  * access console `docker exec -ti dotpanda bash`

## server

dotpanda@prod01 -> ssh -i ~/.ssh/dotpanda dotpanda@ec2-52-91-201-232.compute-1.amazonaws.com
dotpanda@prod02 -> ssh -i ~/.ssh/dotpanda dotpanda@ec2-54-227-28-255.compute-1.amazonaws.com
