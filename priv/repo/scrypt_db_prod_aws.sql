CREATE OR REPLACE FUNCTION public.insert_public_room(
    room_id integer,
    room_original_name text,
    room_name text,
    room_type text)
  RETURNS text AS
$BODY$DECLARE
    NUM integer;
    NEW_NUM integer := -1;
BEGIN

LOCK TABLE public_rooms IN ACCESS EXCLUSIVE MODE;
SELECT lft FROM public_rooms WHERE id = room_id INTO NUM;
IF NUM >= 0 THEN
	UPDATE public_rooms SET rgt = rgt + 2 WHERE rgt > NUM;
	UPDATE public_rooms SET lft = lft + 2 WHERE lft > NUM;
	INSERT INTO public.public_rooms(original_name, name, type, lft, rgt, inserted_at, updated_at) VALUES (room_original_name, room_name, room_type, NUM + 1, NUM + 2, now(), now());
	SELECT cast(currval(pg_get_serial_sequence('public_rooms', 'id')) AS text) INTO NEW_NUM;
END IF;
RETURN NEW_NUM;

END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.insert_public_room(integer, text, text, text)
  OWNER TO panda_admin;

-- <!-- FUNCTION  tree -->
CREATE OR REPLACE VIEW public.tree AS
SELECT
concat(repeat('-'::text, count(parent.name)::integer - 1), '> ', node.original_name) AS original_name,
concat('(', count(parent.name) - 1, ') ', node.type) AS type,
concat(repeat('-'::text, count(parent.name)::integer - 1), '> ', node.name) AS name,
    node.id
   FROM public_rooms node,
    public_rooms parent
  WHERE node.lft >= parent.lft AND node.lft <= parent.rgt
  GROUP BY node.id
  ORDER BY node.lft;

ALTER TABLE public.tree
  OWNER TO panda_admin;

CREATE OR REPLACE FUNCTION public.check_biggest_date_between_two(
      DATE_A timestamp,
      DATE_B timestamp)
    RETURNS timestamp AS
  $BODY$DECLARE
      DATE_RESULT timestamp;
  BEGIN

  IF (DATE_A IS NULL) AND (DATE_B IS NULL) THEN
    DATE_RESULT := now();
  ELSIF (DATE_A IS NOT NULL) AND (DATE_B IS NULL) THEN
    DATE_RESULT := DATE_A;
  ELSIF (DATE_A IS NULL) AND (DATE_B IS NOT NULL) THEN
    DATE_RESULT := DATE_B;
  ELSE
    IF (DATE_A > DATE_B) THEN
      DATE_RESULT := DATE_A;
    ELSE
      DATE_RESULT := DATE_B;
    END IF;
  END IF;
  RETURN DATE_RESULT;

END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.check_biggest_date_between_two(timestamp, timestamp)
  OWNER TO panda_admin;

CREATE OR REPLACE FUNCTION public.check_biggest_date_between_three(
      DATE_A timestamp,
      DATE_B timestamp,
      DATE_C timestamp)
    RETURNS timestamp AS
  $BODY$DECLARE
      DATE_A_TMP timestamp;
      DATE_B_TMP timestamp;
      DATE_RESULT timestamp;
  BEGIN

  IF (DATE_A IS NULL) AND (DATE_B IS NULL) AND (DATE_C IS NULL) THEN
    DATE_RESULT := now();
  ELSIF (DATE_A IS NOT NULL) AND (DATE_B IS NULL) AND (DATE_C IS NULL) THEN
    DATE_RESULT := DATE_A;
  ELSIF (DATE_A IS NULL) AND (DATE_B IS NOT NULL) AND (DATE_C IS NULL) THEN
    DATE_RESULT := DATE_B;
  ELSIF (DATE_A IS NULL) AND (DATE_B IS NULL) AND (DATE_C IS NOT NULL) THEN
    DATE_RESULT := DATE_C;
  ELSIF (DATE_A IS NOT NULL) AND (DATE_B IS NOT NULL) AND (DATE_C IS NULL) THEN
    DATE_RESULT := public.check_biggest_date_between_two(DATE_A, DATE_B);
  ELSIF (DATE_A IS NOT NULL) AND (DATE_B IS NULL) AND (DATE_C IS NOT NULL) THEN
    DATE_RESULT := public.check_biggest_date_between_two(DATE_A, DATE_C);
  ELSIF (DATE_A IS NULL) AND (DATE_B IS NOT NULL) AND (DATE_C IS NOT NULL) THEN
    DATE_RESULT := public.check_biggest_date_between_two(DATE_B, DATE_C);
  ELSE
    DATE_A_TMP := public.check_biggest_date_between_two(DATE_A, DATE_B);
    DATE_B_TMP := public.check_biggest_date_between_two(DATE_B, DATE_C);
    DATE_RESULT := public.check_biggest_date_between_two(DATE_A_TMP, DATE_B_TMP);
  END IF;
  RETURN DATE_RESULT;

END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.check_biggest_date_between_three(timestamp, timestamp, timestamp)
  OWNER TO panda_admin;
