defmodule PandaPhoenix.Repo.Migrations.CreateUserReported do
  use Ecto.Migration

  def change do
    create table(:user_reported) do
      add :count, :integer
      add :user_id, :integer
      add :is_active, :boolean, default: false
      add :is_used, :boolean, default: false
      add :notified, :integer, default: 0

      timestamps
    end
    create unique_index(:user_reported, [:user_id])

  end
end
