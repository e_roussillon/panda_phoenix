defmodule PandaPhoenix.Repo.Migrations.CreateFilter do
  use Ecto.Migration

  def change do
    create table(:user_filters) do
      add :name, :string
      add :is_highlight, :boolean, default: false
      add :user_id, :integer
      add :active, :boolean, default: true

      timestamps
    end

    create unique_index(:user_filters, [:name, :user_id])
  end
end
