defmodule PandaPhoenix.Repo.Migrations.CreatePrivateRoom do
  use Ecto.Migration

  def change do
    create table(:private_rooms) do
      add :name, :string
      add :is_group, :boolean, default: false
      add :data_id, :integer

      timestamps
    end

    create index(:private_rooms, [:data_id])
  end
end
