defmodule PandaPhoenix.Repo.Migrations.CreateData do
  use Ecto.Migration
  use Arc.Ecto.Schema

  def change do
    create table(:datas) do
      add :url_thumb, :text
      add :url_original, :text
      add :url_blur, :text
      add :type, :integer
      add :size, :float

      timestamps
    end

  end
end
