defmodule PandaPhoenix.Repo.Migrations.CreateUserReport do
  use Ecto.Migration

  def change do
    create table(:user_report) do
      add :user_id_reportor, :integer
      add :user_id_reported, :integer

      timestamps
    end
    create unique_index(:user_report, [:user_id_reportor, :user_id_reported])

  end
end
