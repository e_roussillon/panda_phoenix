defmodule PandaPhoenix.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    alter table(:user_passwords) do
      add :reset_password_token, :string
    end
  end
end
