defmodule PandaPhoenix.Repo.Migrations.CreatePublicMessage do
  use Ecto.Migration

  def change do
    create table(:public_messages) do
      add :from_user_id, :integer
      add :is_broadcast, :boolean, default: false
      add :is_general, :boolean, default: true
      add :data_id, :integer, default: -1
      add :msg, :string
      add :public_room_id, :integer

      timestamps
    end
    create index(:public_messages, [:from_user_id, :data_id, :public_room_id])

  end
end
