defmodule PandaPhoenix.Repo.Migrations.CreatePrivateRoomUser do
  use Ecto.Migration

  def change do
    create table(:private_room_users) do
      add :room_id, :integer
      add :user_id, :integer

      timestamps
    end

    create unique_index(:private_room_users, [:room_id, :user_id])
  end
end
