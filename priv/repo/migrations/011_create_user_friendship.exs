defmodule PandaPhoenix.Repo.Migrations.CreateUserFriendship do
  use Ecto.Migration

  def change do
    create table(:user_friendships) do
      add :status, :integer
      add :user_id, :integer
      add :to_user_id, :integer
      add :last_action, :integer

      timestamps
    end
    create unique_index(:user_friendships, [:user_id, :to_user_id])
    create unique_index(:user_friendships, [:to_user_id, :user_id])
    create index(:user_friendships, [:last_action])

  end
end
