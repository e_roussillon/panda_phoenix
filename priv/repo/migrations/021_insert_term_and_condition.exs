defmodule PandaPhoenix.Repo.Migrations.InsertData do
  use Ecto.Migration

  def up do
      execute "DELETE FROM public.extra_infos WHERE type='term_and_condition'"

      execute "INSERT INTO public.extra_infos(content, language, type, inserted_at, updated_at) VALUES ('<h3>Terms of use</h3>

      <p>Welcome to the .panda application</p>

      <p>Please read these terms of use (hereinafter the “Terms”), and any special terms stated on this pages of the
      application, which govern your use of the “.panda” mobile application.</p>

      <p>These Terms cover your rights and legal responsibilities, and become applicable as soon as you start to use
      the application.</p>

      <p>This application is available to you free of charge (except for Internet connection costs) for your personal
      use, subject to compliance with the Terms set out below.</p>

      <p>By accessing, visiting and/or using this application, you acknowledge that you have read, understood and
      agree to be bound by these Terms, and that you undertake to comply with all relevant laws and
      regulations. If you do not agree to these Terms, please do not use this application.</p>

      <p>You undertake to access the information provided on this application exclusively for personal and
      commercial use.</p>

      <p>You may only use the application for legitimate purposes. It may not be used or misappropriated for
      purposes contrary to Public Order and Morality.</p>

      <p>The content of these Terms of use may change. We therefore advise you to read them regularly.</p>


      <h3>1. Legal information</h3>

      <p>This application is the property of .panda, a registered mark at the national institute of industrial property.
      This application is a property of the cofounder of .panda.</p>

      <h3>2. Limitation of liability and guarantees</h3>

      <p>As a user of the application, you acknowledge that you have the necessary skills and means to access and
      use this application.</p>

      <p>.panda and its contributors make every effort to ensure that the information provided on this application is
      accurate and up-to- date, reserving the right to modify the content at any time and without notice.
      However, they cannot guarantee that this information is complete, or that it has not been modified by a
      third party (hacking, virus, etc.). .panda and its contributors also accept no liability (direct or indirect) in the
      event of delay, error or omission with regard to the contents and use of these pages and in the event of
      the service being interrupted or unavailable.</p>

      <p>You agree that you have been informed that the application is accessible 24 hours a day, 7 days a week,
      except in the event of force majeure, server problems, problems caused by the telecommunications
      network infrastructures or other technical difficulties. .Panda may occasionally shut down its application for
      maintenance purposes. In this case, it will make every effort to notify users in advance.</p>

      <p>.panda is not responsible for delays, operating problems or incompatibility between the application your OS
      .panda may under no circumstances be held liable for any direct or indirect damage resulting from or
      subsequent to third-party distribution of a virus by means of our application, which may infect you.
      Likewise, .panda may not be held liable for any material or consequential damage (including but not limited
      to technical failure, disclosure of confidential documents and data loss), or for any other indirect damage,
      arising from or associated with the use of the application.</p>

      <p>.panda makes every possible effort to ensure that the information provided on the application is accurate
      and up-to- date, and reserves the right to correct the content at any time and without prior notice.
      However, .panda and its contributors make no undertaking and assume no liability, under any
      circumstances whatsoever, with regard to the relevance, continuity, accuracy, absence of errors, veracity,
      timeliness, fair and commercial nature, quality, validity and availability of the information contained in this
      application. Each Internet user shall be fully responsible for the risks incurred by giving credence to this
      information. Incorrect information or omissions may be encountered arising from typographical or
      formatting errors. If you find any such errors, please inform us so that we can make the relevant
      corrections.</p>

      <p>The information on the application is provided “as seen”, without guarantee of any kind, whether implied or
      explicit. .panda categorically rejects any interpretation that seeks to represent the content of the application
      an offer of purchase or an incitement to purchase shares or other securities.</p>

      <p>.panda reserves the right to modify any content of the application at its sole discretion. Pursuant to its
      policy of updating and improving the application, .panda may decide to modify these Terms.</p>

      <p>Any dated information published on the application is valid exclusively for the specified date.</p>

      <p>Users are also reminded that the confidentiality of correspondence cannot be guaranteed on the network
      and that every Internet user is responsible for taking all appropriate measures to protect their own data
      and/or software from contamination by any viruses that may be circulating on the Internet.
      The application includes links to other websites or applications likely to have their own legal conditions,
      which should be read and complied with.</p>

      <h3>3. Access codes and passwords</h3>

      <p>Services on the application require an authentication through access code and password.</3>

      <p>These access codes and passwords are confidential, personal and nontransferable. You are responsible for
      their management, safekeeping and for the consequences of their use. You are responsible for taking the
      measures necessary for their protection and safekeeping. .panda may under no circumstances be held
      liable for any fraudulent use.</p>

      <p>For security reasons, access to secure sections will be automatically barred after several failed access
      attempts. .panda reserves the right to suspend access to the application in the event of fraud or attempted
      fraud in the use of a password or access code. In the event of your access being suspended, .panda will
      notify you.</p>

      <p>The information recorded by the application system when the user logs on will constitute evidence of any
      actions performed by the user on the application, and evidence that those actions have been processed.</p>

      <h3>4. Intellectual property</h3>

      <p>The application is fully subject to French law on copyright, trademarks and, in general, on intellectual
      property.</p>

      <p>The .panda trademarks and logos (semi-figurative trademarks) shown on the application are registered
      trademarks. Any full or partial reproduction or representation, alone or in company with other elements,
      without the prior express written authorisation of .panda is strictly forbidden.</p>

      <p>The general structure, software, texts, images, videos, sounds, know-how, animations, and more generally
      all information and content shown on the application, are the property of .panda or are subject to rights of
      use or exploitation. These elements are subject to copyright law.</p>

      <p>Any full or partial representation, modification, reproduction, distortion, of all or part of the application or
      its contents, by any procedure whatsoever and in any medium whatsoever, constitutes an infringement
      sanctioned by articles L 335-2 of Intellectual Property Code.</p>

      <p>These Terms do not grant you any licence for the use of trademarks, logos or photographs belonging to
      .panda.</p>

      <p>Any databases appearing on the application are protected by the legal provisions for the protection of
      databases. In this respect, .panda expressly prohibits any re-use, reproduction or extraction of elements of
      these databases. The user shall be held liable for any such reuse, reproduction or extraction.</p>

      <p>.panda reserves the right to delete immediately and without prior notice any content – message, text,
      image or graphic that contravenes the laws and regulations in force and particularly the regulations
      specified above.</p>

      <p>If you wish to use some of the application content (text, image, etc), you must obtain prior express written
      authorisation from .panda, by writing to the e-mail to the webmaster.</p>

      <h3>5. Respect for privacy</h3>

      <p>As a user of the application, you are bound by the relevant laws, and notably the French law on privacy,
      data protection and freedom of information. Infringement of this law is a criminal offence.</p>

      <p>In particular, you must refrain from compiling and using in any fraudulent manner any personal data you
      obtain, and from any action likely to damage the privacy, reputation, esteem, image or sensitivity, of any
      individual or corporation, and notably of .panda, by avoiding any defamatory, provocative, malicious or
      threatening comments, messages or texts on any medium.</p>

      <h3>6. Personal information</h3>

      <p>In order to meet your needs while you are using the application, .panda may collect personal information
      about you, which is processed by electronic methods (some information, which is marked with an asterisk,
      is mandatory).</p>

      <p>You are responsible for checking that the information that you provide through this application is accurate
      and complete. So that we can deal with your requests correctly, please inform us immediately of any
      change to the information you have provided.</p>

      <p>The information collected is exclusively for the use of .panda, which is authorised by these Terms of use,
      unless opposed by you for legitimate reasons, to store and use them in order to inform you about its
      products and services.</p>

      <p>.panda may also pass this information to other companies, to its brokers, its insurers, and to third parties or
      subcontractors, but only for the purpose of administration or prospecting, unless opposed by you.</p>

      <p>You can oppose the use of data about you for canvassing purposes, at no cost to yourself.</p>

      <p>To exercise your right to access, correct and dispute personal information, please contact us.</p>

      <p>The application is not intended to receive any confidential information which you may submit.
      Consequently, and with the exception of the personal data outlined above, any information (documents,
      data, graphics, questions, suggestions, concepts, remarks or other) which you communicate on the
      application will in no way be deemed confidential. As a result, your decision to communicate that
      information to us entitles us to use, reproduce, post, modify or forward it, in order to process your request.</p>

      <h3>7. Hyperlinks</h3>

      <p>.panda may not be held liable for hyperlinks to other websites or applications, and particularly for the
      content of those websites and applications.</p>

      <p>.panda is not liable for hyperlinks from other sites to this application and no such links may be set up
      without its prior written authorization.</p>

      <h3>8. Cautionary statement on forward-looking data</h3>

      <p>The application may contain certain information that does not rely on historical facts and that makes
      statements about the future, including – but not limited to – statements that are predictions of future
      events, trends, plans or objectives. Such statements are based on the current views and assumptions of
      management and are subject to significant risks and uncertainties that could cause actual results to differ
      materially from those expressed or implied in the forward-looking statements (or from past results).
      Additional information regarding these risks and uncertainties is set out in the documents filed by .panda
      with the relevant authorities. Forward-looking statements represent predictions made on a certain date,
      and .panda makes no undertaking to update or revise them, whether in response to new information,
      future events or for any other reason.</p>

      <h3>9. General clauses</h3>

      <p>Any assignment or other transfer of the rights granted under these conditions is strictly forbidden.
      If, for any reason whatsoever, a competent court were to rule that a provision of these Terms is invalid, the
      invalidity of that provision will not in any way affect the validity of the remaining Terms, which will remain
      in force.</p>

      <p>Failure by one of the parties to exercise a right or to institute legal proceedings under the terms of these
      Terms shall not be deemed to be a waiver of such a right or such proceedings.</p>

      <p>The applicaiton is subject to French law. These Terms will be governed by and interpreted in accordance
      with French Law.</p>', 'en', 'term_and_condition', now(), now());"

      execute "INSERT INTO public.extra_infos(content, language, type, inserted_at, updated_at) VALUES ('<h2>Mentions Légales</h2>

      <p>Bienvenue sur l’application .panda</p>

      <p>Conformément à la loi, nous vous invitons à prendre connaissance des présentes conditions d’utilisation (ci-
      après les «Conditions»), ainsi que toute condition spécifique figurant sur les pages de l’application,
      régissant votre utilisation de la dite application.</p>

      <p>Ces Conditions portent sur vos droits et responsabilités légales applicables dès lors que vous utilisez cette
      application.</p>

      <p>Cette application est mise gratuitement à votre disposition (hors frais de connexion) pour votre usage
      personnel, sous réserve du respect des Conditions définies ci-après.</p>

      <p>En accédant à cette application, en y navigant et/ou en l’utilisant, vous reconnaissez que vous avez lu, que
      vous avez compris et que vous acceptez d’être lié par ces Conditions, de même que vous vous engagez à
      vous conformer à l’ensemble des lois et règlements applicables. Si vous n’acceptez pas ces Conditions,
      veuillez ne pas utiliser la présente application.</p>

      <p>Vous ne pouvez utiliser l’application que dans un but légitime ; aucune utilisation, ou détournement n’est
      autorisé, notamment pour des finalités contraires à l’Ordre Public et aux Bonnes Mœurs.
      Le contenu de la présente notice peut être amené à changer ; nous vous invitons par conséquent à la
      consulter très régulièrement.</p>

      <h3>1. Informations d’ordre général</h3>

      <p>La présente application est la une marque déposée et la propriété de ces fondateurs.
      .panda est une marque déposée à l’institut national de la propriété industrielle.</p>


      <h3>2. Limitation de responsabilité et exclusion de garantie</h3>

      <p>En tant qu’utilisateur vous reconnaissez disposer de la compétence et des moyens nécessaires pour accéder
      et utiliser cette application</p>

      <p>.panda et ses contributeurs mettent tout en œuvre pour s’assurer que les informations affichées sont
      exactes et à jour, en se réservant le droit d’en modifier le contenu à tout moment et sans préavis.
      Néanmoins, ils ne sauraient garantir que ces informations sont complètes, ni qu’elles ne seront pas
      modifiées par un tiers (piratage, virus). .panda et ses contributeurs déclinent également toute
      responsabilité (directe ou indirecte) en cas de retard, d’erreur ou d’omission en ce qui concerne le contenu
      et l’utilisation de ces pages, de même qu’en cas d’interruption ou d’indisponibilité du service.</p>

      <p>Vous reconnaissez avoir été informé que l’application est accessible 24h/24h et 7 jours/7 jours, à exception
      des cas de force majeure, difficultés informatiques, difficultés liées à la structure des réseaux de
      télécommunications ou autres difficultés techniques. Pour des raisons de maintenance, .Panda pourra être
      interrompue momentanément. Il s’efforcera d’en avertir préalablement les utilisateurs. .Panda n’est pas
      responsable des retards, difficulté d’utilisation, ou de l’incompatibilité entre l’application et les versions
      précédentes de votre OS.</p>

      <p>.panda ne pourra en aucun cas être tenu responsable de tout dommage direct ou indirect résultant ou
      consécutif à la diffusion par une personne tierce d’un virus par l’intermédiaire de notre application et
      susceptible d’infecter votre système informatique à la suite de votre connexion. De même .panda ne pourra
      être responsable de dommage matériel ou accessoire (y compris, mais sans s’y limiter, défaillance
      technique, divulgation de documents confidentiels, perte de données), ni de tout autre dommage indirect,
      quelconques survenant lors de ou liés à l’utilisation de l’application.</p>

      <p>.panda s’efforce d’assurer au mieux de ses possibilités, l’exactitude et la mise à jour des informations
      diffusées, dont il se réserve le droit de corriger, à tout moment et sans préavis, le contenu. Toutefois,
      .panda et ses contributeurs ne donnent aucune garantie ni n’assument aucune responsabilité, en aucun
      cas, quant à l’adéquation, la séquence, l’exactitude, l’absence d’erreurs, la véracité, l’actualité, le caractère
      loyal et commercial, la qualité, le bien-fondé, et la disponibilité des informations contenues sur cette
      application. Chaque utilisateur assume pleinement les risques liés au crédit qu’il accorde à ces informations.
      Des informations erronées ou des omissions pourront être constatées dues notamment à des erreurs
      typographiques ou de mise en page. Si vous constatiez quelques erreurs vous êtes invités à nous les
      indiquer pour qu’il soit procédé aux corrections appropriées.</p>

      <p>Les éléments de l’application sont fournis «en l’état» sans aucune garantie d’aucune sorte, implicite ou
      explicite. .panda rejette catégoriquement toute interprétation qui viserait à assimiler le contenu de
      l’application à des offres d’achat ou des incitations à acquérir des actions ou autres valeurs mobilières
      cotées ou non cotées.</p>

      <p>.panda se réserve le droit, à sa seule discrétion, de modifier tout élément de l’application. Dans le cadre de
      sa politique de mise à jour et d’optimisation de l’application, .panda peut décider de modifier les présentes
      conditions.</p>

      <p>Toute information datée qui est publiée sur l’application ne vaut que pour la date précisée uniquement.
      Il est rappelé également que le secret des correspondances n’est pas garanti sur le réseau et qu’il
      appartient à chaque utilisateur d’Internet de prendre toutes les mesures appropriées de façon à protéger
      ses propres données.</p>

      <h3>3. Gestion des codes d’accès et mots de passe</h3>

      <p>Les services présents sur cette application nécessitent une connexion et une identification à travers un
      login et un mot de passe.</p>

      <p>Ces codes d’accès et mots de passe sont confidentiels, personnels, incessibles et intransmissibles. Vous êtes
      responsable de leur gestion, conservation et des conséquences de leur utilisation. Il vous appartient de
      prendre les dispositions nécessaires à leur protection et à leur conservation. .panda ne pourra en aucun cas
      être tenu pour responsable de toute utilisation frauduleuse.</p>

      <p>Pour des raisons de sécurité, l’accès aux rubriques sécurisées sera automatiquement invalidé à l’issue de
      plusieurs tentatives d’accès erronées. .panda se réserve le droit de suspendre l’accès à l’application en cas
      d’utilisation frauduleuse ou de tentative d’utilisation frauduleuse du mot de passe ou du code d’accès d’un
      utilisateur. En cas de suspension de l’accès, .panda vous en informera.</p>

      <p>Les enregistrements exécutés par le système informatique de l’application lors des connexions de
      l’utilisateur constitueront la preuve de tout acte effectué par l’utilisateur sur l’application, ainsi que la preuve
      du traitement de ces actes.</p>

      <h3>4. Propriété intellectuelle</h3>

      <p>L’application est régi dans son intégralité par la législation française relative au droit d’auteur, au droit des
      marques et d’une manière générale, à la propriété intellectuelle.</p>

      <p>Les marques et les logos (marques semi-figuratives) de .panda figurant sur l’application sont des marques
      déposées. Toute reproduction ou représentation totale ou partielle, seules ou intégrées à d’autres éléments,
      sans l’autorisation écrite, expresse et préalable de .panda en est strictement interdite.</p>

      <p>La structure générale, les logiciels, textes, images, vidéos, sons, savoir-faire, animations, et plus
      généralement toutes les informations et contenus figurant dans l’application, sont la propriété de .panda ou
      font l’objet d’un droit d’utilisation ou d’exploitation. Ces éléments sont soumis à la législation protégeant le
      droit d’auteur.</p>

      <p>Toute représentation, modification, reproduction, dénaturation, totale ou partielle, de tout ou partie de
      l’application ou de son contenu, par quelque procédé que ce soit, et sur quelque support que ce soit
      constituerait une contrefaçon sanctionnée par les articles L 335-2 et suivants du Code de la Propriété
      Intellectuelle.</p>

      <p>Les présentes Conditions ne vous accordent aucune licence d’utilisation des marques, logos ou
      photographies de .panda.</p>

      <p>Les bases de données figurant, le cas échéant, sur le l’application sont protégées par les dispositions
      relatives à la protection juridique des bases de données. A ce titre, .panda interdit expressément toute
      réutilisation, reproduction ou extraction d’éléments de ces bases de données. La réutilisation, reproduction
      ou extraction non autorisée engage la responsabilité de l’utilisateur.</p>

      <p>.panda se réserve la faculté, de supprimer sans délais, et sans mise en demeure préalable, tout contenu :
      message, texte, image, graphique qui contreviendrait aux lois et règlements en vigueur et notamment les
      réglementations précisées ci-dessus.</p>

      <p>Dans l’hypothèse où vous souhaiteriez utiliser un des contenus de l’application (texte, image…), vous devez
      obtenir l’autorisation écrite, expresse et préalable de .panda, en envoyant un courriel au webmaster.</p>

      <h3>5. Respect de la vie privée</h3>

      <p>En tant qu’utilisateur de l’application vous êtes tenus de respecter les législations applicables, et notamment
      les dispositions de la loi relative à l’informatique, aux fichiers et aux libertés, dont la violation est passible
      de sanctions pénales.</p>

      <p>Vous devez notamment vous abstenir de toute collecte, de toute utilisation détournée, notamment des
      informations nominatives auxquelles vous accédez, et d’une manière générale, de tout acte susceptible de
      porter atteinte à la vie privée, à l’honneur, à la sensibilité, à l’image de marque, à la notoriété de toute
      personne, physique ou morale, et notamment de .panda, en évitant toute mention, message ou texte
      diffamant, provocant, malveillant, dénigrant ou menaçant sur quelque support que ce soit.</p>

      <h3>6. Données à caractère personnel</h3>

      <p>Afin de répondre à vos besoins et pendant votre navigation sur l’application, .panda peut être amenée à
      recueillir des informations à caractère personnel vous concernant, traitées par des moyens informatiques
      (certaines informations, qui sont marquées d’un astérisque, étant obligatoires).</p>

      <p>Il vous appartient de vérifier que les informations que vous nous fournissez par le biais de cette application
      sont exactes et complètes. Pour le bon suivi de vos demandes, nous vous remercions de nous informer
      immédiatement de toute modification des informations fournies.</p>

      <p>Les informations recueillies sont exclusivement destinées à .panda, qui est autorisée par les présentes, sauf
      si vous vous y opposez pour des raisons légitimes, à les stocker et les utiliser afin de vous informer sur ses
      produits et services.</p>

      <p>Vous pouvez vous opposer, sans frais, à l’utilisation des données vous concernant à des fins de prospection.
      Pour exercer vos droits d’accès, de rectification et d’opposition, veuillez nous contacter.</p>

      <p>L’application n’est pas destiné à recevoir de votre part des informations à caractère confidentiel. En
      conséquence et à l’exception des données personnelles visées ci-dessus, toute information, quelle qu’en
      soit la forme – document, donnée, graphique, question, suggestion, concept, remarque, ou autre – que
      vous nous communiquerez à travers l’application ne sera en aucun cas tenue pour confidentielle. En
      conséquence sa simple transmission par vos soins nous confère le droit de l’utiliser de la reproduire de la
      diffuser, de la modifier ou de la transmettre dans le but de traiter votre demande.</p>

      <h3>7. Liens hypertextes</h3>

      <p>Les liens hypertextes mis en place en direction d’autres Sites ne sauraient engager la responsabilité de
      .panda, notamment s’agissant du contenu de ces Sites.</p>

      <h3>8. Avertissement sur les informations à caractère prévisionnel</h3>

      <p>L’application peut contenir certaines données non historiques constituant des déclarations à caractère
      prévisionnel, et notamment les déclarations prospectives concernant des événements, tendances, plans ou
      objectifs futurs. Ces déclarations sont fondées sur les vues et hypothèses actuelles des autres utilisateurs et
      sont sujettes à des risques et aléas importants susceptibles d’entraîner une différence significative entre les
      résultats réels et ceux contenus explicitement ou implicitement dans ces déclarations (ou les résultats
      antérieurs).</p>

      <h3>9. Clauses d’ordre général</h3>

      <p>Toute cession, ou autre transfert des droits conférés par les présentes conditions est strictement interdit.
      Si, pour quelque raison que ce soit, une juridiction compétente venait à considérer qu’une disposition des
      présentes Conditions est invalide, l’invalidité de cette disposition n’affectera en aucune façon la validité du
      reste des conditions, qui demeurera en vigueur.</p>

      <p>L’absence d’exercice par l’une des parties d’un droit ou d’une action en justice aux termes des présentes
      conditions ne pourra être considérée comme une renonciation à un tel droit ou à une telle action.
      L’application est régi par le droit français. Les présentes Conditions seront régies par, et interprétées,
      conformément à la loi française.</p>', 'fr', 'term_and_condition', now(), now());"
  end
end
