defmodule PandaPhoenix.Repo.Migrations.CreatePrivateMessage do
  use Ecto.Migration

  def change do
    create table(:private_messages) do
      add :room_id, :integer
      add :msg, :string
      add :status, :integer, default: -1
      add :from_user_id, :integer
      add :type, :integer, default: -1
      add :index_row, :string
      add :data_id, :integer

      timestamps
    end

    create unique_index(:private_messages, [:index_row])
    create index(:private_messages, [:room_id, :from_user_id, :data_id])

  end
end
