defmodule PandaPhoenix.Repo.Migrations.CreateExtraInfo do
  use Ecto.Migration

  def change do
    create table(:extra_infos) do
      add :content, :text
      add :language, :string
      add :type, :string

      timestamps
    end

    create unique_index(:extra_infos, [:language, :type])
  end
end
