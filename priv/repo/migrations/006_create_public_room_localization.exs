defmodule PandaPhoenix.Repo.Migrations.CreatePublicRoomLocalization do
  use Ecto.Migration

  def change do
    create table(:public_room_localization) do
      add :latitude, :string
      add :longitude, :string
      add :public_room_id, :integer
      add :api_result, :text
      add :result, :text
      add :google_result, :text

      timestamps
    end
    create index(:public_room_localization, [:public_room_id])
    create unique_index(:public_room_localization, [:latitude, :longitude])

  end
end
