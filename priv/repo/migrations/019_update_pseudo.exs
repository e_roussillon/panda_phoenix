defmodule PandaPhoenix.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create unique_index(:users, [:pseudo])
  end
end
