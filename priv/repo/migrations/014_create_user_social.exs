defmodule PandaPhoenix.Repo.Migrations.CreateUserSocial do
  use Ecto.Migration

  def change do
    create table(:user_socials) do
      add :social_id, :string
      add :social_type, :string
      add :user_id, :integer

      timestamps
    end
    create unique_index(:user_socials, [:user_id])

  end
end
