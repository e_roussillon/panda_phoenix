defmodule PandaPhoenix.Repo.Migrations.CreatePublicRoom do
  use Ecto.Migration

  def change do
    create table(:public_rooms) do
      add :name, :string
      add :original_name, :string
      add :type, :string
      add :lft, :integer
      add :rgt, :integer

      timestamps
    end

  end
end
