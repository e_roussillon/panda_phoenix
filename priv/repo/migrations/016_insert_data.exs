defmodule PandaPhoenix.Repo.Migrations.InsertData do
  use Ecto.Migration

  def up do

    case Mix.env do
      :dev ->
        # insert_data
        insert_data_basic
        insert_fonctions
      :test ->
        insert_data_basic
        insert_fonctions
      :prod ->
        # insert_data_basic
        # insert_fonctions_prod
    end
  end

  defp insert_fonctions do
    Mix.shell.cmd("export PGPASSWORD=postgres")
    Mix.shell.cmd("psql -h localhost -b panda_#{Mix.env} -U postgres -p 5432 -a -w -f #{Path.join([File.cwd!,"priv","repo","scrypt_db.sql"])}")
  end

  defp insert_fonctions_prod do
    Mix.shell.cmd("export PGPASSWORD=d2tivacm14p75t")
    Mix.shell.cmd("psql -h postgres://zsufwyvlweinna:VW99pnRBP1asoG_OVyxXebHeoe@ec2-54-235-217-221.compute-1.amazonaws.com:5432/d2tivacm14p75t -b panda_#{Mix.env} -U zsufwyvlweinna -p 5432 -a -w -f #{Path.join([File.cwd!,"priv","repo","scrypt_db_prod.sql"])}")
  end

  defp insert_data_basic do
    execute "INSERT INTO public.public_rooms(name, original_name, type, lft, rgt, inserted_at, updated_at) VALUES ('world', 'world', 'world', 0, 1, now(), now());"
  end

  defp insert_data do

    execute "INSERT INTO users (pseudo, email, status, description, birthday, gender, data_id, inserted_at, updated_at) VALUES ('dovi', 'dovi@gmail.com', NULL, NULL, NULL, false, NULL, '2016-04-21 15:54:04', '2016-04-21 15:54:04');"
    execute "INSERT INTO user_authentications (token, room, is_online, user_id, inserted_at, updated_at) VALUES ('g3QAAAACZAAEZGF0YWEBZAAGc2lnbmVkbgYANz6HOVQB##ZJDvpCg39m-Xyd_w4w4QjmpNmgg=', NULL, false, 1, '2016-04-21 15:54:05', '2016-04-21 15:54:05');"
    execute "INSERT INTO user_authentications (token, room, is_online, user_id, inserted_at, updated_at) VALUES ('g3QAAAACZAAEZGF0YWEBZAAGc2lnbmVkbgYANz6HOVQB##ZJDvpCg39m-Xyd_w4w4QjmpNmgg2=', NULL, true, 2, '2016-04-21 15:54:05', '2016-04-21 15:54:05');"
    execute "INSERT INTO user_passwords (password, user_id, inserted_at, updated_at) VALUES ('$2b$12$TO7HeMAcu8LMeL0HVVSXl.KLW0d/N0fkc5SxiNFyboUWV1xFowdSe', 1, '2016-04-21 15:54:05', '2016-04-21 15:54:05');"


    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user1', 'user1@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user2', 'user2@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user3', 'user3@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user4', 'user4@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user5', 'user5@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user6', 'user6@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user7', 'user7@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user8', 'user8@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user9', 'user9@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user10', 'user10@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user11', 'user11@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user12', 'user12@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user13', 'user13@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user14', 'user14@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user15', 'user15@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user16', 'user16@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user17', 'user17@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user18', 'user18@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user19', 'user19@gmail.com', now(), now());"
    execute "INSERT INTO public.users(pseudo, email, inserted_at, updated_at) VALUES ('user20', 'user20@gmail.com', now(), now());"

    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 2, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 3, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 4, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 5, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 6, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 7, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 8, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 9, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 10, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 11, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 12, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 13, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 14, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 15, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 16, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (4, 1, 17, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (1, 1, 18, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (1, 1, 19, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (6, 1, 20, 1, now(), now());"
    execute "INSERT INTO public.user_friendships(status, user_id, to_user_id, last_action, inserted_at, updated_at) VALUES (6, 1, 21, 1, now(), now());"
  end


end
