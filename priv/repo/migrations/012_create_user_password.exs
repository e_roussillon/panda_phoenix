defmodule PandaPhoenix.Repo.Migrations.CreateUserPassword do
  use Ecto.Migration

  def change do
    create table(:user_passwords) do
      add :password, :string
      add :can_broadcast, :boolean, default: false
      add :user_id, :integer

      timestamps
    end
    create unique_index(:user_passwords, [:user_id])

  end
end
