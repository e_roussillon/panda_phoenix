defmodule PandaPhoenix.Repo.Migrations.CreateUserAuthentication do
  use Ecto.Migration

  def change do
    create table(:user_authentications) do
      add :token, :string
      add :refresh_token, :string
      add :room, :string
      add :last_seen, :datetime
      add :is_online, :boolean, default: false
      add :user_id, :integer
      add :push_token, :string
      add :os, :string

      timestamps
    end
    
    create unique_index(:user_authentications, [:user_id])
    create unique_index(:user_authentications, [:token])
    create unique_index(:user_authentications, [:refresh_token])

  end
end
