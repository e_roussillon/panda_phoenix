defmodule PandaPhoenix.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :pseudo, :string
      add :email, :string
      add :description, :string
      add :birthday, :date
      add :gender, :integer, default: 0
      add :data_id, :integer

      timestamps
    end

    create index(:users, [:data_id])
    create unique_index(:users, [:email])
  end
end
